<?php

  /*
  *
  *	Template Name: Page [ Submissions ]
  *	Filename: page--submissions.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_the_ID();

  // ---------------------------------------- Mount Flexible Content
  get_template_part( 'snippets/flexible-content/main' );

  echo "<div class='submissions-page'>";
    echo "<div class='submissions-page__form'>";
      echo $THEME->render_bs_container( 'open', 'col-12 col-lg-6 offset-lg-3' );
        include( locate_template( './snippets/forms/unscripted-submissions.php' ) );
      echo $THEME->render_bs_container( 'closed' );
    echo "</div>";
  echo "</div>";

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
