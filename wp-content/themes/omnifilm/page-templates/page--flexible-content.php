<?php

  /*
  *
  *	Template Name: Page [ Flexible Content ]
  *	Filename: page--flexible-content.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Mount Flexible Content
  get_template_part( 'snippets/flexible-content/main' );

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
