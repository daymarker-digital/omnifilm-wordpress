<?php

  /*
  *
  *	Template Name: Page [ Productions Grid ]
  *	Filename: page--productions-grid.php
  *
  */

  // ---------------------------------------- Load Header Template
  get_header();

  // ---------------------------------------- Vars
  $THEME = $THEME ?? new CustomTheme();

  $block_name = 'productions';
  $current_id = get_the_ID();
  $categories = get_field( 'categories', $current_id ) ? get_field( 'categories', $current_id ) : [];
  $featured = get_field( 'featured', $current_id ) ? get_field( 'featured', $current_id ) : [];

?>

<div class='<?= $block_name; ?>'>

  <?= $THEME->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1', 'container-fluid' ); ?>
    <?= $THEME->render_navigation_page_children([ 'classes' => $block_name . '__sub-navigation d-none d-lg-flex', 'current_id' => $current_id ]); ?>
    <?= $THEME->render_productions_listing([ 'categories' => $categories, 'exclude' => $featured ]); ?>
  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>

<?php get_footer(); ?>
