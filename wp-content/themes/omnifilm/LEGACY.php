<!--     <h1 class="<?= $block_name ?>__title title title--page-title"><?= $title; ?></h1> -->




    switch( $layout ) {
        case 'featured-grid-large':
          break;
        case 'grid':
        case 'grid-large':
          break;
      }

      $html .= '<div class="d-lg-none">';
          $html .= $listing;
      $html .= '</div>';

      $html .= '<div class="d-none d-lg-block">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          $html .= $listing;
        $html .= $this->render_bs_container( 'closed' );
      $html .= '</div>';








          // ---------------------------------------- Post Data
          $about = get_field( 'about', $id ) ? get_field( 'about', $id ) : '';
          $abc_type = '25 x 30 Instructional Fitness Series';
          $abc_date = '2020 - Present';
          $alternate_featured = get_field( 'alternate_featured', $id ) ? get_field( 'alternate_featured', $id ) : [];
          $broadcasters = get_field( 'broadcasters', $id ) ? get_field( 'broadcasters', $id ) : [];
          $collapse_id = 'production__' . md5(uniqid(rand(), true));
          $excerpt = get_the_excerpt( $id );
          $feature_image = $this->get_featured_image_by_post_id( $id );
          $permalink = get_the_permalink( $id );
          $reel = get_field( 'reel', $id ) ? get_field( 'reel', $id ) : [];
          $title = get_the_title( $id );


 switch ( $layout ) {

              case 'featured-grid-large':
                $html .= $this->render_production_preview([ 'id' => $id, 'layout' => $layout ]);
                break;

              case 'featured-grid-large':
                $html .= $this->render_production_preview([ 'id' => $id, 'layout' => $layout ]);
                break;

                // ---------------------------------------- Cover
                $html .= '<div class="productions__listing-cover">';
                  $html .= $feature_image ? $this->render_lazyload_image([
                    'classes' => 'productions__listing-cover-image',
                    'background' => true,
                    'duration' => 250,
                    'image' => $feature_image,
                  ]) : '';
                  $html .= $abc_type ? '<span class="productions__listing-cover-abc">' . $abc_type . '</span>' : '';
                  $html .= $title ? '<h2 class="productions__listing-cover-title">' . $title . '</h2>' : '';
                  $html .= '<button class="productions__def" type="button">Play Trailer</button>';
                  $html .= '<button
                    class="productions__listing-cover-button button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#' . $collapse_id .'"
                    aria-expanded="false"
                    aria-controls="' . $collapse_id .'"
                    >' . $this->render_svg_icon("plus") . '</button>';
                $html .= '</div>';

                // ---------------------------------------- Body
                $html .= '<div class="productions__listing-body collapse" id="' . $collapse_id .'">';
                  $html .= '<div class="productions__listing-body-padding">';
                    $html .= $about ? '<div class="productions__listing-about rte">' . $about . '</div>' : '';
                    $html .= $broadcasters ? $this->render_partners([
                      'partners' => $broadcasters,
                      'classes' => 'productions__listing-broadcasters',
                      'title' => 'Airing on',
                    ]) : '';
                  $html .= '</div>';
                $html .= '</div>';

                break;
              case 'grid':
                $html .= $permalink ? '<a class="productions__listing-link link" href="' . $permalink . '" target="_self" title="' . $title . '">' : '';
                  $html .= '<div class="productions__listing-image">';
                    $html .= $feature_image ? $this->render_lazyload_image([ 'background' => true, 'image' => $feature_image ]) : '';
                    $html .= '<div class="productions__listing-nat-geo"></div>';
                  $html .= '</div>';
                  $html .= '<div class="productions__listing-gradient"></div>';
                  $html .= $title ? '<h3 class="productions__listing-title">' . $title . '</h3>' : '';
                $html .= $permalink ? '</a>' : '';
                break;
              case 'grid-large':
                break;
            }

          $html .= '</div>';
