<?php

  /*
  *
  *	Filename: functions.php
  *
  */

  $includes = [
    "tools",
    "advanced-custom-fields",
    "dashboard-customizer",
    "image-sizes",
    "optimization",
    "custom-post-types",
    "custom-theme-base",
    "custom-theme-templates",
    "custom-theme",
    "security",
    "utilities",
    "wysiwyg-formats",
    "enqueue-scripts",
    "enqueue-styles",
    "seo",
  ];

  foreach( $includes as $include ) {
    get_template_part( "functions/{$include}" );
  }

  function custom_default_blocks() {
    $post_type_object = get_post_type_object( "news" );
    $post_type_object->template = [
      [ "acf/hero" ],
      [ "acf/text" ],
      [ "acf/latest-articles" ],
    ];
  }

  add_action( "init", "custom_default_blocks" );



