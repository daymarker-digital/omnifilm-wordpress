<?php

/*
*
*	Filename: footer.php
*
*/

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$post_id = $THEME->get_theme_info('post_ID');
$block_name = 'footer';
$footer = get_field( 'footer', 'options' ) ?: [];
$social_accounts = $footer['social'] ?? [];

?>

</main>

<footer id="<?= $block_name; ?>" class="<?= $block_name; ?>">
  <div class="container-fluid"><div class="row">

    <div class="col-12">
      <span class="<?= $block_name; ?>__call-out font-weight--700 uppercase">Let's Talk</span>
    </div>

    <div class="col-10 offset-1">
      <div class="<?= $block_name; ?>__main">

        <div class="row row--inner row--top">
          <div class="col-12 col-lg-4 col-xl-3">
            <h2 class="<?= $block_name; ?>__heading heading heading--section-title font-weight--400 uppercase">
              <span class="heading__title">Contact</span>
              <span class="heading__hr"></span>
            </h2>
          </div>
          <div class="col-12 col-lg-4 col-xl-3">
            <h3 class="<?= $block_name; ?>__heading heading heading--tertiary font-weight--300">Omnifilm Entertainment</h3>
            <div class="<?= $block_name; ?>__content content">
              <span class="super"><a href="tel:604.681.6543">604.681.6543</a></span>
              <span class="super"><a href="mailto:info@omnifilm.com">info@omnifilm.com</a></span>
              <span>Suite 204 - 111 Water Street<br>Vancouver BC, Canada - V6B 1A7</span>
              <span><a href="<?= $THEME->get_google_maps_directions_link([ 'full_address' => '204-111 Water Street, Vancouver, BC, Canada, V6B 1A7, Omnifilm Entertainment' ]); ?>" target="_blank" title="Get Directions">Get Directions</a></span>
            </div>
          </div>
        </div>
        <!-- /.row--top -->

        <div class="row row--inner row--bottom">
          <div class="col-12 col-lg-4 col-xl-3">
            <h3 class="<?= $block_name; ?>__heading heading heading--tertiary font-weight--300">Omnifilm Releasing</h3>
            <div class="<?= $block_name; ?>__content content">
              <span>Suite 206 - 111 Water Street<br>Vancouver BC, Canada<br>V6B 1A7</span>
              <span><a href="tel:604.681.6543">604.681.6543</a></span>
              <span><a href="mailto:sales@omnifilm.com">sales@omnifilm.com</a></span>
            </div>
          </div>
          <div class="col-12 col-lg-4 col-xl-3">
            <h3 class="<?= $block_name; ?>__heading heading heading--tertiary font-weight--300">Omnifilm Post</h3>
            <div class="<?= $block_name; ?>__content content">
              <span>Suite 350 - 2285 Clark Drive<br>Vancouver BC, Canada<br>V5N 3G9</span>
              <span><a href="tel:604.684.8025">604.684.8025</a></span>
              <span><a href="mailto:post@omnifilm.com">post@omnifilm.com</a></span>
            </div>
          </div>
          <div class="col-12 col-lg-4 col-xl-3">
            <h3 class="<?= $block_name; ?>__heading heading heading--tertiary font-weight--300">Opportunities</h3>
            <nav class="<?= $block_name; ?>__navigation navigation navigation--opportunities">
              <?= $THEME->render_wp_navigation([ 'current_id' => $post_id, 'menu_name' => 'Opportunities', 'wrapper' => false ]); ?>
            </nav>
          </div>
          <div class="col-12 col-lg-4 col-xl-3">
            <h3 class="<?= $block_name; ?>__heading heading heading--tertiary font-weight--300">Find us on</h3>
            <?= $social_accounts ? $THEME->render_social_accounts([ 'accounts' => $social_accounts ]) : ''; ?>
          </div>
        </div>
        <!-- /.row--bottom -->

      </div>
    </div>

    <div class="col-12">
      <div class="<?= $block_name; ?>__legal legal">
        <span>&copy; <?= date('Y') . ' ' . get_bloginfo( 'name' ); ?></span>
      </div>
    </div>

  </div></div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
