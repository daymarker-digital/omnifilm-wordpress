if ( !empty($team_members) ) {
              $html .= '<div class="' . $block_name . '__grid">';
                foreach ( $team_members as $index => $item ) {

                  $id = $item['team_member']->ID ?? 0;
                  $about = get_field( 'about', $id ) ?: [];
                  $name = $item['team_member']->post_title ?? '';
                  $profile_picture = $this->get_featured_image_by_post_id($id) ?: [];
                  $role = get_field( 'role', $id ) ?: [];
                  $modal_id = $this->get_unique_id("team-member-modal-{$id}");

                  if ( $name ) {

                    $html .= '<div class="' . $block_name . '__item" data-target-modal-id="' . $modal_id . '">';
                      $html .= $this->render_card([
                        'heading' => $name,
                        'image' => $profile_picture,
                        'link_type' => 'Read Bio',
                        'link_type' => 'modal',
                        'subheading' => $role,
                      ]);
                    $html .= '</div>';

                    $html .= '<div class="' . $block_name . '__modal modal fade" id="' . $modal_id . '" tabindex="-1" aria-hidden="true">';
                      $html .= '<div class="' . $block_name . '__modal-dialog modal-dialog">';
                        $html .= '<div class="' . $block_name . '__modal-content modal-content">';

                          $html .= '<button class="' . $block_name . '__modal-button modal-button-close button" type="button" data-bs-dismiss="modal" aria-label="Close">';
                            $html .= $this->render_svg_icon("plus");
                          $html .= '</button>';

                          if ( $profile_picture ) {
                            $html .= '<div class="' . $block_name . '__modal-picture modal-picture">';
                              $html .= $this->render_lazyload_image([
                                'alt_text' => $name,
                                'image' => $profile_picture,
                                'preload' => true,
                              ]);
                            $html .= '</div>';
                          }

                          $html .= '<div class="' . $block_name . '__modal-body modal-body">';
                            $html .= '<strong class="' . $block_name . '__modal-name modal-team-member-name">' . $name . '</strong>';
                            $html .= $role ? '<div class="' . $block_name . '__modal-role modal-team-member-role">' . $role . '</div>' : '';
                            $html .= $about ? '<div class="' . $block_name . '__modal-bio modal-team-member-bio messsage">' . $about . '</div>' : '';
                          $html .= '</div>';


                        $html .= '</div>';
                      $html .= '</div>';
                    $html .= '</div>';

                  }

                }
              $html .= '</div>';
            }