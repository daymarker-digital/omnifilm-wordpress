<?php

/*
*
*	Filename: single.php
*
*/

get_header();

if ( have_posts() ) {
  while ( have_posts() ) {

    // init post
    the_post();

    // display post
    the_content();


  }
}

get_footer();

?>
