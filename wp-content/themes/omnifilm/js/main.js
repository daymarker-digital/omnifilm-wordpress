import Credits from './modules/credits';
import Forms from './modules/forms';
import Galleries from './modules/galleries';
import MobileMenu from './modules/mobileMenu';
import Productions from './modules/productions';
import Scrolling from './modules/scrolling';
import Sizing from './modules/sizing';
import Tools from './modules/tools';
import Video from './modules/video';

AOS.init();
Sizing.init();
Credits.init();

window.addEventListener( 'load', (event) => {

  AOS.refresh();
  Forms.init();
  Galleries.init();
  MobileMenu.init();
  Productions.init();
  Scrolling.init();
  Sizing.init();
  Video.init();

  ( document.querySelectorAll( '.card-team-member' ) || [] ).forEach( card => {
    card.addEventListener( 'click', (event) => {
      let modalID = card.dataset.targetModalId || '';
      let modalElement = document.getElementById( modalID );
      let modal = new bootstrap.Modal(modalElement, {});
      modal.show();
    });
  });

});

window.addEventListener( 'resize', Tools.throttle(() => {
  Sizing.init();
}, 300));

window.addEventListener( 'scroll', Tools.throttle(() => {
  Scrolling.init();
}, 300));


