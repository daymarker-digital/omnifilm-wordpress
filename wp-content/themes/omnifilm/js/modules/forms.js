import Tools from 'tools';

const config = { debug: false, name: 'forms.js', version: '1.1' };
const maxMB = 25.0;
const formFiles = [];
const messages = {
  error: {
    429: "Too many attempts to recover password. Please try again later.",
  },
  success: {
    password_recover: "We've sent you an email with a link to update your password.",
    register_account: "Thanks for registering! You will be redirected shortly."
  },
};

const onFormFocus = () => {

  ( document.querySelectorAll('.form') || [] ).forEach( form => {

    form.addEventListener('focusin', (e) => {
      if ( e.target.closest('.form__field') ) {
        e.target.closest('.form__field').classList.add("in-focus");
      }
    });

    form.addEventListener('focusout', (e) => {
      if ( e.target.closest('.form__field') && !e.target.value ) {
        e.target.closest('.form__field').classList.remove("in-focus");
      }
    });

  });

};

const isFieldValid = ( $field = false ) => {

  let value = '';
  let isValid = false;
  let name = $field.name || 'no-name';
  let type = $field.type || 'no-type';
  let nodeName = $field.nodeName || 'no-node-name';

  switch ( nodeName ) {
    case 'INPUT':
      switch ( type ) {

        case 'checkbox':
          isValid = $field.checked;
          break;

        case 'email':
          if ( validator.isEmail( $field.value ) ) {
            isValid = true;
          }
          break;

        case 'file':
          break;

        case 'radio':
          break;

        case 'tel':
          if ( validator.isMobilePhone( $field.value ) ) {
            isValid = true;
          }
          break;

        case 'password':
        case 'text':
          if ( ! validator.isEmpty( $field.value ) ) {
            isValid = true;
          }
          break;

      }
      break;
    case 'TEXTAREA':
      if ( ! validator.isEmpty( $field.value ) ) {
        isValid = true;
      }
      break;
  }

  if ( isValid ) {
    Tools.removeClass( 'error', [ $field.closest('.form__field') ] );
  } else {
    Tools.addClass( 'error', [ $field.closest('.form__field') ] );
  }

  return isValid;

};

const isFormValid = ( $form = false ) => {

  let isValid = true;
  let requiredFields = $form.querySelectorAll('.required') || [];

  for ( let i = 0; i < requiredFields.length; i++ ) {
    if ( !isFieldValid( requiredFields[i] ) ) {
      isValid = false;
    }
  }

  return isValid;

};

const hasSpam = ( $form = false ) => {

  let inputs = $form.querySelectorAll('input.rude') || [];

  for ( let i = 0; i < inputs.length; i++ ) {
    if ( inputs[i].value ) {
      return true;
    }
  }

  return false;

};

const underFileSizeLimit = ( $form = false ) => {

  let totalMB = 0;
  let inputs = $form.querySelectorAll('input[type="file"]') || [];
  formFiles.length = 0;

  for ( let i = 0; i < inputs.length; i++ ) {

    let input = inputs[i];
    let field = input.closest('.field');
    let fieldErrorMessage = field.querySelector('.form__error-message') || false;
    let files = input.files || {};
    let message = '';
    for ( let key in files ) {
      totalMB += files[key].size || 0;
      if ( files[key].size ) {
        formFiles.push( files[key] );
      }
    }
    totalMB = totalMB/1024/1024;
    if ( totalMB > maxMB ) {
      message = `The total file size (${totalMB.toFixed(1)}MB) is larger than ${maxMB}.0MB`;
      field.classList.add( 'error' );
      if ( fieldErrorMessage ) fieldErrorMessage.innerHTML = message;
      return false;
    } else {
      if ( fieldErrorMessage ) fieldErrorMessage.innerHTML = message;
      field.classList.remove( 'error' );
    }
  }

  return true;

};

const submitForm = ( $form = false ) => {

  let formAction = $form.action || false;
  let formData = new FormData( $form );
  let redirectURL = $form.dataset.redirectUrl || false;

  formFiles.forEach( ( file, index ) => {
    formData.append( 'files' , file );
  });

  $form.classList.add('posting');

  axios.post( formAction, formData )
  .then(( data ) => {
    if ( config.debug ) console.log( 'then() data :: ', data );
    if ( redirectURL ) {
      window.location.replace( redirectURL );
    }
  })
  .catch(( data ) => {
    console.log( 'submitForm() Error :: ', data );
  }).finally(() => {
    $form.reset();
    $form.classList.remove('posting');
  });

};

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);

    onFormFocus();

    document.querySelectorAll('.js--validate-me [type="submit"]').forEach( button => {

      button.addEventListener('click', ( event ) => {

        event.preventDefault();

        let form = button.closest( 'form' );
        let formRedirectURL = false;
        let formValid = isFormValid( form );
        let formHasSpam = hasSpam( form );
        let formUnderFileSizeLimit = underFileSizeLimit( form );

        if ( config.debug ) console.log( `${config.name}.init()`, { formValid, formHasSpam, formUnderFileSizeLimit } );

        if ( formValid && !formHasSpam && formUnderFileSizeLimit ) {
          submitForm( form );
        }

      });

    });

  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
