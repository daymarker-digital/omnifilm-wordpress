const info = { company : 'Omnifilm Entertainment', tagline : '"We Produce Premium Content"',  version : '2.2' };

const init = () => {
  console.log( `${info.company} - ${info.tagline} - Version ${info.version}` );
  console.log( 'Site by Daymarker Digital – https://daymarker.digital/' );
};

export default { init }

