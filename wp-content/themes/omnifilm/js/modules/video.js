const config = { debug: false, name: 'video.js', version: '1.0' };

const autoPlayIframe = ( element = false ) => {
  if ( element ) {
    let source = element.getAttribute("src") || element.getAttribute("data-src") || "";
    let sourceWithParams = source.indexOf("?") >= 0 ? true : false;
    if ( sourceWithParams ) {
      source += "&";
    } else {
      source = "?";
    }
    element.setAttribute( "src", `${source}autoplay=1` );
  }
}

const getVideoElement = ( type = '', parent = false ) => {
  if ( parent && type ) {
    switch ( type ) {
      case "video-file": {
        return parent.querySelector("video") || false;
        break;
      }
      case "vimeo": {
        return parent.querySelector("iframe") || false;
        break;
      }
    }
  }
  return false;
}

const onClickPlayVideo = () => {
  ( document.querySelectorAll(".js--click-to-play-video") || [] ).forEach( element => {
    element.addEventListener( 'click', () => {

      let videoType = element.dataset.videoType || '';
      let videoElement = getVideoElement( videoType, element );
      let videoPlaceholder = element.querySelector(".video__placeholder") || false;

      if ( videoElement && videoPlaceholder ) {
        anime({
          targets: videoPlaceholder,
          opacity: {
            value: 0,
            duration: 500,
            easing: 'easeOutExpo',
          },
          complete: function(anim) {
            setTimeout( () => {

              videoPlaceholder.remove();

              switch ( videoType ) {
                case "video-file": {
                  videoElement.play();
                  break;
                }
                case 'vimeo': {
                  autoPlayIframe(videoElement);
                  break;
                }
              }

            }, 250 );
          }
        });
      }

    });
  });
}

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    onClickPlayVideo();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
