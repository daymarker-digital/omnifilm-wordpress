import Tools from 'tools';

const config = { debug: false, name: 'mobileMenu.js', version: '1.0' };
const elements = document.querySelectorAll( 'body, footer, header, main' ) || [];
const triggers = document.querySelectorAll('.js--mobile-menu-trigger') || [];

const onClickCloseMobileMenu = () => {
  document.body.addEventListener( 'click', event => {
    let mobileMenu = event.target.closest( '.mobile-menu' ) ? true : false;
    let activeButton = event.target.closest( '.js--mobile-menu-trigger.is-active' ) ?  true : false;
    if ( !mobileMenu && !activeButton ) {
      Tools.removeClass( 'mobile-menu--active', elements );
      Tools.removeClass( 'is-active', triggers );
    }
  });
};

const onClickToggleMobileMenu = () => {
  triggers.forEach( trigger => {
    trigger.addEventListener('click',() => {
      Tools.toggleClass( 'mobile-menu--active', elements );
      trigger.classList.toggle( 'is-active' );
    });
  });
};

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
  onClickCloseMobileMenu();
  onClickToggleMobileMenu();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
