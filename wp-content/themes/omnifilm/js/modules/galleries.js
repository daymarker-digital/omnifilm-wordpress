const config = { debug: false, name: 'galleries.js', version: '1.0' };

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    lightGallery(document.getElementById('collapsible__photos'), {
      selector: '.photos__item img',
    });
    lightGallery(document.getElementById('production__tabbed-content'), {
      selector: '.photos__item img',
    });
    lightGallery(document.getElementById('watch-omni-reel'), {
      selector: 'this',
      plugins: [ lgAutoplay, lgFullscreen, lgVideo ],
      videojs: true,
      autoplay: true,
      autoplayControls: false,
      controls: false,            // prev/next buttons display options,
      counter: false,             // show total number of images and index number of currently displayed image
      download: false,
      licenseKey: "0000-0000-000-0000",
    });
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
