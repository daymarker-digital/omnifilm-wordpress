import Breakpoints from 'breakpoints';

const config = { debug: false, name: 'productions.js', version: '1.0' };

const updateCardStateOnCollapseEvent = () => {
  ( document.querySelectorAll('.card-production__body.collapse') || [] ).forEach( element => {
    element.addEventListener('hide.bs.collapse', function () {
      element.closest('.card-production').classList.remove('expanded');
    });
    element.addEventListener('show.bs.collapse', function () {
      element.closest('.card-production').classList.add('expanded');
    });
  });
}

const toggleCardReel = () => {
  ( document.querySelectorAll( '.js--toggle-card-production-reel' ) || [] ).forEach( button => {
    button.addEventListener( 'click', (e) => {
      let reelElement = button.closest( '.card-production' ).querySelector( '.card-production__body-reel' ) || false;
      if ( reelElement ) reelElement.classList.toggle( 'd-none' );
    });
  });
}

const onHoverShowProductionFeatureImageAlt = () => {
  ( document.querySelectorAll(".card-production") || [] ).forEach( element => {

    let elClassList = Array.from(element.classList || []);
    let elContainer = element.closest(".productions-listing__listing") || false;
    let elFeatureImageAltSrc = element.dataset.featureImageAltSrc || false;
    let elGrid = elClassList.includes("grid") ? true : false;

    if ( elContainer ) {

      if ( elGrid ) {
        element.addEventListener('mouseenter', (event) => {
          elContainer.classList.add("hover");
          element.classList.add("active");
        });
        element.addEventListener('mouseleave', (event) => {
          elContainer.classList.remove("hover");
          element.classList.remove("active");
        });
      }

      element.addEventListener('mouseenter', (event) => {
        if ( elFeatureImageAltSrc && window.innerWidth >= Breakpoints.sizes.lg ) {
          element.classList.add("spotlight");
          elContainer.classList.add("hover");
          elContainer.style.backgroundImage = `url('${elFeatureImageAltSrc}')`;
        }
      });
      element.addEventListener('mouseleave', (event) => {
        if ( elFeatureImageAltSrc && window.innerWidth >= Breakpoints.sizes.lg ) {
          element.classList.remove("spotlight");
          elContainer.classList.remove("hover");
          elContainer.style.backgroundImage = ``;
        }
      });

    }

  });
};

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
  onHoverShowProductionFeatureImageAlt();
  toggleCardReel();
  updateCardStateOnCollapseEvent();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
