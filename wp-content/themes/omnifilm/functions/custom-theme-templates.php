<?php

class CustomThemeTemplates extends CustomThemeBase {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Custom Theme Templates';
  private $version = '2.0';

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Instance
  //////////////////////////////////////////////////////////
  */

  // --------------------------- Button
  public function render_button( $button = [] ) {

    $html = '';
    $block_name = 'button';

    if ( $button ) {

      $link = $link_attachment = $link_category = $link_external = $link_page = $link_type = $title = false;
      $appearance = 'primary';
      $target = '_self';

      extract( $button );

      switch ( $link_type ) {
        case 'attachment':
          $link = ( isset($link_attachment['url']) ) ? $link_attachment['url'] : false;
          $target = '_blank';
          break;
        case 'category':
          $link = get_category_link( $link_category );
          break;
        case 'external':
          $link = $link_external;
          $target = '_blank';
          break;
        case 'page':
          $link = get_permalink( $link_page );
          break;
      }

      if ( $link && $title ) {
        $html .= '<a
          class="' . $block_name . '"
          href="' . $link . '"
          target="' . $target . '"
          title="' . $title . '"
          data-appearance="' . $appearance . '"
        >' . $title . '</a>';
      }

    }

    return $html;

  }

  // --------------------------- Card | News
  public function render_card_news_by_id( $id = 0 ) {

    $block_name = "card-news";
    $cta_icon = $this->render_svg_icon("plus-circle");
    $title = get_the_title( $id ) ?: '';
    $excerpt = get_the_excerpt( $id ) ?: '';
    $featured_image = $this->get_featured_image_by_post_id( $id );
    $featured_image_lazy = $this->render_nu_lazyload_image([ 'image' => $featured_image ]);
    $permalink = get_the_permalink( $id ) ?: '';
    $post_date = get_the_date( 'd M Y', $id ) ?: '';
    $post_date_time =  get_the_date( 'c', $id ) ?: '';

    return "
      <article class='{$block_name} card'>
        <a class='card__link' href='{$permalink}' target='_self' title='{$title}'>
          <div class='card__wrapper'>
            <div class='card__image'>
              {$featured_image_lazy}
              <time class='card__image-caption' datetime='{$post_date_time}' itemprop='datePublished'>{$post_date}</time>
            </div>
            <div class='card__content'>
              <strong class='card__title'>{$title}</strong>
              " . ( $excerpt ? "<div class='card__body-copy'>{$excerpt}</div>" : "" ) . "
            </div>
            <div class='card__cta'>
              <div class='card__button'>
                <span class='card__button-title'>Read More</span>
                " . ( $cta_icon ? "<span class='card__button-icon'>{$cta_icon}</span>" : "" ) . "
              </div>
            </div>
           </div>
        </a>
      </article>
    ";

  }

  // --------------------------- Card | Team Member
  public function render_card_team_member_by_id( $id = 0 ) {

    $about = get_field( "about", $id ) ?: [];
    $block_name = "card-team-member";
    $button_icon = $this->render_svg_icon("plus");
    $cta_icon = $this->render_svg_icon("plus-circle");
    $name = get_the_title($id) ?: "";
    $profile_picture = $this->get_featured_image_by_post_id($id) ?: [];
    $profile_picture_lazy = $this->render_nu_lazyload_image([ 'image' => $profile_picture ]);
    $role = get_field( "role", $id ) ?: [];
    $modal_id = $this->get_unique_id("{$block_name}-modal-{$id}");

    return "
      <article class='{$block_name} card' data-target-modal-id='{$modal_id}'>
        <div class='card__wrapper'>
          <div class='card__image'>{$profile_picture_lazy}</div>
          <div class='card__content'>
            <strong class='card__title'>{$name}</strong>
            " . ( $role ? "<div class='card__body-copy'>{$role}</div>" : "" ) . "
          </div>
          <div class='card__cta'>
            <div class='card__button'>
              <span class='card__button-title'>Read Bio</span>
              " . ( $cta_icon ? "<span class='card__button-icon'>{$cta_icon}</span>" : "" ) . "
            </div>
          </div>
        </div>
      </article>

      <div class='{$block_name} modal fade' id='{$modal_id}' tabindex='-1' aria-hidden='true'>
        <div class='modal-dialog'>
          <div class='modal-content'>
            <button class='modal-button-close button' type='button' data-bs-dismiss='modal' aria-label='Close'>{$button_icon}</button>
            <div class='modal-picture'>{$profile_picture_lazy}</div>
            <div class='modal-body'>
              <strong class='modal-team-member-name'>{$name}</strong>
              " . ( $role ? "<div class='modal-team-member-role'>{$role}</div>" : "" ) . "
              " . ( $about ? "<div class='modal-team-member-bio messsage'>{$about}</div>" : "" ) . "
            </div>
          </div>
        </div>
      </div>
    ";

  }

  // --------------------------- Card
  public function render_card( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'card',
        'classes' => '',
        'heading' => '',
        'html' => '',
        'image' => [],
        'link_title' => 'Read More',
        'link_type' => 'internal',
        'link_url' => '',
        'subheading' => '',
      ],
      $params
    ));

    if ( $heading && $image ) {
      $html .= '<div class="card">';
        $html .= '<div class="card__image">';
          $html .= $this->render_nu_lazyload_image([ 'image' => $image ]);
        $html .= '</div>';
        $html .= '<div class="card__content">';
          $html .= '<strong class="card__heading">' . $heading . '</strong>';
          $html .= $subheading ? '<span class="card__subheading">' . $subheading . '</span>' : '';
        $html .= '</div>';
        $html .= '<div class="card__cta">';
          switch ( $link_type ) {
            case 'external':
            case 'internal':
              $link_target = ( $link_type == 'internal' ) ? '_self' : '_blank';
              $html .= '<a class="card__cta-link link" href="' . $link_url . '" target="' . $link_target . '" title="' . $link_title . '">';
                $html .= '<span class="link__title">' . $link_title . '</span>';
                $html .= '<span class="link__icon">' . $this->render_svg_icon("plus-circle") . '</span>';
              $html .= '</a>';
              break;
            case 'modal':
              $html .= '<div class="card__cta-link link">';
                $html .= '<span class="link__title">' . $link_title . '</span>';
                $html .= '<span class="link__icon">' . $this->render_svg_icon("plus-circle") . '</span>';
              $html .= '</div>';
              break;
          }
        $html .= '</div>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Call to Action
  public function render_cta( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        "html" => "",
        "link" => "",
        "link_internal" => "",
        "link_external" => "",
        "rel" => "",
        "style" => "outlined-arrow",
        "target" => "_self",
        "title" => "",
        "type" => "internal",
      ],
      $params
    ));

    $classes = "outlined-plus" == $style ? "button--secondary" : "button--primary";

    switch( $type ) {
      case 'external': {
        $link = $link_internal;
        $rel = 'noopener';
        $target = '_blank';
        break;
      }
      case 'internal': {
        $link = $link_internal;
        break;
      }
    }

    if ( $link && $title ) {
      $html = $this->render_link([
        "block_name" => "button",
        "classes" => "button {$classes}",
        "link" => $link,
        "title" => $title,
        "style" => $style,
        "target" => $target,
        "rel" => $rel
      ]);
    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  //// Flexible Content
  //////////////////////////////////////////////////////////
  */

  public function render_flexible_content( $params = [] ) {

    // ---------------------------------------- Defaults
    $defaults = [
      'block_name' => 'flexible-content',
      'current_id' => '',
      'html' => '',
    ];

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $flexible_content = get_field( 'flex_content', $current_id ) ? get_field( 'flex_content', $current_id ) : [];

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_classes . '">';

      if ( $flexible_content ) {
        foreach ( $flexible_content as $index => $section ) {

          $layout = $section['acf_fc_layout'] ?? '';
          $enable = $section['enable'] ?? false;

          if ( $enable ) {
            $html .= '<section class="' . $block_name . '__section section section--' . $layout . '" data-index="' . $index . '">';
              switch ( $layout ) {

                case 'hero': {
                  $html .= $this->render_flexible_content_hero([ 'index' => $index, 'section' => $section ]);
                  break;
                }

                case 'navigation': {
                  $menu_name = $section['menu_name'] ?? '';
                  $title = get_the_title( $current_id );
                  $html .= '<h1 class="flexible-content__title title title--page-title d-lg-none">' . $title .'</h1>';
                  $html .= $this->render_wp_navigation([
                    'classes' => 'flexible-content__navigation',
                    'current_id' => $current_id,
                    'menu_name' => $menu_name
                  ]);
                  break;
                }

                case 'partners': {
                  $partners = get_field( 'partners', 'options' ) ?: [];
                  $title = $section['title'] ?? $partners['title'] ?? '';
                  $broadcasters = $partners['broadcasters'] ?? $broadcasters;
                  $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
                    $html .= $this->render_partners([ 'partners' => $broadcasters, 'title' => $title ]);
                  $html .= $this->render_bs_container( 'closed' );
                  break;
                }

                case 'productions-listing': {
                  $html .= $this->render_productions_listing([ 'section' => $section ]);
                  break;
                }

                case 'screener-request': {
                  $html .= $this->render_screener_request();
                  break;
                }

                case 'team-members': {
                  $html .= $this->render_bs_container( 'open', 'col-12' );
                    $html .= $this->render_flexible_content_team_members([ 'section' => $section ]);
                  $html .= $this->render_bs_container( 'closed' );
                  break;
                }

                case 'text-grid': {
                  $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
                    $html .= $this->render_flexible_content_text_grid([ 'section' => $section ]);
                  $html .= $this->render_bs_container( 'closed' );
                  break;
                }

                case 'video': {
                  $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
                    $html .= $this->render_flexible_content_video([ 'section' => $section ]);
                  $html .= $this->render_bs_container( 'closed' );
                  break;
                }

                case 'wysiwyg': {
                  $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
                    $html .= $this->render_flexible_content_wysiwyg([ 'section' => $section ]);
                  $html .= $this->render_bs_container( 'closed' );
                  break;
                }

              }
            $html .= '</section>';
          }

        }
      }
    $html .= '</div>';

    return $html;

  }

  // --------------------------- Hero
  public function render_flexible_content_hero( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'hero',
        'current_id' => '',
        'html' => '',
        'index' => '',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $cta = $section['hero']['cta'] ?? [];
    $heading = $section['hero']['heading'] ?? '';
    $id = $this->get_unique_id("hero--");
    $background = $section['hero']['background'] ?? 'image';
    $image = $section['hero']['image'] ?? [];
    $video_file = $section['hero']['video_file'] ?? [];
    $video_id = $section['hero']['video_id'] ?? '';
    $video_ratio_width = (float)$section['hero']['video_ratio_width'] ?? 0;
    $video_ratio_height = (float)$section['hero']['video_ratio_height'] ?? 0;

    $a = $video_ratio_height/$video_ratio_width * 100;
    $b = $video_ratio_width/$video_ratio_height * 100;

    $html .= "
      <style>
        #{$id} iframe {
          height: {$a}vw;
          min-width: {$b}vh;
          min-height: 100vh;
          width: 100vw;
        }
      </style>
    ";

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_name . '" id="' . $id . '">';

      $html .= $image ? $this->render_lazyload_image([
        'background' => true,
        'classes' => $block_name . '__image',
        'duration' => 300,
        'image' => $image
      ]) : '';

      $html .= '<div class="' . $block_name . '__' . $background . '">';
        switch ( $background ) {
          case 'embedded-video':
            $html .= $this->render_lazyload_iframe([
              'background' => true,
              'video_id' => $video_id,
              'video_source' => 'vimeo',
            ]);
            break;
          case 'video-file':
            $html .= $video_file ? $this->render_lazyload_video([
              'autoplay' => true,
              'background' => true,
              'classes' => $block_name . '__video',
              'loop' => true,
              'muted' => true,
              'video' => $video_file,
            ]) : '';
            break;
        }
      $html .= '</div>';

      if ( $heading || $cta ) {
        $html .= '<div class="' . $block_name . '__main">';
          $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
            $html .= $heading ? '<h1 class="' . $block_name . '__heading">' . $heading . '</h1>' : '';
            $html .= $cta ? $this->render_cta([
              'classes' => $block_name . '__cta',
              'cta' => $cta,
              'justification' => 'center',
            ]) : '';
          $html .= $this->render_bs_container( 'closed' );
        $html .= '</div>';
      }

      $html .= '<div class="' . $block_name . '__gradient"></div>';

      if ( 0 === $index ) {
        $partners = get_field( 'partners', 'options' ) ?: [];
        $title = $partners['title'] ?? $title;
        $broadcasters = $partners['broadcasters'] ?? $broadcasters;
        $html .= $this->render_partners([
          'partners' => $broadcasters,
          'classes' => $block_name . '__broadcasters',
          'title' => $title
        ]);
      }

    $html .= '</div>';

    return $html;

  }

  // --------------------------- Productions
  public function render_flexible_content_productions( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'productions',
        'current_id' => '',
        'html' => '',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $categories = $section['categories'] ?: [];
    $cta = $section['cta'] ?: [];
    $layout = $section['layout'] ?: 'grid';
    $limit = $section['limit'] ?: 4;
    $order = $section['order'] ?: 'ASC';
    $order_by = $section['order_by'] ?: 'title';

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_name . '">';

      $html .= $this->render_productions_listing([
        'categories' => $categories,
        'layout' => $layout,
        'limit' => $limit,
        'order' => $order,
        'order_by' => $order_by,
      ]);
      $html .= $cta ? $this->render_cta([ 'classes' => 'productions__cta', 'cta' => $cta, 'justification' => 'center' ]) : '';

    $html .= '</div>';

    return $html;

  }

  // --------------------------- Team Members
  public function render_flexible_content_team_members( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'team-members',
        'current_id' => '',
        'html' => '',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $block_name;
    $layout = $section['layout'] ?: 'grid';
    $heading = $section['heading'] ?: 'grid';
    $team_members = $section['team_members'] ?: [];

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_name . '">';
      $html .= '<div class="' . $block_name . '__main">';

          $html .= $heading ? '<strong class="' . $block_name . '__heading title title--page-title">' . $heading . '</strong>' : '';

          if ( !empty($team_members) ) {
            $html .= '<div class="' . $block_name . '__grid">';
              foreach ( $team_members as $index => $item ) {

                $id = $item['team_member']->ID ?? 0;
                $about = get_field( 'about', $id ) ?: [];
                $name = $item['team_member']->post_title ?? '';
                $profile_picture = $this->get_featured_image_by_post_id($id) ?: [];
                $role = get_field( 'role', $id ) ?: [];
                $modal_id = $this->get_unique_id("team-member-modal-{$id}");

                if ( $name ) {

                  $html .= '<div class="' . $block_name . '__item" data-target-modal-id="' . $modal_id . '">';
                    $html .= $this->render_card([
                      'heading' => $name,
                      'image' => $profile_picture,
                      'link_type' => 'Read Bio',
                      'link_type' => 'modal',
                      'subheading' => $role,
                    ]);
                  $html .= '</div>';

                  $html .= '<div class="' . $block_name . '__modal modal fade" id="' . $modal_id . '" tabindex="-1" aria-hidden="true">';
                    $html .= '<div class="' . $block_name . '__modal-dialog modal-dialog">';
                      $html .= '<div class="' . $block_name . '__modal-content modal-content">';

                        $html .= '<button class="' . $block_name . '__modal-button modal-button-close button" type="button" data-bs-dismiss="modal" aria-label="Close">';
                          $html .= $this->render_svg_icon("plus");
                        $html .= '</button>';

                        if ( $profile_picture ) {
                          $html .= '<div class="' . $block_name . '__modal-picture modal-picture">';
                            $html .= $this->render_lazyload_image([
                              'alt_text' => $name,
                              'image' => $profile_picture,
                              'preload' => true,
                            ]);
                          $html .= '</div>';
                        }

                        $html .= '<div class="' . $block_name . '__modal-body modal-body">';
                          $html .= '<strong class="' . $block_name . '__modal-name modal-team-member-name">' . $name . '</strong>';
                          $html .= $role ? '<div class="' . $block_name . '__modal-role modal-team-member-role">' . $role . '</div>' : '';
                          $html .= $about ? '<div class="' . $block_name . '__modal-bio modal-team-member-bio messsage">' . $about . '</div>' : '';
                        $html .= '</div>';


                      $html .= '</div>';
                    $html .= '</div>';
                  $html .= '</div>';

                }

              }
            $html .= '</div>';
          }

      $html .= '</div>';
    $html .= '</div>';

    return $html;

  }

  // --------------------------- Text Grid
  public function render_flexible_content_text_grid( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'text-grid',
        'current_id' => '',
        'html' => '',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $block_name;
    $heading = isset($section['heading']) && !empty($section['heading']) ? $section['heading'] : '';
    $layout = isset($section['layout']) && !empty($section['layout']) ? $section['layout'] : 'not-set';
    $content = isset($section['content']) && !empty($section['content']) ? $section['content'] : [];

    // ---------------------------------------- Template
    if ( $heading || $content ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $heading ? '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>' : '';
        if ( $content ) {
          $html .= '<div class="' . $block_name . '__content" data-layout="' . $layout . '">';
            foreach ( $content as $i => $block ) {

              $block_heading = isset($block['heading']) && !empty($block['heading']) ? $block['heading'] : '';
              $block_content = isset($block['content']) && !empty($block['content']) ? $block['content'] : '';

              if ( $block_heading || $block_content ) {
                $html .= '<div class="' . $block_name . '__content-block">';
                  $html .= $block_heading ? '<h3 class="' . $block_name . '__content-block-heading heading">' . $block_heading . '</h3>' : '';
                  $html .= $block_content ? '<div class="' . $block_name . '__content-block-rte rte">' . $block_content . '</div>' : '';
                $html .= '</div>';
              }

            }
          $html .= '</div>';
        }
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Video
  public function render_flexible_content_video( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'video',
        'current_id' => '',
        'html' => '',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $block_name;
    $file = isset($section['file']) && !empty($section['file']) ? $section['file'] : [];
    $placeholder = isset($section['placeholder']) && !empty($section['placeholder']) ? $section['placeholder'] : [];
    $block_classes .= $placeholder ? ' js--click-to-play' : '';

    // ---------------------------------------- Template
    if ( $file ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $this->render_lazyload_video([
          'classes' => $block_name . '__file',
          'controls' => true,
          'video' => $file
        ]);
        if ( $placeholder ) {
          $html .= '<div class="' . $block_name . '__placeholder">';
            $html .= $this->render_lazyload_image([
              'background' => true,
              'classes' => $block_name . '__placeholder-image',
              'duration' => 250,
              'image' => $placeholder
            ]);
            $html .= '<div class="' . $block_name . '__placeholder-play-icon">' . $this->render_svg_icon("play") . '</div>';
          $html .= '</div>';
        }
      $html .= '</div>';
    } else {
      $html .= '<!-- No Video File! -->';
    }

    return $html;

  }

  // --------------------------- WYSIWYG
  public function render_flexible_content_wysiwyg( $params = [] ) {

    // ---------------------------------------- Defaults
    $defaults = [
      'block_name' => 'wysiwyg',
      'current_id' => '',
      'html' => '',
      'section' => [],
    ];

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Data
    $content = isset($section['content']) && !empty($section['content']) ? $section['content'] : '';
    $cta = isset($section['cta']) && !empty($section['cta']) ? $section['cta'] : [];

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_name . '">';
      $html .= $content ? '<div class="' . $block_name . '__main rte">' . $content . '</div>' : '';
      $html .= $cta ? $this->render_cta([ 'classes' => $block_name . '__cta', 'cta' => $cta, 'justification' => 'center' ]) : '';
    $html .= '</div>';

    return $html;

  }

  // --------------------------- Form Field
  public function render_form_field( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accept' => '',
        'block_name' => 'form',
        'disabled' => false,
        'error_message' => [
          'checkbox' => '',
          'email' => 'Please enter a valid email address',
          'file' => 'File type or file size incorrect',
          'tel' => 'Please enter a valid phone number',
          'text' => 'This field cannot be blank',
        ],
        'html' => '',
        'label' => '',
        'multiple' => false,
        'name' => '',
        'placeholder' => '',
        'required' => false,
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    $block_classe = $block_name . '__field field field--' . $type;
    $input_class = $block_name . '__input input input--' . $type;
    $input_class .= $required ? ' required' : '';
    $label_class = $block_name . '__label label';
    $label = $required ? $label . '*' : $label;

    // ---------------------------------------- Template
    if ( $name && $type ) {
      $html .= '<div class="' . $block_classe . '">';

        switch ( $type  ) {
          case 'checkbox':
            $html .= $label ? '<label class="' . $label_class . '">' : '';
              $html .= '<input
                class="' . $input_class . '"
                type="' . $type . '"
                name="' . $name . '"
                ' . ( $value ? ' value="' . $value . '"' : '' ) . '
                ' . ( 'rude' === $name ? ' tabindex="-1"' : '' ) .'
              >';
            $html .= $label ?  $label . '</label>' : '';
            break;
          case 'email':
          case 'file':
          case 'tel':
          case 'text':
            $html .= $label ? '<label class="' . $label_class . '">' . $label . '</label>' : '';
            $html .= '<input
              class="' . $input_class . '"
              type="' . $type . '"
              name="' . $name . '"
              ' . ( 'rude' === $name ? ' tabindex="-1"' : '' ) .'
              ' . ( $value ? ' value="' . $value . '"' : '' ) . '
              ' . ( $placeholder ? ' placehodler="' . $placeholder . '"' : '' ) . '
              ' . ( $disabled ? ' disabled' : '' ) . '
              ' . ( $multiple ? ' multiple' : '' ) . '
              ' . ( $accept ? ' accept="' . $accept . '"' : '' ) . '
            >';
            break;
        }

        $html .= '<div class="' . $block_name . '__error-message">' . ( $error_message[$type] ?? '' ) . '</div>';

      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Google Site Tag
  public function render_google_site_tag( $google_analytics_measurement_id = false ) {

    $html = '<!-- Global site tag (gtag.js) - Google Analytics -->';
    if ( $google_analytics_measurement_id ) {
      $html .= '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $google_analytics_measurement_id . '"></script>';
      $html .= '<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag("js", new Date());

        gtag("config", "' . $google_analytics_measurement_id . '" );
      </script>';
    } else {
      $html = '<!-- No Google Analytics Measurement ID Provided -->';
    }

    return $html;

  }

  // ---------------------------------------- Intro
  public function render_intro( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'intro',
        'classes' => '',
        'html' => '',
        'id' => false,
      ],
      $params
    ));

    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $title = get_field( 'title', $id ) ?: get_the_title( $id );
    $hide_title = get_field( 'hide_title', $id ) ?: false;

    if ( !$hide_title && $title ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          $html .= '<h1 class="' . $block_name . '__title title title--page-title">' . $title . '</h1>';
        $html .= $this->render_bs_container( 'closed' );
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Lazyload iFrame
  public function render_lazyload_iframe( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'aspect_ratio' => '16-9',
        'background' => false,
        'classes' => '',
        'delay' => 0,
        'duration' => 750,
        'html' => '',
        'preload' => false,
        'video_id' => '', // 160730254, 163590531, 221632885
        'video_source' => 'vimeo',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $iframe_classes = 'lazyload lazyload-item lazyload-item--iframe';
    $iframe_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $iframe_classes .= $preload ? ' lazypreload' : '';
    $iframe_classes = $classes ? $classes . ' ' . $iframe_classes : $iframe_classes;

    $video_source_url = ( 'vimeo' == $video_source ) ? 'https://player.vimeo.com/video/' : 'https://www.youtube.com/embed/';
    $video_source_url .= $video_id;
    $video_source_url .= ( $background ) ? '?autoplay=1&loop=1&autopause=0&muted=1&background=1' : '?autoplay=0&loop=1&autopause=0&muted=0&background=0';

    // ---------------------------------------- Template
    if ( $video_source_url && $video_id ) {

      $html = '
        <iframe
          class="' . trim($iframe_classes) . '"
          data-aspect-ratio="' . $aspect_ratio . '"
          data-src="' . $video_source_url . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          src="' . $video_source_url . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_lazyload_image( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'alt_text' => '',
        'background' => false,
        'classes' => '',
        'custom_sizes_title' => $this->custom_image_title,
        'custom_sizes' => $this->custom_image_sizes,
        'delay' => 0,
        'duration' => 450,
        'html' => '',
        'image' => [],
        'image_srcset' => '',
        'is_svg' => false,
        'preload' => false,
      ],
      $params
    ));

    // ---------------------------------------- Data
    $image_classes = 'lazyload lazyload-item lazyload-item--image';
    $image_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $image_classes .= $preload ? ' lazypreload' : '';
    $image_classes = $classes ? $classes . ' ' . $image_classes : $image_classes;

    $image_full_source = isset($image['url']) ? $image['url'] : false;
    $image_placeholder_source = isset($image['sizes'][$custom_sizes_title . '-10']) ? $image['sizes'][$custom_sizes_title . '-10'] : false;
    $image_width = isset($image['width']) ? $image['width'] : '';
    $image_height = isset($image['height']) ? $image['height'] : '';

    $is_svg = ( isset($image['subtype']) && 'svg+xml' == $image['subtype'] ) ? true : false;

    foreach ( $custom_sizes as $i => $size ) {
      $image_srcset .= ( $i > 0 ) ? ', ' : '';
      $image_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
    }

    // ---------------------------------------- Template
    if ( $image_full_source ) {
      if ( $background ) {

        $html = '<div
          class="' . $image_classes . '"
          data-bg="' . $image_full_source . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"';
        $html .= $image_placeholder_source ? ' style="background-url(' . $image_placeholder_source . ')"' : '';
        $html .= '></div>';

       } else {

        if ( $is_svg ) {
          $html = '<img src="' . $image_full_source . '"';
            $html .= $image_width ? ' width="' . $image_width . '"' : '';
            $html .= $image_height ? ' height="' . $image_height . '"' : '';
            $html .= $alt_text ? ' alt="' . $alt_text . '"' : '';
            $html .= ' loading="lazy"';
          $html .= ' />';
        } else {
          $html = '<img
            class="' . $image_classes . '"
            data-src="' . $image_full_source . '"
            data-transition-delay="' . $delay . '"
            data-transition-duration="' . $duration . '"';
          $html .= $image_width ? ' width="' . $image_width . '"' : '';
          $html .= $image_height ? ' height="' . $image_height . '"' : '';
          $html .= $image_srcset ? ' data-sizes="auto" data-srcset="' . $image_srcset . '"' : '';
          $html .= $image_placeholder_source ? ' src="' . $image_placeholder_source . '"' : '';
          $html .= $alt_text ? ' alt="' . $alt_text . '"' : '';
          $html .= ' />';
        }

      }
    }

    return $html;

  }

  // ---------------------------------------- Lazyload Video
  public function render_lazyload_video( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'autoplay' => false,
        'background' => false,
        'classes' => '',
        'controls' => false,
        'delay' => 0,
        'duration' => 750,
        'html' => '',
        'loop' => false,
        'mime_type' => '',
        'muted' => false,
        'preload' => false,
        'video' => [],
        'video_url' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $video_classes = 'lazyload lazyload-item lazyload-item--video';
    $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $video_classes .= $preload ? ' lazypreload' : '';
    $video_classes = $classes ? $classes . ' ' . $video_classes : $video_classes;

    $video_url = isset($video['url']) ? $video['url'] : false;
    $mime_type = isset($video['mime_type']) ? $video['mime_type'] : false;

    // ---------------------------------------- Template
    if ( $video_url && $mime_type ) {
      $html = '<video
        class="' . $video_classes . '"
        src="' . $video_url . '"
        data-transition-delay="' . $delay . '"
        data-transition-duration="' . $duration . '"
        ' . ( $autoplay ? ' autoplay' : '' ) . '
        ' . ( $controls ? ' controls' : '' ) . '
        ' . ( $loop ? ' loop' : '' ) . '
        ' . ( $muted ? ' muted' : '' ) . '
      >';
        $html .= '<source src="' . $video_url . '" type="' . $mime_type . '">';
      $html .= '</video>';
    }

    return $html;

  }

  // --------------------------- Link
  public function render_link( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'active' => false,
        'block_name' => 'link',
        'classes' => '',
        'html' => '',
        'link' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ?: $block_name;
    $block_classes .= $style ? " {$style}" : "";
    $block_classes .= $active ? ' active' : '';
    $icon = "";

    // ---------------------------------------- Template
    if ( $title && $link ) {

      if ( "production-media-kit" == $style ) {
        $link = "mailto:{$link}";
      }

      switch ( $style ) {
        case 'mobile-menu-child':
          $icon = $this->render_svg_icon("chevron");
          break;
        case 'outlined-arrow':
          $icon = $this->render_svg_icon("arrow");
          break;
        case 'outlined-plus':
          $icon = $this->render_svg_icon("plus-circle");
          break;
        case 'production-buy':
          $icon = $this->render_svg_icon("add-to-cart");
          break;
        case 'production-media-kit':
          $icon = $this->render_svg_icon("open-email");
          break;
        case 'production-website':
          $icon = $this->render_svg_icon("www");
          break;
      }

      return "
        <a class='{$block_classes}' href='{$link}' target='{$target}' title='{$title}'>
          <span class='{$block_name}__title'>{$title}</span>
          " . ( $icon ? "<span class='{$block_name}__icon'>{$icon}</span>" : "" ) . "
        </a>
      ";

    }

    return;

  }

  // --------------------------- Mobile Menu
  public function render_mobile_menu( $params = [] ) {

    // ---------------------------------------- Vars
    $defaults = [
      'block_name' => 'mobile-menu',
      'current_id' => false,
      'html' => '',
      'site_name' => get_bloginfo('name'),
    ];

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Templates
    $html .= '<div class="' . $block_name . '">';
      $html .= '<div class="' . $block_name . '__main">';

        $html .= '<nav class="' . $block_name . '__navigation navigation" id="' . $block_name . '__navigation">';
          $html .= $this->render_wp_navigation( [ 'current_id' => $current_id, 'menu_name' => 'Mobile Menu', 'wrapper' => false ] );
        $html .= '</nav>';

      $html .= '</div>';
    $html .= '</div>';

    // ---------------------------------------- Returned
    return $html;

  }

  // --------------------------- Navigation
  public function render_navigation( $links = [], $current_id = false, $class_modifier = '' ) {

    $html = '';
    $block_name = 'navigation';
    $block_classes = ( $class_modifier ) ? $block_name . ' ' . $block_name . '--' . $class_modifier : $block_name;

    if ( $links ) {
      $html .= '<nav class="' . $block_classes . '">';
        foreach( $links as $i => $item ) {

          // set defaults
          $type = $title = $link = $link_anchor = $link_category = $link_external = $link_page = false;
          $is_internal = true;
          $is_active = false;

          // extract link data
          extract( $item );

          switch ( $type ) {
            case 'anchor':
              $link = ( $link_anchor ) ? '#' . $link_anchor : false;
              break;
            case 'external':
              $link = ( $link_external ) ? $link_external : false;
              $is_internal = false;
              break;
            case 'category':
              break;
            case 'page-post':
              break;
          }

          $html .= $this->render_navigation_item( $title, $link, $is_active, $is_internal );

        }
      $html .= '</nav>';
    }

    return $html;

  }

  // --------------------------- Navigation for Pages with Children Links
  public function render_navigation_page_children( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'sub-navigation',
        'classes' => '',
        'current_id' => '',
        'custom_link' => [],
        'html' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $parent_id = wp_get_post_parent_id( $current_id ) ? wp_get_post_parent_id( $current_id ) : $current_id;
    $child_pages = get_pages([ 'child_of' => $parent_id, 'sort_column' => 'menu_order' ]);

    // ---------------------------------------- Template
    if ( $child_pages ) {
      $html .= '<nav class="' . $block_classes . '">';

        if ( $custom_link ) {

          $link = isset($custom_link['link']) && !empty($custom_link['link']) ? $custom_link['link'] : '';
          $title = isset($custom_link['title']) && !empty($custom_link['title']) ? $custom_link['title'] : '';
          $active = isset($custom_link['active']) && !empty($custom_link['active']) ? $custom_link['active'] : '';
          $target = isset($custom_link['target']) && !empty($custom_link['target']) ? $custom_link['target'] : '';
          $rel = isset($custom_link['rel']) && !empty($custom_link['rel']) ? $custom_link['rel'] : '';

          if ( $link && $title ) {
            $html .= '<div class="' . $block_name . '__item">';
              $html .= $this->render_link([
                'active' => $active,
                'classes' => $block_name . '__link',
                'target' => $target,
                'rel' => $rel,
                'title' => $title,
                'link' => $link,
              ]);
            $html .= '</div>';
          }

        }

        foreach( $child_pages as $i => $item ) {

          $id = $item->ID;
          $link = get_permalink($id);
          $title = $item->post_title;
          $post_status = $item->post_status;
          $active = ( $id == $current_id ) ? true : false;
          $target = '_self';
          $rel = '';

          if ( 'publish' === $post_status ) {
            $html .= '<div class="' . $block_name . '__item">';
              $html .= $this->render_link([
                'active' => $active,
                'classes' => $block_name . '__link',
                'target' => $target,
                'rel' => $rel,
                'title' => $title,
                'link' => $link,
              ]);
            $html .= '</div>';
          }

        }
      $html .= '</nav>';
    }

    return $html;

  }

  // --------------------------- Navigation Item
  public function render_navigation_item( $title = '', $url = '', $active = false, $internal = true ) {

    $html = '';
    $block_name = 'navigation';
    $target = '_self';
    $rel = false;

    if ( !$internal ) {
      $target = '_blank';
      $rel = 'noopener';
    }

    if ( $title && $url ) {
      $html .= '<div class="' . $block_name . '__item' . ( $active ? ' active' : '' ) . '">';
        $html .= '<a
          class="' . $block_name . '__link' . ( $active ? ' active' : '' ) . '"
          href="' . $url . '"
          target="' . $target . '"
          title="' . $title . '"
          ' . ( $rel ? 'rel="' . $rel . '"' : '' ) . '
        >' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Pagination
  public function render_pagination( $params = [] ) {

    $block_name = 'pagination';
    $defaults = [
      'next' => false,
      'prev' => false,
      'pages' => false,
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Template
    $html .= '<section class="' . $block_name . '__pagination pagination">';
      $html .= $this->render_bs_container( 'open', 'col-12', 'container-fluid' );
        $html .= '<div class="' . $block_name . '__main">';

          $html .= '<div class="' . $block_name . '__item prev ' . ( $prev ? 'active' : 'not-active' ) . '">';
            $html .= $prev ? $prev : 'Prev';
          $html .= '</div>';

          foreach( $pages as $i => $item ) {
            $html .= '<div class="' . $block_name . '__item page">' . $item . '</div>';
          }

          $html .= '<div class="' . $block_name . '__item next ' . ( $next ? 'active' : 'not-active' ) . '">';
            $html .= $next ? $next : 'Next';
          $html .= '</div>';

        $html .= '</div>';
      $html .= $this->render_bs_container( 'closed' );
    $html .= '</section>';

    return $html;

  }

  // ---------------------------------------- Partner
  public function render_partner_by_id( $id = 0 ) {

    $block_name = "partner";
    $image = get_field("image", $id) ?: "";
    $title = get_the_title( $id ) ?: "";
    $permalink = get_the_permalink( $id ) ?: "";
    $website = get_field("website", $id) ?: "";

    return $image && $website ? "
      <div class='{$block_name}'>
        <a class='{$block_name}__link link' href='{$website}' target='_blank' title='{$title}'>
          <div class='{$block_name}__image'>{$image}</div>
        </a>
      </div>
    " : "";

  }

  // --------------------------- Partners
  public function render_partners( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'partners',
        'classes' => '',
        'current_id' => '',
        'html' => '',
        'partners' => [],
        'title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;

    // ---------------------------------------- Template
    if ( $partners ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $title ? '<strong class="' . $block_name . '__title">' . $title . '</strong>' : '';
        $html .= '<ul class="' . $block_name . '__list">';
          foreach ( $partners as $i => $item ) {

            $partner = isset($item['partner']) && !empty($item['partner']) ? $item['partner'] : [];
            $id = isset($partner->ID) && !empty($partner->ID) ? $partner->ID : false;
            $image = get_field( 'image', $id ) ? get_field( 'image', $id ) : '';
            $title = get_the_title( $id ) ? get_the_title( $id ) : '';
            $website = get_field( 'website', $id ) ? get_field( 'website', $id ) : '';

            if ( $image ) {
              $html .= '<li class="' . $block_name . '__item">';
                $html .= $website ? '<a class="' . $block_name . '__link link" href="' . $website . '" target="_blank" title="' . $title . '">' : '';
                  $html .= $image;
                $html .= $website ? '</a>' : '';
              $html .= '</li>';
            }

          }
        $html .= '</ul>';
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Production About
  public function render_production_about_by_id( $id = 0 ) {

    $block_name = "about";
    $block_html = "";
    $about = get_field( "about", $id ) ?: "";
    $available_on = get_field( "available_on", $id ) ?: [];
    $broadcasters = get_field( "broadcasters", $id ) ?: [];
    $buy_link = get_field( "buy_link", $id ) ?: "";
    $length = get_field( "length", $id ) ?: "";
    $media_kit = get_field( "media_kit", $id ) ?: [];
    $media_kit_email = $media_kit["email"] ?? "";
    $social_accounts = get_field( "social", $id ) ?: [];
    $type = get_field( "type", $id ) ?: "";
    $website = get_field( "website", $id ) ?: "";

    if ( true ) {
      $block_html .= "<div class='{$block_name}'>";
        $block_html .= "<div class='{$block_name}__layout'>";

          $block_html .= "<div class='{$block_name}__main'>";
            $block_html .= "<div class='{$block_name}__content body-copy--primary body-copy--md'>";
              if ( $type && $length ) {
                $block_html .= "<p class='{$block_name}__type'>{$type}, {$length}</p>";
              }
              $block_html .= $about ?: "";
            $block_html .= "</div>";
          $block_html .= "</div>";

          $block_html .= "<div class='{$block_name}__extras body-copy--primary body-copy--md'>";
            if ( !empty($social_accounts) ) {
              $block_html .= $this->render_social_accounts([ "accounts" => $social_accounts ]);
            }
            if ( !empty($broadcasters) ) {
              $block_html .= $this->render_production_partners([
                "partners" => $broadcasters,
                "title" => "Broadcasters"
              ]);
            }
            if ( !empty($available_on) ) {
              $block_html .= $this->render_production_partners([
                "partners" => $available_on,
                "title" => "Available On"
              ]);
            }
            if ( $buy_link ) {
              $link = $this->render_link([
                "link" => $buy_link,
                "style" => "production-buy",
                "target" => "_blank",
                "title" => "Buy"
              ]);
              $block_html .= "<div class='about__cta'>{$link}</div>";
            }
            if ( $website ) {
              $link = $this->render_link([
                "link" => $website,
                "style" => "production-website",
                "target" => "_blank",
                "title" => "Visit the Website"
              ]);
              $block_html .= "<div class='about__cta'>{$link}</div>";
            }
            if ( $media_kit_email ) {
              $link = $this->render_link([
                "link" => $media_kit_email,
                "style" => "production-media-kit",
                "target" => "_blank",
                "title" => "Request a Media Kit"
              ]);
              $block_html .= "<div class='about__cta'>{$link}</div>";
            }
          $block_html .= "</div>";

        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // ---------------------------------------- Production Awards
  public function render_production_awards_by_id( $id = 0 ) {

    $block_name = "awards";
    $block_html = "";
    $awards = get_field( "awards", $id ) ?: [];

    if ( !empty($awards) ) {
      $block_html .= "<div class='{$block_name}'>";
        $block_html .= "<div class='{$block_name}__main'>";
          foreach ( $awards as $i => $item ) {
            $award = $item["award"] ?? "";
            if ( $award ) {
              $block_html .= "<div class='{$block_name}__item rte'><p>" . $award . "</p></div>";
            }
          }
        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // ---------------------------------------- Production Cast
  public function render_production_cast_by_id( $id = 0 ) {

    $block_name = "cast";
    $block_html = "";
    $cast = get_field( "cast", $id ) ?: [];
    $svg_icon_plus_circle = $this->render_svg_icon("plus-circle");

    // ---------------------------------------- Template
    if ( !empty($cast) ) {
      $block_html .= "<div class='{$block_name}'>";
        $block_html .= "<div class='{$block_name}__main'>";

          foreach ( $cast as $index => $item ) {

            $link = $item["link"] ?: "";
            $link_title = str_contains( $link, "imdb" ) ? "Imdb" : "Read More";
            $name = $item["name"] ?: "";
            $photo = $item["photo"] ?: [];
            $photo_lazy = $this->render_nu_lazyload_image([ "image" => $photo ]);
            $role = $item["role"] ?: "";

            $block_html .= "<div class='{$block_name}__item' data-index='{$index}'>";
              $block_html .= $link ? "<a class='{$block_name}__link link' href='{$link}' target='_blank' title='{$name}'>" : "";

                $block_html .= $photo_lazy ? "<div class='{$block_name}__photo'>{$photo_lazy}</div>" : "";

                if ( $name || $role ) {
                  $block_html .= "<div class='{$block_name}__details'>";
                    $block_html .= $name ? "<strong class='{$block_name}__name'>{$name}</strong>" : "";
                    $block_html .= $role ? "<span class='{$block_name}__role'>{$role}</span>" : "";
                  $block_html .= "</div>";
                }

                if ( $link ) {
                  $block_html .= "<div class='{$block_name}__cta'>";
                    $block_html .= "<div class='button button--card'>";
                      $block_html .= "<span class='button__title'>{$link_title}</span>";
                      $block_html .= "<span class='button__icon'>{$svg_icon_plus_circle}</span>";
                    $block_html .= "</div>";
                  $block_html .= "</div>";
                }

              $block_html .= $link ? "</a>" : "";
            $block_html .= "</div>";

          }

        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // ---------------------------------------- Production Partners
  public function render_production_partners( $params = [] ) {

    extract(array_merge(
      [
        'block_name' => 'partners',
        'block_html' => '',
        'partners' => [],
        'title' => '',
      ],
      $params
    ));

    if ( !empty($partners) ) {
      $block_html .= "<div class='{$block_name}'>";
        if ( $title ) {
          $block_html .= "<strong class='{$block_name}__title'>{$title}</strong>";
        }
        $block_html .= "<div class='{$block_name}__listing'>";
          foreach ( $partners as $i => $item ) {
            $partner_id = $item['partner']->ID ?? 0;
            $partner_image = get_field( 'image', $partner_id ) ?: "";
            $partner_title = get_the_title( $partner_id ) ?: "";
            $partner_website = get_field( 'website', $partner_id ) ?: "";
            if ( $partner_image ) {
              $block_html .= "<div class='{$block_name}__item'>";
                $block_html .= $partner_website ? "<a class='{$block_name}__link link' href='{$partner_website}' target='_blank' title='{$partner_title}'>" : "";
                  $block_html .= $partner_image;
                $block_html .= $partner_website ? "</a>" : "";
              $block_html .= "</div>";
            }
          }
        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // ---------------------------------------- Production News
  public function render_production_news_by_id( $id = 0 ) {

    $block_name = "news";
    $block_html = "";
    $news_articles = get_field("news", $id) ?: [];

    if ( $news_articles ) {
      $block_html .= "<div class='{$block_name}'>";
        $block_html .= "<div class='{$block_name}__listing grid grid--1 grid--md-3'>";
          foreach ( $news_articles as $item ) {
            $block_html .= $this->render_card_news_by_id($item['article']->ID ?? 0);
          }
        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // ---------------------------------------- Production Photos
  public function render_production_photos_by_id( $id = 0 ) {

    $block_name = "photos";
    $block_html = "";
    $block_id = $this->get_unique_id("{$block_name}--");
    $photos = get_field( "photos", $id ) ?: [];

    // ---------------------------------------- Template
    if ( !empty($photos) ) {
      $block_html .= "<div class='{$block_name}' id='{$block_id}'>";
        $block_html .= "<div class='{$block_name}__main'>";
          foreach( $photos as $i => $item ) {
            $image = $item["photo"] ?: [];
            $image_lazy = $this->render_nu_lazyload_image([ "image" => $image ]);
            if ( $image_lazy ) {
              $block_html .= "<div class='{$block_name}__item'>{$image_lazy}</div>";
            }
          }
        $block_html .= "</div>";
      $block_html .= "</div>";
    }

    return $block_html;

  }

  // --------------------------- Preview
  public function render_production_preview( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'id' => false,
        'index' => '',
        'html' => '',
        'layout' => 'grid',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $about = get_field( 'about', $id ) ?: '';
    $type = get_field( 'type', $id ) ?: '';
    $length = get_field( 'length', $id ) ?: '';
    $alternate_featured = get_field( 'alternate_featured', $id ) ?: [];
    $alternate_featured_src = $alternate_featured['url'] ?? '';
    $broadcasters = get_field( 'broadcasters', $id ) ?: [];
    $collapse_id = $this->get_unique_id("production__");
    $excerpt = get_the_excerpt( $id );
    $feature_image = $this->get_featured_image_by_post_id( $id );
    $feature_image_lazy = $this->render_nu_lazyload_image([ "image" => $feature_image ]);
    $permalink = get_the_permalink( $id );
    $reel = get_field( 'reel', $id ) ?: [];
    $title = get_the_title( $id );

    // ---------------------------------------- Template
    $html .= '<div
      class="production" data-layout="' . $layout . '"
      data-alternate-feature-image="' . $alternate_featured_src . '"
      data-id="' . $id . '"
      data-index="' . $index . '"
    >';

      switch ( $layout ) {

        case 'featured-grid-large': {
          $html .= '<div class="production__cover">';
            $html .= $permalink ? '<a class="production__link link" href="' . $permalink . '" target="_self">' : '';
              $html .= '<div class="production__cover-placeholder d-block d-lg-none">' . $this->render_svg([ 'type' => 'placeholder.square' ]) . '</div>';
              $html .= '<div class="production__cover-placeholder d-none d-lg-block">' . $this->render_svg([ 'type' => 'placeholder.wide' ]) . '</div>';
              $html .= $feature_image_lazy ?: '';
              $html .= '<div class="production__overlay"></div>';
              if ( $title || $type ) {
                $html .= '<div class="production__content">';
                  $html .= '<div class="production__content-padding">';
                    $html .= $type ? '<span class="production__abc">' . $type . '</span>' : '';
                    $html .= $title ? '<h3 class="production__title">' . $title . '</h3>' : '';
                  $html .= '</div>';
                $html .= '</div>';
              }
            $html .= $permalink ? '</a>' : '';
          $html .= '</div>';
          break;
        }

        case 'featured-grid-small': {

          // ---------------------------------------- Cover
          $html .= '<div class="production__cover">';
            $html .= $feature_image ? $this->render_lazyload_image([
              'classes' => 'production__feature-image',
              'background' => true,
              'duration' => 250,
              'image' => $feature_image,
            ]) : '';
            if ( $title || $type ) {
              $html .= '<div class="production__cover-content">';
                $html .= $type ? '<span class="production__abc">' . $type . '</span>' : '';
                $html .= $title ? '<h2 class="production__title">' . $title . '</h2>' : '';
              $html .= '</div>';
            }
            if ( $reel ) {
              $html .= '<button
                class="production__button button button--watch-trailer js--toggle-trailer"
                type="button"
              >';
                $html .= '<span class="button__icon">' . $this->render_svg_icon("play") . '</span>';
                $html .= '<span class="button__title">Watch Trailer</span>';
              $html .= '</button>';
            }
            $html .= '<button
              class="production__button button button--show-more collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#' . $collapse_id .'"
              aria-expanded="false"
              aria-controls="' . $collapse_id .'"
            >' . $this->render_svg_icon("plus") . '</button>';
          $html .= '</div>';

          // ---------------------------------------- Body
          $html .= '<div class="production__body collapse" id="' . $collapse_id .'">';
            $html .= '<div class="production__body-padding">';
              $html .= $reel ? $this->render_lazyload_video([
                'classes' => 'production__reel d-none',
                'controls' => true,
                'duration' => 250,
                'preload' => true,
                'video' => $reel,
              ]) : '';
              $html .= $about ? '<div class="production__about rte">' . $about . '</div>' : '';
              $html .= $broadcasters ? $this->render_partners([
                'partners' => $broadcasters,
                'classes' => 'production__broadcasters',
                'title' => 'Airing on',
              ]) : '';
              if ( $permalink ) {
                $html .= '<div class="production__cta">';
                  $html .= '<a class="link" href="' . $permalink . '" target="_self">';
                    $html .= '<span class="link__icon">' . $this->render_svg_icon("plus-circle") . '</span>';
                    $html .= '<span class="link__title">Read More</span>';
                  $html .= '</a>';
                $html .= '</div>';
              }
            $html .= '</div>';
          $html .= '</div>';

          // ---------------------------------------- Preview
          $html .= '<div class="production__preview">';
            $html .= '<div class="production__preview-content">';
              $html .= $title ? '<h2 class="production__preview-content-title">' . $title . '</h2>' : '';
              $html .= $excerpt ? '<div class="production__preview-content-excerpt rte">' . trim_string( $excerpt, 265, '...' ) . '</div>' : '';
              if ( $permalink ) {
                $html .= '<div class="production__preview-content-cta">';
                  $html .= '<a class="link" href="' . $permalink . '" target="_self">';
                    $html .= '<span class="link__icon">' . $this->render_svg_icon("plus-circle") . '</span>';
                    $html .= '<span class="link__title">Read More</span>';
                  $html .= '</a>';
                $html .= '</div>';
              }
              $html .= $broadcasters ? $this->render_partners([
                'partners' => $broadcasters,
                'classes' => 'production__preview-content-broadcasters',
              ]) : '';
            $html .= '</div>';
          $html .= '</div>';

          break;

        }

        case 'grid': {
          $html .= '<div class="production__cover">';
            $html .= $permalink ? '<a class="production__link link" href="' . $permalink . '" target="_self">' : '';
              $html .= '<div class="production__cover-placeholder d-block d-lg-none">' . $this->render_svg([ 'type' => 'placeholder.square' ]) . '</div>';
              $html .= '<div class="production__cover-placeholder d-none d-lg-block">' . $this->render_svg([ 'type' => 'placeholder.wide' ]) . '</div>';
              $html .= $feature_image ? $this->render_lazyload_image([
                'classes' => 'production__feature-image feature-image',
                'background' => true,
                'duration' => 250,
                'image' => $feature_image,
              ]) : '';
              $html .= '<div class="production__gradient"></div>';
              $html .= $title ? '<h3 class="production__title">' . $title . '</h3>' : '';
              $html .= '<div class="production__nat-geo nat-geo"></div>';
            $html .= $permalink ? '</a>' : '';
          $html .= '</div>';
          break;
        }

      }

    $html .= '</div>';

    return $html;

  }

  // ---------------------------------------- Productions Listing
  public function render_productions_listing( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'productions-listing',
        'categories' => [],
        'cta' => [],
        'current_id' => '',
        'exclude' => [],
        'html' => '',
        'layout' => 'grid',
        'limit' => 48,
        'order' => 'ASC',
        'order_by' => 'title',
        'section' => [],
      ],
      $params
    ));

    // ---------------------------------------- Data
    $categories = $section['categories'] ?? $categories;
    $cta = $section['cta'] ?? $cta;
    $exclude = $section['exclude'] ?? [];
    $layout = $section['layout'] ?? $layout;
    $limit = $section['limit'] ?? $limit;
    $order = $section['order'] ?? $order;
    $order_by = $section['order_by'] ?? $order_by;

    $query_args = [
      'category__in' => $categories,
      'posts_per_page' => $limit,
      'post_type' => 'production',
      'post__not_in' => $exclude,
      'order' => $order,
      'orderby' => $order_by,
    ];

    $wp_query = new WP_Query( $query_args );
    $wp_query_index = 0;

    // ---------------------------------------- Custom WP Query
    $html .= '<div class="' . $block_name . '">';

      if ( $wp_query->have_posts() ){

        $html .= '<div class="' . $block_name . '__layout" data-layout="' . $layout . '">';
          while( $wp_query->have_posts() ) {

            // ---------------------------------------- Data
            $wp_query->the_post();
            $id = get_the_ID();

            // ---------------------------------------- Template
            $html .= '<div
              class="' . $block_name . '__item" data-index="' . $wp_query_index . '"
              data-layout="' . $layout . '"
              data-post-id="' . $id . '"
            >';
              $html .= $this->render_production_preview([ 'id' => $id, 'index' => $wp_query_index, 'layout' => $layout ]);
            $html .= '</div>';

            $wp_query_index++;

          }
        $html .= '</div>';

        wp_reset_postdata();

      } else {
        $html .= '<!-- No posts! Check the post query args or the database.  -->';
      }

      $html .= $cta ? $this->render_cta([ 'classes' => $block_name .'__cta', 'cta' => $cta, 'justification' => 'center' ]) : '';

    $html .= '</div>';

    return $html;

  }

  // --------------------------- Post Categories
  public function render_post_categories( $params = [] ) {

    $block_name = 'categories';
    $defaults = [ 'show_cat_icon' => false, 'post_id' => false, 'limit' => 1 ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $categories = get_the_category( $post_id ) ? get_the_category( $post_id ) : [];

      foreach( $categories as $i => $cat ) {



        if ( $i < $limit ) {

          // ---------------------------------------- WP Data
          $cat_name = $cat->name;
          $cat_id = $cat->term_id;
          $cat_slug = $cat->slug;
          $cat_url = get_category_link( $cat_id );
          $cat_term = get_term( $cat_id ) ? get_term( $cat_id ) : false;

          // ---------------------------------------- ACF Data
          $cat_icon = get_field( 'icon_svg', $cat_term );

          if ( $i > 0 ) {
            $html .= '<div class="' . $block_name . '__item delimiter">|</div>';
          }

          $html .= '<div class="' . $block_name . '__item">';
            $html .= ( $show_cat_icon && $cat_icon ) ? '<div class="' . $block_name . '__icon">' . $cat_icon . '</div>' : '';
            $html .= '<div class="' . $block_name . '__name">' . $cat_name . '</div>';
          $html .= '</div>';

        }
      }

    }

    return $html;

  }

  // --------------------------- Post Meta
  public function render_post_meta( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'show_cat_icon' => false, 'cat_limit' => 1, 'date_format' => 'm.d.y', 'meta_types' => [], 'post_id' => false ];
    $html = $meta_html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id && $meta_types ) {

      if ( 'post' !== get_post_type( $post_id ) ) {
        $block_name = get_post_type( $post_id );
      }

      foreach ( $meta_types as $i => $meta ) {

        switch( $meta ) {
          case 'author':

            $author_id = get_post_field( 'post_author', $post_id );
            $author = get_the_author_meta( 'display_name', $author_id );
            $meta_html = ( $author ) ? '<div class="' . $block_name . '__author author">' . $author . '</div>' : '';
            break;

          case 'categories':

            $categories = $this->render_post_categories([ 'show_cat_icon' => $show_cat_icon, 'post_id' => $post_id, 'limit' => $cat_limit ]);
            $meta_html = ( $categories ) ? '<div class="' . $block_name . '__categories categories" data-show-cat-icont="' . $show_cat_icon . '">' . $categories . '</div>' : '';
            break;

          case 'date':

            $date = get_the_date( $date_format, $post_id );
            $meta_html = ( $date ) ? '<time class="' . $block_name . '__date date" datetime="' . $date . '">' . $date . '</time>' : '';
            break;

          case 'issue':

            $issue = get_field( 'issue', $post_id );
            $meta_html = ( $issue ) ? '<div class="' . $block_name . '__issue issue">Issue ' . ( $issue > 10 ? $issue : '0' . $issue ) . '</div>' : '';
            break;

        }

        if ( $meta_html ) {
          $html .= ( $i > 0 ) ? '<div class="' . $block_name . '__delimiter delimiter">|</div>' . $meta_html : $meta_html;
        }

      }
    }

    return $html;

  }

  // --------------------------- Post Preview
  public function render_post_preview( $post_id = false, $params = [] ) {

    $html = '';
    $block_name = 'article';

    if ( $post_id ) {

      // default $params
      $appearance = '';
      $date_format = 'F j, Y';

      if ( $params ) {
        extract( $params );
      }

      // get data
      $date = ( get_the_date( $date_format, $post_id ) ) ?: false;
      $excerpt = ( get_the_excerpt( $post_id ) ) ?: false;
      $featured_image = $this->get_featured_image_by_post_id( $post_id ) ?: false;
      $permalink = get_permalink( $post_id ) ?: '';
      $title = get_the_title( $post_id ) ?: '';

      // build template
      $html .= '<article class="' . $block_name . ( $appearance ? ' ' . $block_name . '--' . $appearance : '' ) . '" data-post-id="' . $post_id . '">';
        $html .= '<a href="' . $permalink . '" target="_self">';

          $html .= '<div class="' . $block_name . '__featured-image">';
            if ( $featured_image ) {
              $html .= $this->render_lazyload_image( $featured_image );
            }
          $html .= '</div>';

          $html .= '<div class="' . $block_name . '__content">';
           if ( $date ) {
              $html .= '<div class="' . $block_name . '__date">' . $date . '</div>';
           }
            if ( $title ) {
              $html .= '<h2 class="' . $block_name . '__title">' . $title . '</h2>';
            }
          $html .= '</div>';

        $html .= '</a>';
      $html .= '</article>';

    }

    return $html;

  }

  // --------------------------- Screener Request
  public function render_screener_request( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'screener-request',
        'classes' => '',
        'current_id' => '',
        'html' => '',
        'screener_title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $screener_request = get_field( 'screener_request', 'options' ) ? get_field( 'screener_request', 'options' ) : [];
    $heading = isset($screener_request['heading']) && !empty($screener_request['heading']) ? $screener_request['heading'] : '';
    $banner = isset($screener_request['banner']) && !empty($screener_request['banner']) ? $screener_request['banner'] : [];
    $contact_name = isset($screener_request['contact_name']) && !empty($screener_request['contact_name']) ? $screener_request['contact_name'] : '';
    $contact_role = isset($screener_request['contact_role']) && !empty($screener_request['contact_role']) ? $screener_request['contact_role'] : '';
    $contact_phone = isset($screener_request['contact_phone']) && !empty($screener_request['contact_phone']) ? $screener_request['contact_phone'] : '';
    $contact_email = isset($screener_request['contact_email']) && !empty($screener_request['contact_email']) ? $screener_request['contact_email'] : '';
    $contact_subject = $screener_title ? 'Screener Request for – ' . $screener_title : 'General Screener Request';

    // ---------------------------------------- Template
    $html .= '<div class="' . $block_classes . '">';
      $html .= $banner ? $this->render_lazyload_image([
        'classes' => $block_name . '__banner',
        'background' => true,
        'duration' => 250,
        'image' => $banner,
      ]) : '';
      $html .= '<div class="' . $block_name . '__main">';
        $html .= $this->render_bs_container( 'open', 'col-10 offset-1 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1', 'container-fluid' );
          $html .= '<div class="' . $block_name . '__content">';
            $html .= $heading ? '<h3 class="' . $block_name . '__heading">' . $heading . '</h3>' : '';
            if ( $contact_name || $contact_role || $contact_phone || $contact_email ) {
              $html .= '<div class="' . $block_name . '__contact-info">';
                $html .= $contact_name ? '<span class="' . $block_name . '__contact-info-item name">' . $contact_name . '</span>' : '';
                $html .= $contact_role ? '<span class="' . $block_name . '__contact-info-item role">' . $contact_role . '</span>' : '';
                if ( $contact_phone ) {
                  $contact_phone_link = 'tel:' . $contact_phone;
                  $html .= '<span class="' . $block_name . '__contact-info-item phone">';
                    $html .= '<a class="link" href="' . $contact_phone_link . '">' . $contact_phone . '</a>';
                  $html .= '</span>';
                }
                if ( $contact_email ) {
                  $contact_email_link = $contact_subject ? 'mailto:' . $contact_email . '?subject=' . rawurlencode($contact_subject) : 'mailto:' . $contact_email;
                  $html .= '<span class="' . $block_name . '__contact-info-item email">';
                    $html .= '<a class="link" href="' . $contact_email_link . '">' . $contact_email . '</a>';
                  $html .= '</span>';
                }
              $html .= '</div>';
            }
          $html .= '</div>';
        $html .= $this->render_bs_container( 'closed' );
      $html .= '</div>';
    $html .= '</div>';

    return $html;

  }


  // --------------------------- Site Background
  public function render_site_background_by_page_id( $id = 0 ) {

    $post_type = get_post_type( $id ) ?: 'not-set';
    $image_lazy = "";

    switch ( $post_type ) {
      case 'page': {
        $image = get_field( 'background', $id ) ?: [];
        $image_lazy = $this->render_nu_lazyload_image([ "image" => $image ]);
        break;
      }
      case 'production': {
        $image = get_field( 'alternate_featured', $id ) ?: $this->get_featured_image_by_post_id( $id );
        $image_lazy = $this->render_nu_lazyload_image([ "image" => $image ]);
        break;
      }
    }

    return $image_lazy ? "
      <div class='site-background'>{$image_lazy}</div>
    " : "";

  }

  // --------------------------- Render Social Accounts
  public function render_social_accounts( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accounts' => [],
        'block_name' => 'social',
        'classes' => '',
        'style' => 'light',
        'html' => '',
      ],
      $params
    ));


    // ---------------------------------------- Data
    // $social = get_field( 'social', 'options' ) ? get_field( 'social', 'options' ) : [];

    // ---------------------------------------- Template
    if ( $accounts ) {
      $html .= '<div class="' . $block_name . '">';
        foreach( $accounts as $i => $item ) {

          $account = isset($item['account']) && !empty($item['account']) ? $item['account'] : '';
          $link = isset($item['url']) && !empty($item['url']) ? $item['url'] : '';

          if ( $account && $link ) {
            $html .= '<div class="' . $block_name . '__item">';
              $html .= '<a class="' . $block_name . '__link link" href="' . $link . '" target="_blank" rel="noopener">' . $this->render_svg([ 'type' => $account ]) . '</a>';
            $html .= '</div>';
          }

        }
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Subnavigation Item
  public function render_subnavigation_item( $params = [], $current_id = 0 ) {

    // default data
    $html = '';
    $block_name = 'subnavigation';
    $link = $link_attachment = $link_category = $link_external = $link_id = $link_page = $link_type = $title = false;
    $target = '_self';

    extract( $params );

    switch( $link_type ) {
      case 'attachment':
        $link = ( isset($link_attachment['url']) ) ? $link_attachment['url'] : false;
        $target = '_blank';
        break;
      case 'category':
        $link_id = $link_category;
        $link = get_category_link( $link_id );
        break;
      case 'external':
        $link = $link_external;
        $target = '_blank';
        break;
      case 'page':
        $link_id = $link_page;
        $link = get_permalink( $link_id );
        break;
    }

    $is_active = ( $current_id === $link_id ) ? true : false;

    if ( $title && $link ) {
      $html .= '<div class="' . $block_name . '__item" data-is-active="' . ( $is_active ? 'true' : 'false' ) . '">';
        $html .= '<a class="' . $block_name . '__link' . ( $is_active ? ' active' : '' ) . '" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- SVG Icon
  public function render_svg( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => 'not-set',
      ],
      $params
    ));

    switch( $type ) {

      case 'placeholder.square': {
        $html = '<svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
          <g opacity="0.075">
            <path d="M292.465 224.94H315.398C317.364 224.94 318.675 226.253 318.675 228.223V240.044C318.675 242.014 317.364 243.328 315.398 243.328H292.465C290.499 243.328 289.188 242.014 289.188 240.044V228.223C289.188 226.253 290.499 224.94 292.465 224.94ZM292.465 257.775H315.398C317.364 257.775 318.675 259.088 318.675 261.059V272.879C318.675 274.849 317.364 276.163 315.398 276.163H292.465C290.499 276.163 289.188 274.849 289.188 272.879V261.059C289.188 259.088 290.499 257.775 292.465 257.775ZM292.465 293.237H315.398C317.364 293.237 318.675 294.551 318.675 296.521V308.342C318.675 310.312 317.364 311.625 315.398 311.625H292.465C290.499 311.625 289.188 310.312 289.188 308.342V296.521C289.188 294.551 290.499 293.237 292.465 293.237ZM292.465 326.073H315.398C317.364 326.073 318.675 327.386 318.675 329.356V341.177C318.675 343.147 317.364 344.461 315.398 344.461H292.465C290.499 344.461 289.188 343.147 289.188 341.177V329.356C289.188 327.386 290.499 326.073 292.465 326.073ZM292.465 358.908H315.398C317.364 358.908 318.675 360.221 318.675 362.192V374.012C318.675 375.326 317.364 377.296 315.398 377.296H292.465C290.499 377.296 289.188 375.982 289.188 374.012V362.192C289.188 360.221 290.499 358.908 292.465 358.908ZM280.015 385.833C285.257 387.147 290.499 387.147 296.396 387.147C344.229 387.147 383.544 347.744 383.544 299.804C383.544 251.865 344.229 212.462 296.396 212.462C290.499 212.462 285.257 213.119 280.015 213.776V385.833V385.833Z" fill="white"/>
            <path d="M298.362 205.238C350.782 205.238 393.373 247.924 393.373 300.461C393.373 352.998 350.782 395.684 298.362 395.684C245.942 395.684 203.351 352.998 203.351 300.461C203.351 247.924 245.942 205.238 298.362 205.238ZM298.362 177C230.871 177 175.831 232.163 175.831 299.804C175.831 367.445 230.871 422.609 298.362 422.609C365.852 422.609 420.893 367.445 420.893 299.804C420.893 232.163 365.852 177 298.362 177Z" fill="white"/>
          </g>
        </svg>';
        break;
      }

      case 'placeholder.wide': {
        $html = '<svg width="800" height="600" viewBox="0 0 800 600" xmlns="http://www.w3.org/2000/svg">
          <g opacity="0.15">
            <path d="M392.465 224.94H415.398C417.364 224.94 418.675 226.253 418.675 228.223V240.044C418.675 242.014 417.364 243.328 415.398 243.328H392.465C390.499 243.328 389.188 242.014 389.188 240.044V228.223C389.188 226.253 390.499 224.94 392.465 224.94ZM392.465 257.775H415.398C417.364 257.775 418.675 259.088 418.675 261.059V272.879C418.675 274.849 417.364 276.163 415.398 276.163H392.465C390.499 276.163 389.188 274.849 389.188 272.879V261.059C389.188 259.088 390.499 257.775 392.465 257.775ZM392.465 293.237H415.398C417.364 293.237 418.675 294.551 418.675 296.521V308.342C418.675 310.312 417.364 311.625 415.398 311.625H392.465C390.499 311.625 389.188 310.312 389.188 308.342V296.521C389.188 294.551 390.499 293.237 392.465 293.237ZM392.465 326.073H415.398C417.364 326.073 418.675 327.386 418.675 329.356V341.177C418.675 343.147 417.364 344.461 415.398 344.461H392.465C390.499 344.461 389.188 343.147 389.188 341.177V329.356C389.188 327.386 390.499 326.073 392.465 326.073ZM392.465 358.908H415.398C417.364 358.908 418.675 360.221 418.675 362.192V374.012C418.675 375.326 417.364 377.296 415.398 377.296H392.465C390.499 377.296 389.188 375.982 389.188 374.012V362.192C389.188 360.221 390.499 358.908 392.465 358.908ZM380.015 385.833C385.257 387.147 390.499 387.147 396.396 387.147C444.229 387.147 483.544 347.744 483.544 299.804C483.544 251.865 444.229 212.462 396.396 212.462C390.499 212.462 385.257 213.119 380.015 213.776V385.833V385.833Z" fill="white"/>
            <path d="M398.362 205.238C450.782 205.238 493.373 247.924 493.373 300.461C493.373 352.998 450.782 395.684 398.362 395.684C345.942 395.684 303.351 352.998 303.351 300.461C303.351 247.924 345.942 205.238 398.362 205.238ZM398.362 177C330.871 177 275.831 232.163 275.831 299.804C275.831 367.445 330.871 422.609 398.362 422.609C465.852 422.609 520.893 367.445 520.893 299.804C520.893 232.163 465.852 177 398.362 177Z" fill="white"/>
          </g>
        </svg>';
        break;
      }

      case 'facebook': {
        $html = '<svg viewBox="0 0 448 512">
          <path d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"></path>
        </svg>';
        break;
      }

      case 'instagram': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
        <path d="M100,50c0,10,0,16.8-0.2,20.6c-0.5,9.1-3,16.1-8.2,21c-5.1,4.9-11.9,7.7-21,8.2C66.8,100,59.8,100,50,100
	        c-10,0-16.8,0-20.6-0.2c-9.1-0.5-16.1-3-21-8.2c-4.9-4.9-7.7-11.9-8.2-21C0,66.8,0,59.8,0,50s0-16.8,0.2-20.6c0.5-9.1,3-16.1,8.2-21
	        c4.9-4.9,11.9-7.7,21-8.2C33.2,0,40.2,0,50,0c10,0,16.8,0,20.6,0.2c9.1,0.5,16.1,3,21,8.2c4.9,5.1,7.7,11.9,8.2,21
	        C99.8,33.2,100,40,100,50z M54.9,8.9c-3,0-4.7,0-4.9,0s-1.9,0-4.9,0c-3,0-5.4,0-6.8,0c-1.6,0-3.7,0-6.3,0.2c-2.6,0-4.9,0.2-6.8,0.7
	        c-1.9,0.2-3.5,0.7-4.7,1.2c-2.1,0.9-4,2.1-5.8,3.7c-1.6,1.6-2.8,3.5-3.7,5.8c-0.5,1.2-0.9,2.8-1.2,4.7c-0.2,1.9-0.5,4-0.7,6.8
	        c0,2.6-0.2,4.7-0.2,6.3c0,1.6,0,4,0,6.8c0,3,0,4.7,0,4.9c0,0.2,0,1.9,0,4.9s0,5.4,0,6.8c0,1.6,0,3.7,0.2,6.3c0,2.6,0.2,4.9,0.7,6.8
	        c0.5,1.9,0.7,3.5,1.2,4.7c0.9,2.1,2.1,4,3.7,5.8c1.6,1.6,3.5,2.8,5.8,3.7c1.2,0.5,2.8,0.9,4.7,1.2c1.9,0.2,4,0.5,6.8,0.7
	        c2.8,0.2,4.7,0.2,6.3,0.2s4,0,6.8,0c3,0,4.7,0,4.9,0s1.9,0,4.9,0s5.4,0,6.8,0c1.6,0,3.7,0,6.3-0.2c2.6,0,4.9-0.2,6.8-0.7
	        c1.9-0.2,3.5-0.7,4.7-1.2c2.1-0.9,4-2.1,5.8-3.7c1.6-1.6,2.8-3.5,3.7-5.8c0.5-1.2,0.9-2.8,1.2-4.7s0.5-4,0.7-6.8
	        c0-2.6,0.2-4.7,0.2-6.3c0-1.6,0-4,0-6.8c0-3,0-4.7,0-4.9c0-0.2,0-1.9,0-4.9s0-5.4,0-6.8c0-1.6,0-3.7-0.2-6.3c0-2.6-0.2-4.9-0.7-6.8
	        c-0.2-1.9-0.7-3.5-1.2-4.7c-0.9-2.1-2.1-4-3.7-5.8c-1.6-1.6-3.5-2.8-5.8-3.7c-1.2-0.5-2.8-0.9-4.7-1.2c-1.9-0.2-4-0.5-6.8-0.7
	        c-2.6,0-4.7-0.2-6.3-0.2C60.3,8.9,57.9,8.9,54.9,8.9z M68,31.8c4.9,4.9,7.5,11,7.5,18.2S72.9,63.1,68,68.2c-4.9,4.9-11,7.5-18.2,7.5
	        s-13.1-2.6-18.2-7.5c-4.9-4.9-7.5-11-7.5-18.2s2.6-13.1,7.5-18.2c4.9-4.9,11-7.5,18.2-7.5S63.1,26.6,68,31.8z M61.7,61.7
	        c3.3-3.3,4.9-7.2,4.9-11.7s-1.6-8.6-4.9-11.9c-3.3-3.3-7.2-4.9-11.9-4.9s-8.6,1.6-11.9,4.9c-3.3,3.3-4.9,7.2-4.9,11.9
	        s1.6,8.6,4.9,11.7c3.3,3.3,7.2,4.9,11.9,4.9C54.4,66.6,58.4,65,61.7,61.7z M80.8,18.9c1.2,1.2,1.9,2.6,1.9,4.2c0,1.6-0.7,3-1.9,4.2
	        c-1.2,1.2-2.6,1.9-4.2,1.9s-3-0.7-4.2-1.9c-1.2-1.2-1.9-2.6-1.9-4.2c0-1.6,0.7-3,1.9-4.2c1.2-1.2,2.6-1.9,4.2-1.9
	        S79.7,17.8,80.8,18.9z"/>
        </svg>';
        break;
      }

      case 'linkedin': {
        $html = '<svg x="0px" y="0px"viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;"  xml:space="preserve">
          <path d="M22.4,100H1.7V33.2h20.7V100z M12,24.1C5.4,24.1,0,18.6,0,12S5.4,0,12,0s12,5.4,12,12S18.6,24.1,12,24.1z M100,100H79.3
	        V67.5c0-7.7-0.2-17.7-10.8-17.7c-10.8,0-12.4,8.4-12.4,17.1V100H35.4V33.2h19.9v9.1h0.3c2.8-5.2,9.5-10.8,19.6-10.8
	        c21,0,24.8,13.8,24.8,31.8L100,100L100,100L100,100z"/>
        </svg>';
        break;
      }

      case 'twitter': {
        $html = '<svg viewBox="0 0 512 512">
          <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
        </svg>';
        break;
      }

      case 'icon.add-to-cart': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 104 100" style="enable-background:new 0 0 104 100;" xml:space="preserve">
          <g>
	          <path d="M76,77.4c-6.2,0-11.3,5.1-11.3,11.3S69.8,100,76,100s11.3-5.1,11.3-11.3S82.3,77.4,76,77.4z M76,93.5
		          c-2.7,0-4.9-2.2-4.9-4.9s2.2-4.9,4.9-4.9s4.9,2.2,4.9,4.9S78.7,93.5,76,93.5z"/>
	          <path d="M43.7,77.4c-6.2,0-11.3,5.1-11.3,11.3S37.4,100,43.7,100S55,94.9,55,88.7S49.9,77.4,43.7,77.4z M43.7,93.5
		          c-2.7,0-4.9-2.2-4.9-4.9s2.2-4.9,4.9-4.9s4.9,2.2,4.9,4.9S46.3,93.5,43.7,93.5z"/>
	          <path d="M101.2,25.7c-1.7-0.5-3.5,0.5-4,2.2l-10,34c-0.4,1.5-1.7,2.5-3.1,2.5c0,0,0,0,0,0H37.3c0,0,0,0,0,0c-1.5,0-2.8-1-3.1-2.4
		          l-14-52.4C18.7,4,13.6,0,7.7,0H3.2C1.4,0,0,1.4,0,3.2s1.4,3.2,3.2,3.2h4.5c2.9,0,5.5,2,6.3,4.8l14,52.4c1.1,4.2,5,7.2,9.4,7.2
		          c0,0,0,0,0,0H84c0,0,0,0,0,0c4.4,0,8.2-3,9.4-7.2l10-33.9C103.9,28,102.9,26.2,101.2,25.7z"/>
	          <path d="M48.5,34.6h9.7v9.7c0,1.8,1.4,3.2,3.2,3.2c1.8,0,3.2-1.4,3.2-3.2v-9.7h9.7c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2
		          h-9.7v-9.7c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2v9.7h-9.7c-1.8,0-3.2,1.4-3.2,3.2C45.3,33.1,46.7,34.6,48.5,34.6z"/>
          </g>
          </svg>';
        break;
      }

      case 'icon.arrow': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 77" style="enable-background:new 0 0 100 77;" xml:space="preserve">
          <polygon points="94.3,32.5 61.8,0 56.2,5.7 84.5,34 0,34 0,42 84.9,42 56.2,70.7 61.8,76.4 94.3,43.8 100,38.2 "/>
        </svg>';
        break;
      }

      case 'icon.chevron': {
        $html = '<svg viewBox="0 0 36 100">
          <path d="M1.93,100a1.69,1.69,0,0,1-.56-.1l-.46-.21A1.92,1.92,0,0,1,.4,97.1L31.6,50,.3,2.89A1.79,1.79,0,0,1,0,1.49,1.7,1.7,0,0,1,.91.3,1.7,1.7,0,0,1,2.24,0,2.16,2.16,0,0,1,3.47.82L36,50,3.47,99.17a4.11,4.11,0,0,1-.72.57A1.52,1.52,0,0,1,1.93,100Z"/>
        </svg>';
        break;
      }

      case 'icon.open-email': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 96" style="enable-background:new 0 0 100 96;" xml:space="preserve">
          <g>
	          <path d="M99.8,35.3c-0.1,0-0.5-0.4-0.8-0.9c-0.5-0.8-4.1-3.5-23.2-17.3C63.2,8,52.7,0.6,52.4,0.4c-1-0.4-2.7-0.5-3.8-0.2
		          c-0.5,0.2-1.2,0.4-1.5,0.6C45.3,2,2.5,32.9,1.9,33.5c-0.4,0.4-1,1.2-1.3,1.9L0,36.6V64v27.4l0.5,1c0.8,1.7,2.5,3,4.5,3.4
		          C5.6,96,20,96,50.8,96l44.8-0.1l1-0.5c1.3-0.7,2.3-1.7,2.9-3l0.5-1V63.3C100,41,99.9,35.3,99.8,35.3z M27.1,20.3
		          C45.3,7.2,49.8,4,50.2,4.1c0.3,0,10.4,7.2,22.8,16.2c12.3,8.8,22.3,16.2,22.2,16.3c-0.1,0.2-44.9,24.7-45.2,24.7
		          C49.8,61.2,4.9,37,4.7,36.7C4.6,36.6,14.7,29.2,27.1,20.3z M95.6,91.4c-0.3,0.3-4.7,0.3-45.6,0.3H4.7l-0.3-0.4
		          C4.1,90.9,4.1,88.2,4.1,66V41.2l1.4,0.8c0.8,0.4,11,6,22.7,12.3C42.5,62,49.6,65.8,50,65.8c0.8,0-0.7,0.7,23.9-12.7
		          c11.8-6.4,21.5-11.8,21.7-11.8c0.2-0.1,0.3,4.1,0.3,24.9C95.9,88.6,95.9,91.1,95.6,91.4z"/>
	          <g>
		          <path d="M38.6,35.5h8.5v8.6c0,1.6,1.3,2.9,2.8,2.9s2.8-1.3,2.8-2.9v-8.6h8.5c1.6,0,2.8-1.3,2.8-2.9c0-1.6-1.3-2.9-2.8-2.9h-8.5
			          v-8.6c0-1.6-1.3-2.9-2.8-2.9s-2.8,1.3-2.8,2.9v8.6h-8.5c-1.6,0-2.8,1.3-2.8,2.9C35.8,34.2,37.1,35.5,38.6,35.5z"/>
	          </g>
          </g>
          </svg>';
        break;
      }

      case 'icon.play': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	        <g>
		        <path d="M50,0C22.4,0,0,22.4,0,50s22.4,50,50,50s50-22.4,50-50S77.6,0,50,0z M50,94C25.7,94,6,74.3,6,50S25.7,6,50,6s44,19.7,44,44S74.3,94,50,94z"/>
		        <path d="M35.6,76.4L80,50.8L35.6,25.2V76.4z M41.6,35.6L68,50.8L41.6,66V35.6z"/>
	        </g>
        </svg>';
        break;
      }

      case 'icon.plus': {
        $html = '<svg width="100" height="100" viewBox="0 0 100 100">
          <path d="M100 54.5263H54.5263V100H45.4737V54.5263H0V45.4737H45.4737V0H54.5263V45.4737H100V54.5263Z"/>
        </svg>';
        break;
      }

      case 'icon.plus-circle': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	        <g>
		        <path d="M85.6,14.9C76.3,5.4,63.8,0.1,50.4,0C22.9-0.2,0.2,22,0,49.6C-0.2,77.2,22,99.8,49.6,100c0.1,0,0.3,0,0.4,0
			        c13.2,0,25.6-5.1,35.1-14.4c9.5-9.4,14.8-21.9,14.9-35.2C100.1,37.1,95,24.5,85.6,14.9z M80.9,81.4C72.6,89.5,61.6,94,50,94
			        c-0.1,0-0.2,0-0.4,0C25.4,93.8,5.8,73.9,6,49.6C6.2,25.5,25.9,6,50,6c0.1,0,0.2,0,0.4,0c11.8,0.1,22.8,4.8,31,13.1
			        c8.2,8.4,12.7,19.5,12.6,31.2C93.9,62.1,89.2,73.1,80.9,81.4z"/>
		        <polygon points="53.2,27 46.2,27 46,46 27,45.8 27,52.8 46,53 45.8,73 52.8,73 53,53 73,53.2 73,46.2 53,46 	"/>
	        </g>
        </svg>';
        break;
      }

      case 'logo.wide': {
        $html = '<svg width="300" height="77" viewBox="0 0 300 77" xmlns="http://www.w3.org/2000/svg">
          <path d="M99.9299 64.1436C99.9299 66.7086 101.854 69.2736 105.701 69.2736C107.625 69.2736 109.869 68.3117 110.831 66.0673L113.396 66.7086C111.793 70.5561 108.266 71.1973 105.701 71.1973C101.533 71.1973 97.6855 68.6323 97.6855 63.5023C97.6855 57.7311 102.174 55.8073 105.701 55.8073C111.472 55.8073 113.717 60.6167 113.717 63.5023V63.8229H99.9299V64.1436ZM110.511 61.8992C109.869 59.6548 108.266 58.0517 105.381 58.0517C103.136 58.0517 100.892 59.3342 99.9299 61.8992H110.511Z" />
          <path d="M129.107 70.8767V63.1817C129.107 61.8992 129.107 61.2579 128.786 60.9373C128.466 59.6548 126.862 58.3723 124.297 58.3723C122.374 58.3723 120.129 59.0135 119.167 61.5785C118.847 62.5404 118.847 63.1817 118.847 64.1436V70.8767H116.282V56.4485H118.847V58.0517C119.167 57.7311 119.809 57.0898 120.771 56.7692C121.732 56.4485 123.015 56.1279 124.618 56.1279C127.824 56.1279 130.389 57.7311 131.031 59.6548C131.351 60.9373 131.351 61.8992 131.351 63.1817V70.8767H129.107Z" />
          <path d="M138.405 58.3723V66.0673C138.405 67.991 138.726 68.6323 140.329 68.6323C140.97 68.6323 141.932 68.3117 142.252 68.3117V70.8767C141.291 70.8767 140.649 71.1973 139.687 71.1973C138.726 71.1973 137.443 71.1973 136.802 70.2354C136.161 69.5942 136.161 68.6323 136.161 67.0292V58.3723H133.916V56.4485H135.84V51.3185H138.405V56.4485H141.932V58.3723H138.405Z" />
          <path d="M145.779 64.1436C145.779 66.7086 147.703 69.2736 151.551 69.2736C153.474 69.2736 155.719 68.3117 156.681 66.0673L159.246 66.7086C157.642 70.5561 154.116 71.1973 151.551 71.1973C147.382 71.1973 143.535 68.6323 143.535 63.5023C143.535 57.7311 148.024 55.8073 151.551 55.8073C157.322 55.8073 159.566 60.6167 159.566 63.5023V63.8229H145.779V64.1436ZM156.36 61.8992C155.719 59.6548 154.116 58.0517 151.23 58.0517C148.986 58.0517 146.741 59.3342 145.779 61.8992H156.36Z" />
          <path d="M163.734 56.4485V58.0517C164.055 57.731 165.658 56.1279 168.864 56.1279C169.506 56.1279 170.147 56.1279 170.467 56.4485V59.3342C170.147 59.0136 169.506 59.0135 168.223 59.0135C167.261 59.0135 163.734 59.0135 163.734 63.1817V71.1973H161.169V56.4485H163.734Z" />
          <path d="M175.918 58.3723V66.0673C175.918 67.991 176.239 68.6323 177.842 68.6323C178.483 68.6323 179.445 68.3117 179.766 68.3117V70.8767C178.804 70.8767 178.162 71.1973 177.201 71.1973C176.239 71.1973 174.956 71.1973 174.315 70.2354C173.674 69.5942 173.674 68.6323 173.674 67.0292V58.3723H171.109V56.4485H173.353V51.3185H175.918V56.4485H179.445V58.3723H175.918Z" />
          <path d="M195.476 71.1973C193.232 71.1973 192.911 69.9148 192.911 68.6323C192.591 69.5942 190.667 71.5179 186.499 71.5179C181.369 71.5179 180.407 68.6323 180.407 66.7086C180.407 64.1436 182.01 63.1817 182.651 62.8611C183.934 62.2198 185.857 62.2198 189.064 62.2198H192.911V61.8992C192.911 59.9754 192.911 58.3723 188.422 58.3723C186.499 58.3723 184.254 58.6929 183.934 60.2961L181.369 59.6548C181.369 59.6548 181.689 58.6929 182.01 58.3723C182.651 57.4104 183.613 56.1279 188.422 56.1279C188.743 56.1279 190.667 56.1279 192.27 56.4485C192.591 56.4485 193.873 56.7692 194.835 58.0517C195.476 59.0136 195.476 59.9754 195.476 60.9373V66.7086C195.476 68.3117 195.797 68.9529 197.079 68.9529C197.4 68.9529 198.041 68.9529 198.041 68.9529V70.8767C197.079 71.1973 196.117 71.1973 195.476 71.1973ZM188.102 64.1436C185.537 64.1436 182.972 64.1435 182.972 66.3879C182.972 67.6704 184.254 69.2736 186.819 69.2736C188.422 69.2736 191.308 68.9529 192.591 66.3879C192.911 65.426 192.911 64.4642 192.911 64.1436H188.102Z" />
          <path d="M199.644 54.2042V51.3185H202.209V54.2042H199.644ZM199.644 70.8767V56.4485H202.209V70.8767H199.644Z" />
          <path d="M218.882 70.8767V63.1817C218.882 61.8992 218.882 61.2579 218.561 60.9373C217.92 59.6548 216.637 58.3723 214.072 58.3723C212.149 58.3723 209.904 59.0135 208.942 61.5785C208.622 62.5404 208.622 63.1817 208.622 64.1436V70.8767H206.057V56.4485H208.622V58.0517C208.942 57.7311 209.584 57.0898 210.546 56.7692C211.187 56.4485 212.469 56.1279 214.393 56.1279C217.599 56.1279 220.164 57.7311 220.806 59.6548C221.126 60.9373 221.126 61.8992 221.126 63.1817V70.8767H218.882Z" />
          <path d="M249.662 70.8767V63.1817C249.662 62.2198 249.662 61.5785 249.341 60.6167C249.021 59.6548 247.738 58.3723 245.173 58.3723C242.608 58.3723 241.005 59.9754 240.364 60.9373C240.043 61.5786 240.043 61.8992 240.043 63.1817V70.5561H237.478V63.1817C237.478 62.5404 237.478 61.5786 237.157 60.9373C236.516 58.0517 233.631 58.0517 232.989 58.0517C231.386 58.0517 228.821 59.0135 228.18 60.6167C227.859 61.2579 227.859 62.2198 227.859 63.1817V70.5561H225.294V56.4485H227.859V58.3723C228.821 57.4104 230.745 56.1279 233.631 56.1279C235.554 56.1279 238.119 56.7692 239.081 58.6929C239.722 58.0517 241.326 56.1279 245.494 56.1279C248.059 56.1279 250.944 57.0898 251.906 59.9754C252.227 60.9373 252.227 61.5785 252.227 63.1817V70.8767H249.662Z" />
          <path d="M257.677 64.1436C257.677 66.7086 259.601 69.2736 263.449 69.2736C265.372 69.2736 267.617 68.3117 268.579 66.0673L271.144 66.7086C269.541 70.5561 266.014 71.1973 263.449 71.1973C259.281 71.1973 255.433 68.6323 255.433 63.5023C255.433 57.7311 259.922 55.8073 263.449 55.8073C269.22 55.8073 271.464 60.6167 271.464 63.5023V63.8229H257.677V64.1436ZM268.258 61.8992C267.617 59.6548 266.014 58.0517 263.128 58.0517C260.884 58.0517 258.639 59.3342 257.677 61.8992H268.258Z" />
          <path d="M286.854 70.8767V63.1817C286.854 61.8992 286.854 61.2579 286.534 60.9373C286.213 59.6548 284.61 58.3723 282.045 58.3723C280.121 58.3723 277.877 59.0135 276.915 61.5785C276.594 62.5404 276.594 63.1817 276.594 64.1436V70.8767H274.029V56.4485H276.594V58.0517C276.915 57.7311 277.556 57.0898 278.518 56.7692C279.159 56.4485 280.442 56.1279 282.366 56.1279C285.572 56.1279 288.137 57.7311 288.778 59.6548C289.099 60.9373 289.099 61.8992 289.099 63.1817V70.8767H286.854Z" />
          <path d="M296.152 58.3723V66.0673C296.152 67.991 296.473 68.6323 298.076 68.6323C298.717 68.6323 299.679 68.3117 300 68.3117V70.8767C299.038 70.8767 298.397 71.1973 297.435 71.1973C296.473 71.1973 295.191 71.1973 294.549 70.2354C293.908 69.5942 293.908 68.6323 293.908 67.0292V58.3723H291.664V56.4485H293.908V51.3185H296.152V56.4485H299.679V58.3723H296.152Z" />
          <path d="M112.445 40.4832C105.257 40.4832 97.7111 37.249 97.7111 27.5464C97.7111 21.4374 102.023 14.9691 112.085 14.9691C123.585 14.9691 127.178 21.7968 127.178 27.5464C127.178 35.8116 121.428 40.4832 112.445 40.4832ZM112.085 20.7187C109.21 20.7187 105.976 22.8748 105.976 27.5464C105.976 31.4993 107.773 34.7335 112.804 34.7335C117.116 34.7335 119.272 31.4993 119.272 27.9058C118.913 23.2342 116.397 20.7187 112.085 20.7187Z" />
          <path d="M137.599 16.0471V18.5626C138.318 17.8439 140.474 15.3284 145.146 15.3284C148.739 15.3284 151.255 16.4065 152.692 18.2033C154.13 17.1252 156.645 15.3284 160.957 15.3284C165.988 15.3284 169.223 17.4846 169.223 22.1562V39.7645H161.317V26.109C161.317 22.8749 160.598 21.0781 157.364 21.0781C154.489 21.0781 153.052 22.5155 153.052 25.3903V39.7645H145.146V25.3903C145.146 22.1562 143.708 21.0781 141.193 21.0781C137.599 21.0781 137.24 23.9529 137.24 26.109V39.7645H129.334V16.0471H137.599Z" />
          <path d="M190.424 39.7645V25.031C190.424 22.8749 189.706 21.0781 186.471 21.0781C182.878 21.0781 182.159 23.2342 182.159 26.4684V39.7645H173.894V16.0471H182.159V18.2033C183.597 16.7658 186.112 15.3284 189.706 15.3284C195.455 15.3284 198.33 17.8439 198.33 22.5155V39.7645H190.424Z" />
          <path d="M211.267 16.0471H203.361V39.7645H211.267V16.0471Z" />
          <path d="M225.282 20.7187V39.7645H217.376V20.7187H213.064V16.4065H217.376V12.8129C217.376 8.86004 220.969 6.70392 225.641 6.70392C227.438 6.70392 230.313 7.06328 230.313 7.06328V12.4536H228.875C225.641 12.4536 225.282 13.1723 225.282 14.6097V16.0471H230.672V20.7187H225.282Z" />
          <path d="M242.531 16.0471H234.625V39.7645H242.531V16.0471Z" />
          <path d="M247.202 39.7645V7.42261H255.108V39.7645H247.202Z" />
          <path d="M268.045 16.0471V18.5626C268.764 17.8439 270.92 15.3284 275.591 15.3284C279.185 15.3284 281.7 16.4065 283.138 18.2033C284.575 17.1252 287.091 15.3284 291.403 15.3284C296.434 15.3284 299.668 17.4846 299.668 22.1562V39.7645H291.762V26.109C291.762 22.8749 291.043 21.0781 287.809 21.0781C284.575 21.0781 283.497 22.5155 283.497 25.3903V39.7645H275.591V25.3903C275.591 22.1562 274.154 21.0781 271.638 21.0781C268.045 21.0781 267.685 23.9529 267.685 26.109V39.7645H259.78V16.0471H268.045Z" />
          <path d="M204.08 7.78198H210.548C210.908 7.78198 211.267 8.14133 211.267 8.86004V12.4536C211.267 12.8129 210.908 13.5316 210.548 13.5316H204.08C203.72 13.5316 203.361 13.1723 203.361 12.4536V8.86004C203.361 8.14133 203.72 7.78198 204.08 7.78198Z" />
          <path d="M235.344 7.78198H241.812C242.171 7.78198 242.531 8.14133 242.531 8.86004V12.4536C242.531 12.8129 242.171 13.5316 241.812 13.5316H235.344C234.984 13.5316 234.625 13.1723 234.625 12.4536V8.86004C234.625 8.14133 234.984 7.78198 235.344 7.78198Z" />
          <path d="M36.2365 14.861H43.3616C43.9723 14.861 44.3795 15.2682 44.3795 15.8789V19.5433C44.3795 20.154 43.9723 20.5612 43.3616 20.5612H36.2365C35.6257 20.5612 35.2186 20.154 35.2186 19.5433V15.8789C35.2186 15.2682 35.6257 14.861 36.2365 14.861ZM36.2365 25.0398H43.3616C43.9723 25.0398 44.3795 25.447 44.3795 26.0577V29.7221C44.3795 30.3328 43.9723 30.7399 43.3616 30.7399H36.2365C35.6257 30.7399 35.2186 30.3328 35.2186 29.7221V26.0577C35.2186 25.447 35.6257 25.0398 36.2365 25.0398ZM36.2365 36.0329H43.3616C43.9723 36.0329 44.3795 36.4401 44.3795 37.0508V40.7151C44.3795 41.3259 43.9723 41.733 43.3616 41.733H36.2365C35.6257 41.733 35.2186 41.3259 35.2186 40.7151V37.0508C35.2186 36.4401 35.6257 36.0329 36.2365 36.0329ZM36.2365 46.2117H43.3616C43.9723 46.2117 44.3795 46.6188 44.3795 47.2296V50.8939C44.3795 51.5047 43.9723 51.9118 43.3616 51.9118H36.2365C35.6257 51.9118 35.2186 51.5047 35.2186 50.8939V47.2296C35.2186 46.6188 35.6257 46.2117 36.2365 46.2117ZM36.2365 56.3905H43.3616C43.9723 56.3905 44.3795 56.7976 44.3795 57.4083V61.0727C44.3795 61.4799 43.9723 62.0906 43.3616 62.0906H36.2365C35.6257 62.0906 35.2186 61.6834 35.2186 61.0727V57.4083C35.2186 56.7976 35.6257 56.3905 36.2365 56.3905ZM32.3685 64.7371C33.9971 65.1442 35.6257 65.1442 37.4579 65.1442C52.3189 65.1442 64.5335 52.9297 64.5335 38.0687C64.5335 23.2076 52.3189 10.9931 37.4579 10.9931C35.6257 10.9931 33.9971 11.1967 32.3685 11.4003V64.7371V64.7371Z" />
          <path d="M38.0686 8.75375C54.3547 8.75375 67.5871 21.9862 67.5871 38.2722C67.5871 54.5583 54.3547 67.7907 38.0686 67.7907C21.7826 67.7907 8.55017 54.5583 8.55017 38.2722C8.55017 21.9862 21.7826 8.75375 38.0686 8.75375ZM38.0686 0C17.1004 0 0 17.1004 0 38.0686C0 59.0369 17.1004 76.1373 38.0686 76.1373C59.0369 76.1373 76.1373 59.0369 76.1373 38.0686C76.1373 17.1004 59.0369 0 38.0686 0Z" />
        </svg>';
        break;
      }

      case 'icon.www': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 490 490" style="enable-background:new 0 0 490 490;" xml:space="preserve">
          <path d="M245,0C109.7,0,0,109.7,0,245s109.7,245,245,245s245-109.7,245-245S380.3,0,245,0z M458.6,229.7H406
	          c-1.2-25.4-5.1-48.7-11.2-69.6h46.9C451.1,181.7,456.9,205.1,458.6,229.7z M425.3,129.5h-41.2c-13.7-33-31.5-59.1-48.3-78.4
	          C372.5,68.3,403.5,95.6,425.3,129.5z M321,260.3h54.4c-1.3,25.8-5.8,49-12.5,69.6h-48.2C318.8,306,320.7,282.8,321,260.3z
	           M312.6,160.1h50.2c6.7,20.5,11.3,43.7,12.7,69.6h-54.8C319.4,205.1,316.5,181.8,312.6,160.1z M350.7,129.5h-44.6
	          c-9.8-40.5-22.3-73-31.6-94C286,43.7,324.5,74.1,350.7,129.5z M290.5,260.3c-0.4,22.4-2.5,45.7-6.8,69.6h-77.2
	          c-4.3-23.9-6.4-47.2-6.8-69.6H290.5z M200,229.7c1.2-24.8,4.3-48.1,8.5-69.6h73.1c4.2,21.5,7.3,44.8,8.5,69.6H200z M274.7,129.5
	          h-59.3c9.6-37.4,21.4-66.8,29.7-85C253.3,62.7,265.1,92.1,274.7,129.5z M215.6,35.5c-9.3,21-21.9,53.5-31.6,94h-44.6
	          C165.5,74.1,204,43.7,215.6,35.5z M169.4,229.7h-54.8c1.4-25.9,6-49,12.6-69.6h50.2C173.5,181.8,170.6,205.1,169.4,229.7z
	           M175.2,329.9h-48c-6.7-20.6-11.3-43.8-12.7-69.6H169C169.3,282.8,171.2,306,175.2,329.9z M154.3,51.1
	          c-16.8,19.4-34.6,45.5-48.3,78.4H64.7C86.5,95.6,117.5,68.3,154.3,51.1z M48.2,160.1h46.9c-6,20.9-9.9,44.1-11.2,69.6H31.4
	          C33.1,205.1,38.9,181.7,48.2,160.1z M31.4,260.3h52.5c1.2,25.4,5,48.7,11,69.6H48.2C38.9,308.3,33.1,284.9,31.4,260.3z M64.7,360.5
	          h41c13.5,32.6,31.1,58.6,47.8,78C117.1,421.3,86.3,394.1,64.7,360.5z M139.3,360.5h42.3c7.5,31.2,18.7,63.1,34.7,95.1
	          C205.5,447.8,166,416.8,139.3,360.5z M245,444.3c-14.4-28.2-24.8-56.3-31.9-83.8h63.9C269.8,388.1,259.4,416.2,245,444.3z
	           M273.6,455.6c16.1-32,27.3-63.9,34.8-95.1H351C324.3,417.1,284.7,447.8,273.6,455.6z M336.6,438.5c16.7-19.4,34.3-45.4,47.7-78h41
	          C403.7,394.1,372.9,421.3,336.6,438.5z M441.8,329.9h-46.7c6-20.9,9.8-44.1,11-69.6h52.5C456.9,284.9,451.1,308.3,441.8,329.9z"/>
          </svg>';
        break;
      }

    }

    return $html;

  }

  // --------------------------- SVG Icon
  public function render_svg_icon( $type = "" ) {

    switch ( $type ) {
      case "add-to-cart": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 104 100' style='enable-background:new 0 0 104 100;' xml:space='preserve'>
            <g>
	            <path d='M76,77.4c-6.2,0-11.3,5.1-11.3,11.3S69.8,100,76,100s11.3-5.1,11.3-11.3S82.3,77.4,76,77.4z M76,93.5
		            c-2.7,0-4.9-2.2-4.9-4.9s2.2-4.9,4.9-4.9s4.9,2.2,4.9,4.9S78.7,93.5,76,93.5z'/>
	            <path d='M43.7,77.4c-6.2,0-11.3,5.1-11.3,11.3S37.4,100,43.7,100S55,94.9,55,88.7S49.9,77.4,43.7,77.4z M43.7,93.5
		            c-2.7,0-4.9-2.2-4.9-4.9s2.2-4.9,4.9-4.9s4.9,2.2,4.9,4.9S46.3,93.5,43.7,93.5z'/>
	            <path d='M101.2,25.7c-1.7-0.5-3.5,0.5-4,2.2l-10,34c-0.4,1.5-1.7,2.5-3.1,2.5c0,0,0,0,0,0H37.3c0,0,0,0,0,0c-1.5,0-2.8-1-3.1-2.4
		            l-14-52.4C18.7,4,13.6,0,7.7,0H3.2C1.4,0,0,1.4,0,3.2s1.4,3.2,3.2,3.2h4.5c2.9,0,5.5,2,6.3,4.8l14,52.4c1.1,4.2,5,7.2,9.4,7.2
		            c0,0,0,0,0,0H84c0,0,0,0,0,0c4.4,0,8.2-3,9.4-7.2l10-33.9C103.9,28,102.9,26.2,101.2,25.7z'/>
	            <path d='M48.5,34.6h9.7v9.7c0,1.8,1.4,3.2,3.2,3.2c1.8,0,3.2-1.4,3.2-3.2v-9.7h9.7c1.8,0,3.2-1.4,3.2-3.2c0-1.8-1.4-3.2-3.2-3.2
		            h-9.7v-9.7c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2v9.7h-9.7c-1.8,0-3.2,1.4-3.2,3.2C45.3,33.1,46.7,34.6,48.5,34.6z'/>
            </g>
          </svg>";
      }
      case "arrow": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 100 77' style='enable-background:new 0 0 100 77;' xml:space='preserve'>
            <polygon points='94.3,32.5 61.8,0 56.2,5.7 84.5,34 0,34 0,42 84.9,42 56.2,70.7 61.8,76.4 94.3,43.8 100,38.2 '/>
          </svg>
        ";
      }
      case "chevron": {
        return "
          <svg viewBox='0 0 36 100'>
            <path d='M1.93,100a1.69,1.69,0,0,1-.56-.1l-.46-.21A1.92,1.92,0,0,1,.4,97.1L31.6,50,.3,2.89A1.79,1.79,0,0,1,0,1.49,1.7,1.7,0,0,1,.91.3,1.7,1.7,0,0,1,2.24,0,2.16,2.16,0,0,1,3.47.82L36,50,3.47,99.17a4.11,4.11,0,0,1-.72.57A1.52,1.52,0,0,1,1.93,100Z'/>
          </svg>
        ";
      }
      case "open-email": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 100 96' style='enable-background:new 0 0 100 96;' xml:space='preserve'>
            <g>
	            <path d='M99.8,35.3c-0.1,0-0.5-0.4-0.8-0.9c-0.5-0.8-4.1-3.5-23.2-17.3C63.2,8,52.7,0.6,52.4,0.4c-1-0.4-2.7-0.5-3.8-0.2
		            c-0.5,0.2-1.2,0.4-1.5,0.6C45.3,2,2.5,32.9,1.9,33.5c-0.4,0.4-1,1.2-1.3,1.9L0,36.6V64v27.4l0.5,1c0.8,1.7,2.5,3,4.5,3.4
		            C5.6,96,20,96,50.8,96l44.8-0.1l1-0.5c1.3-0.7,2.3-1.7,2.9-3l0.5-1V63.3C100,41,99.9,35.3,99.8,35.3z M27.1,20.3
		            C45.3,7.2,49.8,4,50.2,4.1c0.3,0,10.4,7.2,22.8,16.2c12.3,8.8,22.3,16.2,22.2,16.3c-0.1,0.2-44.9,24.7-45.2,24.7
		            C49.8,61.2,4.9,37,4.7,36.7C4.6,36.6,14.7,29.2,27.1,20.3z M95.6,91.4c-0.3,0.3-4.7,0.3-45.6,0.3H4.7l-0.3-0.4
		            C4.1,90.9,4.1,88.2,4.1,66V41.2l1.4,0.8c0.8,0.4,11,6,22.7,12.3C42.5,62,49.6,65.8,50,65.8c0.8,0-0.7,0.7,23.9-12.7
		            c11.8-6.4,21.5-11.8,21.7-11.8c0.2-0.1,0.3,4.1,0.3,24.9C95.9,88.6,95.9,91.1,95.6,91.4z'/>
	            <g>
		            <path d='M38.6,35.5h8.5v8.6c0,1.6,1.3,2.9,2.8,2.9s2.8-1.3,2.8-2.9v-8.6h8.5c1.6,0,2.8-1.3,2.8-2.9c0-1.6-1.3-2.9-2.8-2.9h-8.5
			            v-8.6c0-1.6-1.3-2.9-2.8-2.9s-2.8,1.3-2.8,2.9v8.6h-8.5c-1.6,0-2.8,1.3-2.8,2.9C35.8,34.2,37.1,35.5,38.6,35.5z'/>
	            </g>
            </g>
          </svg>
        ";
      }
      case "play": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 100 100' style='enable-background:new 0 0 100 100;' xml:space='preserve'>
	          <g>
		          <path d='M50,0C22.4,0,0,22.4,0,50s22.4,50,50,50s50-22.4,50-50S77.6,0,50,0z M50,94C25.7,94,6,74.3,6,50S25.7,6,50,6s44,19.7,44,44S74.3,94,50,94z'/>
		          <path d='M35.6,76.4L80,50.8L35.6,25.2V76.4z M41.6,35.6L68,50.8L41.6,66V35.6z'/>
	          </g>
          </svg>
        ";
      }
      case "plus": {
        return "
          <svg width='100' height='100' viewBox='0 0 100 100'>
            <path d='M100 54.5263H54.5263V100H45.4737V54.5263H0V45.4737H45.4737V0H54.5263V45.4737H100V54.5263Z'/>
          </svg>
        ";
      }
      case "plus-circle": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 100 100' style='enable-background:new 0 0 100 100;' xml:space='preserve'>
	          <g>
		          <path d='M85.6,14.9C76.3,5.4,63.8,0.1,50.4,0C22.9-0.2,0.2,22,0,49.6C-0.2,77.2,22,99.8,49.6,100c0.1,0,0.3,0,0.4,0
			          c13.2,0,25.6-5.1,35.1-14.4c9.5-9.4,14.8-21.9,14.9-35.2C100.1,37.1,95,24.5,85.6,14.9z M80.9,81.4C72.6,89.5,61.6,94,50,94
			          c-0.1,0-0.2,0-0.4,0C25.4,93.8,5.8,73.9,6,49.6C6.2,25.5,25.9,6,50,6c0.1,0,0.2,0,0.4,0c11.8,0.1,22.8,4.8,31,13.1
			          c8.2,8.4,12.7,19.5,12.6,31.2C93.9,62.1,89.2,73.1,80.9,81.4z'/>
		          <polygon points='53.2,27 46.2,27 46,46 27,45.8 27,52.8 46,53 45.8,73 52.8,73 53,53 73,53.2 73,46.2 53,46 	'/>
	          </g>
          </svg>
        ";
      }
      case "www": {
        return "
          <svg x='0px' y='0px' viewBox='0 0 490 490' style='enable-background:new 0 0 490 490;' xml:space='preserve'>
            <path d='M245,0C109.7,0,0,109.7,0,245s109.7,245,245,245s245-109.7,245-245S380.3,0,245,0z M458.6,229.7H406
	            c-1.2-25.4-5.1-48.7-11.2-69.6h46.9C451.1,181.7,456.9,205.1,458.6,229.7z M425.3,129.5h-41.2c-13.7-33-31.5-59.1-48.3-78.4
	            C372.5,68.3,403.5,95.6,425.3,129.5z M321,260.3h54.4c-1.3,25.8-5.8,49-12.5,69.6h-48.2C318.8,306,320.7,282.8,321,260.3z
	             M312.6,160.1h50.2c6.7,20.5,11.3,43.7,12.7,69.6h-54.8C319.4,205.1,316.5,181.8,312.6,160.1z M350.7,129.5h-44.6
	            c-9.8-40.5-22.3-73-31.6-94C286,43.7,324.5,74.1,350.7,129.5z M290.5,260.3c-0.4,22.4-2.5,45.7-6.8,69.6h-77.2
	            c-4.3-23.9-6.4-47.2-6.8-69.6H290.5z M200,229.7c1.2-24.8,4.3-48.1,8.5-69.6h73.1c4.2,21.5,7.3,44.8,8.5,69.6H200z M274.7,129.5
	            h-59.3c9.6-37.4,21.4-66.8,29.7-85C253.3,62.7,265.1,92.1,274.7,129.5z M215.6,35.5c-9.3,21-21.9,53.5-31.6,94h-44.6
	            C165.5,74.1,204,43.7,215.6,35.5z M169.4,229.7h-54.8c1.4-25.9,6-49,12.6-69.6h50.2C173.5,181.8,170.6,205.1,169.4,229.7z
	             M175.2,329.9h-48c-6.7-20.6-11.3-43.8-12.7-69.6H169C169.3,282.8,171.2,306,175.2,329.9z M154.3,51.1
	            c-16.8,19.4-34.6,45.5-48.3,78.4H64.7C86.5,95.6,117.5,68.3,154.3,51.1z M48.2,160.1h46.9c-6,20.9-9.9,44.1-11.2,69.6H31.4
	            C33.1,205.1,38.9,181.7,48.2,160.1z M31.4,260.3h52.5c1.2,25.4,5,48.7,11,69.6H48.2C38.9,308.3,33.1,284.9,31.4,260.3z M64.7,360.5
	            h41c13.5,32.6,31.1,58.6,47.8,78C117.1,421.3,86.3,394.1,64.7,360.5z M139.3,360.5h42.3c7.5,31.2,18.7,63.1,34.7,95.1
	            C205.5,447.8,166,416.8,139.3,360.5z M245,444.3c-14.4-28.2-24.8-56.3-31.9-83.8h63.9C269.8,388.1,259.4,416.2,245,444.3z
	             M273.6,455.6c16.1-32,27.3-63.9,34.8-95.1H351C324.3,417.1,284.7,447.8,273.6,455.6z M336.6,438.5c16.7-19.4,34.3-45.4,47.7-78h41
	            C403.7,394.1,372.9,421.3,336.6,438.5z M441.8,329.9h-46.7c6-20.9,9.8-44.1,11-69.6h52.5C456.9,284.9,451.1,308.3,441.8,329.9z'/>
          </svg>
        ";
      }
    }

    return;

  }

  // --------------------------- WP Navigation
  public function render_wp_navigation( $params = [] ) {

    // ---------------------------------------- Defaults

    extract(array_merge(
      [
        'block_name' => 'navigation',
        'classes' => '',
        'current_id' => false,
        'html' => '',
        'menu_name' => '',
        'wrapper' => 'nav',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ?? $block_name;
    $menu_items = wp_get_nav_menu_items( $menu_name ) ?: [];

    $html .= $wrapper ? '<' . $wrapper . ' class="' . $block_classes . '">' : '';
      foreach ( $menu_items as $item ) {

        $id = $item->object_id;
        $link = $item->url;
        $title = $item->title;
        $object_type = $item->object;
        $link_type = $item->type;
        $post_status = $item->post_status;
        $post_parent = $item->post_parent;
        $menu_item_parrent = $item->menu_item_parent;
        $active = ( $id == $current_id ) ? true : false;
        $target = ( 'custom' == $link_type ) ? '_blank' : '_self';
        $rel = ( 'custom' == $link_type ) ? 'noopener' : '';

        $block_classes = $post_parent ? $block_name . '__item child' : $block_name . '__item';
        $style = ( $menu_name === 'Mobile Menu' && $post_parent ) ? 'mobile-menu-child' : '';

        if ( 'publish' === $post_status ) {

          $html .= '<div
            class="' . $block_classes . '"
            data-id="' . $id . '"
            data-post-parent-id="' . $post_parent . '"
            data-menu-item-parent-id="' . $menu_item_parrent . '"
          >';

            $html .= $this->render_link([
              'active' => $active,
              'classes' => $block_name . '__link',
              'target' => $target,
              'rel' => $rel,
              'style' => $style,
              'title' => $title,
              'link' => $link,
            ]);

          $html .= '</div>';

        }

      }
    $html .= $wrapper ? '</' . $wrapper . '>' : '';

    return $html;

  }

  // ---------------------------------------- WP Navigation Item
  public function render_wp_navigation_item( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        "block_name" => "navigation",
        "current_id" => 0,
        "html" => "",
        "menu_name" => "",
      ],
      $params
    ));

    // ---------------------------------------- Data
    $menu_items = wp_get_nav_menu_items( $menu_name ) ?: [];

    foreach ( $menu_items as $item ) {

      $id = $item->object_id;
      $link = $item->url;
      $title = $item->title;
      $object_type = $item->object;
      $link_type = $item->type;
      $post_status = $item->post_status;
      $post_parent = $item->post_parent;
      $menu_item_parrent = $item->menu_item_parent;
      $active = ( $id == $current_id ) ? true : false;
      $target = ( 'custom' == $link_type ) ? '_blank' : '_self';
      $rel = ( 'custom' == $link_type ) ? 'noopener' : '';

      $block_classes = $post_parent ? $block_name . '__item child' : $block_name . '__item';
      $style = ( $menu_name === 'Mobile Menu' && $post_parent ) ? 'mobile-menu-child' : '';

      $link_html = $this->render_link([
        'active' => $active,
        'classes' => $block_name . '__link link',
        'target' => $target,
        'rel' => $rel,
        'style' => $style,
        'title' => $title,
        'link' => $link,
      ]);

      if ( 'publish' === $post_status ) {
        $html .= "
          <div class='{$block_classes}' data-id='{$id}' data-post-parent-id='{$post_parent}' data-menu-item-parent-id='{$menu_item_parrent}'>
            {$link_html}
          </div>
        ";
      }

    }

    return $html;

  }

  // ---------------------------------------- WP Navigation Items
  public function render_wp_navigation_items( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        "block_name" => "",
        "current_id" => 0,
        "html" => "",
        "menu_items" => [],
      ],
      $params
    ));

    if ( !empty($menu_items) ) {
      foreach ( $menu_items as $item ) {

        $id = $item->object_id;
        $link = $item->url;
        $title = $item->title;
        $object_type = $item->object;
        $link_type = $item->type;
        $post_status = $item->post_status;
        $post_parent = $item->post_parent;
        $menu_item_parrent = $item->menu_item_parent;
        $active = ( $id == $current_id ) ? true : false;
        $target = ( 'custom' == $link_type ) ? '_blank' : '_self';
        $rel = ( 'custom' == $link_type ) ? 'noopener' : '';

        // ---------------------------------------- Conditionals
        $link_classes = "{$block_name}__navigation-link link";
        $link_style = $post_parent ? "{$block_name}-child" : "";

        $link_html = $this->render_link([
          'active' => $active,
          'classes' => $link_classes,
          'target' => $target,
          'rel' => $rel,
          'style' => $link_style,
          'title' => $title,
          'link' => $link,
        ]);

      if ( 'publish' === $post_status ) {
        $html .= "
          <div class='{$block_name}__navigation-item" . ( $post_parent ? " child" : "" ) . "' data-id='{$id}' data-post-parent-id='{$post_parent}' data-menu-item-parent-id='{$menu_item_parrent}'>
            {$link_html}
          </div>
        ";
      }

    }
    }

    return $html;

  }

}
