<?php

$tools = new Tools();
add_action( 'init', [ $tools, 'init' ] );

class Tools {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Tools';
  private $version = '1.0';

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Instance
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Register Menus
  public function register_menus(){
  	register_nav_menus(
		  array(
			  'main-menu' => __( 'Main Menu' ),
			  'push-menu--left' => __( 'Push Menu [ Left ]' ),
			  'push-menu--right' => __( 'Push Menu [ Right ]' )
		  )
	  );
  }

  // ---------------------------------------- Is Localhost
  public function is_localhost() {
  	$whitelist = [ '127.0.0.1', '::1' ];
  	if ( in_array( $_SERVER['REMOTE_ADDR'], $whitelist ) ) {
  		return true;
  	}
  }

  // ---------------------------------------- Initialize
  public function init() {

    $this->register_menus();

    if ( $this->is_localhost() ) {
      add_filter( 'http_request_args', function ( $r ) {
      	$r['sslverify'] = false;
      	return $r;
      });
    }

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Static
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Debug This
  public static function debug_this( $object = "" ) {
    $object_type = gettype($object);
    echo "
      <div class='php-debugger' style='background: black; width: 90%; display: block; margin: 65px auto; padding: 40px 25px; position: relative; z-index: 10;'>
        <pre style='color: #fff;'>
          <h3 style='margin-bottom: 20px;'>Type: {$object_type}</h3>
          <h3 style='margin-bottom: 20px;'>Contents of {$object_type}:</h3>
          " . print_r( $object, true ) . "
        </pre>
      </div>
    ";
  }

  // ---------------------------------------- Handleize
  public static function handleize( $string = "" ) {

  	$string = strip_tags( $string );
    $string = strtolower( $string );
    $string = preg_replace( "/[^a-z0-9_\s-]/", "", $string );
    $string = preg_replace( "/[\s-]+/", " ", $string );
    $string = preg_replace( "/[\s_]/", "-", $string );
    return $string;

  }

  // ---------------------------------------- Is Child
  public static function is_child() {

    global $post;

	  if ( is_page() && $post->post_parent ) {
		  return true;
	  }

    return false;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
