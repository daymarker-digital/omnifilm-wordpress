<?php

/*
//////////////////////////////////////////////////////////
////  Add Style Select Buttons
//////////////////////////////////////////////////////////
*/

function add_style_select_buttons ( $buttons ) {

  array_unshift( $buttons, 'styleselect' );
  return $buttons;

}

// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

/*
//////////////////////////////////////////////////////////
////  Add Custom Styles
//////////////////////////////////////////////////////////
*/

function my_custom_styles ( $init_array ) {

    $style_formats = [
      [
        'title' => 'Footnote',
        'classes' => 'footnote',
        'wrapper' => true,
        'inline' => 'span'
      ],
      [
        'title' => 'Drop Cap',
        'classes' => 'drop-cap',
        'wrapper' => true,
        'inline' => 'span'
      ],
      [
          'title' => 'Intro Para',
          'classes' => 'intro-para',
          'wrapper' => true,
          'inline' => 'span'
      ]
    ];

    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );
