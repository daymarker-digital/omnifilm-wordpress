<?php

if ( defined( 'WPSEO_VERSION' ) ) {

  function custom_meta_author( $author_name, $presentation ) {

    if ( is_singular("news") ) {

      $id = get_queried_object_id() ?: 0;
      $author_name = get_field( "article_author", $id ) ?: "";

      if ( !$article_author ) {
        $author_id = get_post_field("post_author", $id);
        $author_name = get_the_author_meta( "display_name" , $author_id ) ?: get_bloginfo("name");
      }

    }

    return $author_name;

  }

  function custom_meta_description( $description ) {
    return $description;
  }

  add_filter( "wpseo_meta_author", "custom_meta_author", 10, 2 );
  add_filter( "wpseo_metadesc", "custom_meta_description", 10, 1 );

}
