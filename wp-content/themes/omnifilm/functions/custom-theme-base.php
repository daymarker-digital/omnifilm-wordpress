<?php

class CustomThemeBase {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Custom Theme Base';
  private $version = '2.0';

  public $custom_image_title = 'custom-image-size';
  public $custom_image_sizes = [ 1, 10, 90, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];

  // --------------------------- Featured Image
  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = [];
    $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  // --------------------------- Google Maps Directions Link
  public function get_google_maps_directions_link( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'endpoint_base' => 'https://www.google.com/maps/search/?api=1&query=',
        'full_address' => '',
        'html' => '',
        'link' => '',
      ],
      $params
    ));

    if ( $full_address ) {
      $link = trim($full_address);
    }

    return $link ? $endpoint_base . rawurlencode($link) : '';

  }

  // --------------------------- Theme Classes
  public function get_theme_classes() {

    global $post;
    global $template;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$template = basename( $template, '.php' );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--' . $template;
  		$classes .= ' ' . $template;

      if ( is_front_page() ) {
        $classes .= ' is-front-page';
      }

      if ( is_page() ) {
        $classes .= ' is-page';
      }

      if ( is_page_template( 'page-templates/page--about-us.php') ) {
        $classes .= ' is-about-us-page';
      }

      if ( is_single() ) {
        $classes .= ' is-single';
      }

      if ( is_archive() ) {
        $classes .= ' is-archive';
      }

      if ( is_category() ) {
        $classes .= ' is-category';
      }

  	}

    if ( is_404() ) {
      $classes .= ' is-404';
    }

  	return trim($classes);

  }

  // --------------------------- Theme Directory
  public function get_theme_directory( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  // --------------------------- Theme Info
  public function get_theme_info( $param = 'version' ) {

    global $post;

    switch ( $param ) {
      case 'object_ID':
				$html = get_queried_object_id();
				break;
      case 'php_version':
				$html = phpversion();
				break;
      case 'post_ID':
				$html = ( $post ) ? $post->ID : 'no-post-id';
				break;
			case 'post_type':
				$html = get_post_type( $post->ID );
				break;
      case 'site-name':
				$html = get_bloginfo( 'name' );
      case 'template':
				$html = basename( get_page_template(), ".php" );
				break;
      case 'version':
				$html = $this->version;
				break;
      case 'vitals':
        $html = "<!-- PHP Version: " . $this->get_theme_info( 'php_version' ) . " -->";
  	    $html .= "<!-- WP Version: " . $this->get_theme_info( 'wp_version' ) . " -->";
  	    $html .= "<!-- Current Template: " . $this->get_theme_info( 'template' ) . " -->";
  	    $html .= "<!-- Post ID: " . $this->get_theme_info( 'post_ID' ) . " -->";
        $html .= "<!-- Object ID: " . $this->get_theme_info( 'object_ID' ) . " -->";
				break;
      case 'wp_version':
				$html = get_bloginfo( 'version' );
				break;
      default:
        $html = '';
        break;
    }

    return $html;

  }

  // --------------------------- Get Unique ID
  public function get_unique_id( $prefix = "id--" ) {
    return trim( $prefix . md5(uniqid(rand(), true)) );
  }

  // --------------------------- AOS Attributes
  public function render_aos_attrs( $params = [] ) {

    /*
    *  Note:
    *  AOS library (https://www.npmjs.com/package/aos) needs to be installed
    *  and initialized. JS and CSS files are required for anything to happen.
    *
    *  Timing:
    *  Both delay and duration must be increments of 50
    *
    *  Bugs:
    *  Offset and Mirror are buggy. Disable them for now.
    */

    // ---------------------------------------- Defaults
    $settings = array_merge([
        'anchor' => '',                           // element id
        'anchor_placement' => 'top-bottom',
        'delay' => 0,
        'duration' => 650,
        'easing' => 'ease-in-out',
        // 'mirror' => 'false',                   // DEF buggy, hide for now
        'offset' => 175,                          // buggy, so hide for now (...or?)
        'once' => 'true',
        'transition' => 'fade-in',
      ], $params
    );

    $html = '';

    foreach ( $settings as $key => $value ) {
      switch ( $key ) {
        case 'anchor': {
          $html .= ' data-aos-anchor="#' . $value . '"';
          break;
        }
        case 'anchor_placement': {
          $html .= ' data-aos-anchor-placement="' . $value . '"';
          break;
        }
        case 'transition': {
          $html .= ' data-aos="' . $value . '"';
          break;
        }
        default:
          $html .= ' data-aos-' . $key . '="' . $value . '"';
      }
    }

    return $html;

  }

  // --------------------------- Bootstrap Accordion
  public function render_bs_accordion( $params = [] ) {

    extract(array_merge(
      [
        'block_name' => 'accordion',
        'content' => '',
        'id' => '',
        'open' => false,
        'title' => '',
      ],
      $params
    ));

    $aria_state = $open ? "true" : "false";
    $button_classes = "{$block_name}__trigger button button--accordion" . ( $open ? "" : " collapsed" );
    $main_classes = "{$block_name}__main collapse" . ( $open ? " show" : "" );

    if ( $content && $id && $title ) {
      return "
        <button class='{$button_classes}' type='button' data-bs-toggle='collapse' data-bs-target='#{$id}' aria-expanded='{$aria_state}' aria-controls='{$id}'>
          <span class='{$block_name}__trigger-title'>{$title}</span>
          <span class='{$block_name}__trigger-icon'></span>
        </button>
        <div class='{$main_classes}' id='{$id}'>
          <div class='{$block_name}__main-content'>{$content}</div>

        </div>
      ";
    }

    return;

  }

  // --------------------------- Container
  public function render_bs_container( $state = 'open', $col = 'col-12', $container = 'container' ) {
    if ( "full-width" !== $container ) {
      return "open" !== $state ? "</div></div></div>" : "<div class='{$container}'><div class='row'><div class='{$col}'>";
    }
    return;
  }

  // ---------------------------------------- Element Styles
  public function render_element_styles( $params = [] ) {

    extract(array_merge(
      [
        'background_colour' => 'black',
        'id' => '',
        'padding_bottom' => 0,
        'padding_top' => 0,
        'text_colour' => 'white',
      ],
      $params
    ));

    if ( $id ) {
      return trim("
        #{$id} {
          color: var(--theme-colour--{$text_colour});
          " . ( "none" === $background_colour ? "" : "background: var(--theme-colour--{$background_colour});" ) . "
          padding-top: calc({$padding_top}px * 0.75);
          padding-bottom: calc({$padding_bottom}px  * 0.75);
        }
        @media screen and (min-width: 992px) {
          #{$id} {
            padding-top: {$padding_top}px;
            padding-bottom: {$padding_bottom}px;
          }
        }
      ");
    }

  }

  // ---------------------------------------- Form Input
  public function render_form_input( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accept' => '',
        'classes' => 'form__input input',
        'disabled' => false,
        'honeypot' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'multiple' => false,
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= ' input--' . $type;
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<input
        class="' . $classes . '"
        ' . ( $accept ? 'accept="' . $accept . '"' : '' ) . '
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $honeypot ? 'tabindex="-1"' : '' ) . '
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $multiple ? 'multiple' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $type === 'email' ? 'spellcheck="false" autocapitalize="off"' : '' ) . '
        ' . ( $type === 'tel' ? 'pattern="[0-9\-]*"' : '' ) . '
        type="' . $type . '"
        name="' . $name . '"
        autocomplete="' . $type . '"
      >';

    }

    return $html;

  }

  // ---------------------------------------- Form Label
  public function render_form_label( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__label label',
        'for' => '',
        'hidden' => false,
        'html' => '',
        'modifier' => '',
        'required' => false,
        'value' => '',
      ],
      $params
    ));

    $classes .= $modifier ? $classes . ' label--' . $modifier : '';
    $classes .= $hidden ? ' d-none' : '';
    $value .= $required ? '<span class="required-marker">*</span>' : '';

    if ( $value ) {
      $html .= '<label
        class="' . $classes . '"
        ' . ( $for ? 'for="' . $for . '"' : '' ) . '
      >' . $value . '</label>';
    }

    return $html;

  }

  // ---------------------------------------- Form Select
  public function render_form_select( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__select select',
        'default' => '',
        'disabled' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'modifier' => '',
        'multiple' => false,
        'name' => '',
        'readonly' => false,
        'required' => false,
        'selected' => '',
        'options' => [],
      ],
      $params
    ));

    if ( $name && $options ) {

      $classes .= $modifier ? $classes . ' select--' . $modifier : '';
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<select
        class="' . $classes . '"
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $multiple ? 'multiple' : '' ) . '
        name="' . $name . '"
      >';
      $html .= $default ? '<option value="">' . $default . '</option>' : '';
      foreach ( $options as $index => $option ) {
        $title = $option['title'] ?? '';
        $value = $option['value'] ?? '';
        if ( $title && $value ) {
          $html .= '<option value="' . $value . '" ' . ( $selected === $value ? 'selected' : '' ) . '>' . $title . '</option>';
        }
      }
      $html .= '</select>';

    }

    return $html;

  }

  // ---------------------------------------- Form Textarea
  public function render_form_textarea( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => 'form__textarea textarea',
        'disabled' => false,
        'hidden' => false,
        'html' => '',
        'id' => '',
        'modifier' => '',
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= $modifier ? $classes . ' textarea--' . $modifier : '';
      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html .= '<textarea
        ' . ( $classes ? 'class="' . $classes . '"' : '' ) . '
        ' . ( $id ? 'id="' . $id . '"' : '' ) . '
        ' . ( $disabled ? 'disabled' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        name="' . $name . '"
      ></textarea>';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Embed
  public function render_lazyload_embed( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'embed',
        'classes' => '',
        'controls' => false,
        'delay' => 0,
        'duration' => 500,
        'html' => '',
        'id' => '', // 160730254, 163590531, 221632885
        'preload' => true,
        'source' => 'vimeo',
      ],
      $params
    ));

    $block_classes = "{$block_name} lazyload lazyload-item lazyload-item--{$block_name}";
    $block_classes .= $preload ? " lazypreload" : "";
    $block_classes = $classes ? "{$classes} {$block_classes}" : $block_classes;

    switch ( $source ) {
      case 'vimeo': {
        $embed_source = 'https://player.vimeo.com/video/' . $id;
        $embed_source .= $controls ? '?controls=1&keyboard=1&background=0&loop=0' : '?controls=0&keyboard=0&background=1&loop=1';
        break;
      }
      case 'youtube': {
        $embed_source = 'https://www.youtube.com/embed/' . $id;
        $embed_source .= $controls ? '?controls=1&disablekb=0&loop=0' : '?controls=0&disablekb=1&loop=1';
        break;
      }
      default: {
        $embed_source = '';
        break;
      }
    }

    if ( $embed_source && $id ) {
      $html = "
        <iframe
          class='{$block_classes}'
          data-src='{$embed_source}'
          data-transition-delay='{$delay}'
          data-transition-duration='{$duration}'
          frameborder='0'
          allow='autoplay; encrypted-media'
          src=''
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ";
    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_nu_lazyload_image( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'alt_text' => '',
        'classes' => '',
        'custom_sizes_title' => $this->custom_image_title,
        'custom_sizes' => $this->custom_image_sizes,
        'delay' => 0,
        'duration' => 300,
        'html' => '',
        'image' => [],
      ],
      $params
    ));

    $base_classes = 'lazyload lazyload-item lazyload-item--image lazypreload';
    $classes = ( $classes ?? '' ) . ' ' . $base_classes;
    $img_attrs = '';
    $img_srcset = '';

    if ( !empty($image) ) {

      $img_type = $image['subtype'] ?? 'no-set';
      $img_src = $image['url'] ?? '';

      $img_attrs .= 'class="' . trim($classes) . '"';
      $img_attrs .= $image['width'] ? ' width="' . $image['width'] . '"' : '';
      $img_attrs .= $image['height'] ? ' height="' . $image['height'] . '"' : '';
      $img_attrs .= ' alt="' . ( $alt_text ?: ( $image['alt'] ?? get_bloginfo('name') . ' Photography' ) ) . '"';
      $img_attrs .= $img_src ? ' data-src="' . $img_src . '"' : '';
      $img_attrs .= ' data-transition-delay="' . $delay . '"';
      $img_attrs .= ' data-transition-duration="' . $duration . '"';

      switch ( $img_type ) {
        case 'svg+xml': {
          $img_attrs .= $img_src ? ' src="' . $img_src . '"' : '';
          break;
        }
        default: {
          if ( isset($image['sizes']) && !empty($image['sizes']) ) {
            foreach ( $custom_sizes as $i => $size ) {
              $img_srcset .= ( $i > 0 ) ? ', ' : '';
              $img_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
            }
            $img_attrs .= $img_srcset ? ' data-sizes="auto" data-srcset="' . $img_srcset . '"' : '';
          }
          break;
        }

      }

      $html = '<img ' . $img_attrs . ' />';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Video File
  public function render_lazyload_video_file( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'autoplay' => false,
        'classes' => '',
        'controls' => false,
        'delay' => 0,
        'duration' => 750,
        'html' => '',
        'loop' => false,
        'muted' => false,
        'video_file' => [],
      ],
      $params
    ));

    $video_src = $video_file["url"] ?? "";
    $video_mime_type = $video_file["mime_type"] ?? "";

    if ( $video_mime_type && $video_src ) {
      $html = "
        <video
          class='lazyload lazyload-item lazyload-item--video-file lazypreload'
          " . ( $autoplay ? "autoplay" : "" ) . "
          " . ( $controls ? "controls" : "" ) . "
          " . ( $loop ? "loop" : "" ) . "
          " . ( $muted ? "muted" : "" ) . "
          preload=''
          data-transition-delay='{$delay}'
          data-transition-duration='{$duration}'
        >
          <source src='{$video_src}' type='{$video_mime_type}'>
        </video>
      ";
    }

    return $html;

  }

  // --------------------------- Pre-connect Scripts
  public function render_preconnect_resources( $resources = [] ) {
    $html = '';
    $relationships = [ 'preconnect', 'dns-prefetch' ];
    if ( !empty( $resources ) && !empty( $relationships ) ) {
      foreach ( $relationships as $rel ) {
        foreach ( $resources as $resource ) {
          $html .= "<link rel='{$rel}' href='{$resource}'>";
        }
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_preload_fonts( $fonts = [] ) {
    $html = '';
    if ( !empty( $fonts ) ) {
      foreach ( $fonts as $font ) {
        $font_src = "{$this->get_theme_directory('assets')}/fonts/{$font}.woff2";
        $html .= "<link rel='preload' href='{$font_src}' as='font' type='font/woff2' crossorigin>";
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_seo( $enable = true ) {
    $html = '<meta name="robots" content="noindex, nofollow">';
    if ( $enable && !is_attachment( $this->get_theme_info('post_ID') ) ) {
      if ( defined( 'WPSEO_VERSION' ) ) {
        $html = '<!-- Yoast Plugin IS ACTIVE! -->';
      } else {
        $html = '<!-- Yoast Plugin IS NOT ACTIVE! -->';
        $html .= '<meta name="description" content="' . get_bloginfo( 'description' ) . '">';
      }
    }
    return $html;
  }

  // --------------------------- Theme Vitals
  public function render_theme_vitals() {

    $html = '';
    $html .= "<!-- PHP Version: " . $this->get_theme_info('php_version') . " -->";
    $html .= "<!-- WP Version: " . $this->get_theme_info('wp_version') . " -->";
    $html .= "<!-- Current Template: " . $this->get_theme_info('template') . " -->";
    $html .= "<!-- Post ID: " . $this->get_theme_info('post_ID') . " -->";
    $html .= "<!-- Object ID: " . $this->get_theme_info('object_ID') . " -->";

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
