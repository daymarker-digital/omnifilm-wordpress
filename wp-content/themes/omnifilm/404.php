<?php

  /*
  *
  *	Template Name: 404
  *	Filename: 404.php
  *
  */

  // ---------------------------------------- Load Header Template
  get_header();

  // ---------------------------------------- Vars
  $THEME = $THEME ?? new CustomTheme();

  $block_name = 'error-404';
  $error_404 = get_field( 'error_404', 'options' ) ?: [];
  $heading = $error_404["heading"] ?? "404";
  $message = $error_404["message"] ?? "Oops! The content you're looking for does not exist.";
  $cta = $error_404["cta"] ?? [];

?>

<div class="<?= $block_name ?>">
  <div class="<?= $block_name ?>__main">
    <?= $THEME->render_bs_container( 'open', 'col-12' ); ?>
      <div class="<?= $block_name ?>__content">
        <?php if ( $heading ) : ?>
          <h1 class="<?= $block_name; ?>__heading heading--primary heading--page-title"><?= $heading; ?></h1>
        <?php endif; ?>
        <?php if ( $message ) : ?>
          <div class="<?= $block_name; ?>__message body-copy--primary body-copy--md"><?= $message; ?></div>
        <?php endif; ?>
        <?php if ( !empty($cta) ) : ?>
          <div class="<?= $block_name; ?>__cta cta">
            <?= $THEME->render_cta($cta); ?>
          </div>
        <?php endif; ?>
      </div>
    <?= $THEME->render_bs_container( 'closed' ); ?>
  </div>
</div>

<?php get_footer(); ?>
