switch ( $section_layout ) {
            case 'hero': {
              echo $THEME->render_flexible_content_hero([ 'index' => $index, 'section' => $section ]);
              break;
            }
        case 'navigation': {
          $menu_name = $section['menu_name'] ?? '';
          $title = get_the_title( $id );
          echo "<h1 class='flexible-content__title title title--page-title d-lg-none'>{$title}</h1>";
          echo $THEME->render_wp_navigation([
                  'classes' => 'flexible-content__navigation',
                  'current_id' => $id,
                  'menu_name' => $menu_name
                ]);
          break;
        }
            case 'news-listing': {
              get_template_part( 'snippets/flexible-content/news-listing', null, $section );
              break;
            }
        case 'partners': {
          // $partners = get_field( 'partners', 'options' ) ?: [];
          // $title = $section['title'] ?? $partners['title'] ?? '';
          // $broadcasters = $partners['broadcasters'] ?? $broadcasters;
          // echo $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          // echo $THEME->render_partners([ 'partners' => $broadcasters, 'title' => $title ]);
          // echo $THEME->render_bs_container( 'closed' );
          get_template_part( 'snippets/flexible-content/partners', null, $section );
          break;
        }
            case 'production-listing': {
              get_template_part( 'snippets/flexible-content/productions-listing', null, $section );
              //echo $THEME->render_productions_listing([ 'section' => $section ]);
              break;
            }
        case 'screener-request': {
          get_template_part( 'snippets/flexible-content/screener-request', null, $section );
          //echo $THEME->render_screener_request();
          break;
        }
            case 'team-members': {
              get_template_part( 'snippets/flexible-content/team-members', null, $section );
              // echo $THEME->render_bs_container( 'open', 'col-12' );
              // echo $THEME->render_flexible_content_team_members([ 'section' => $section ]);
              // echo $THEME->render_bs_container( 'closed' );
              break;
            }
        case 'text-grid': {
          get_template_part( 'snippets/flexible-content/team-grid', null, $section );
          // echo $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          // echo $THEME->render_flexible_content_text_grid([ 'section' => $section ]);
          // echo $THEME->render_bs_container( 'closed' );
          break;
        }
        case 'video': {
          get_template_part( 'snippets/flexible-content/video', null, $section );
          // echo $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          // echo $THEME->ender_flexible_content_video([ 'section' => $section ]);
          // echo $THEME->render_bs_container( 'closed' );
          break;
        }
        case 'wysiwyg': {
          get_template_part( 'snippets/flexible-content/wysiwyg', null, $section );
          // echo $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          // echo $THEME->render_flexible_content_wysiwyg([ 'section' => $section ]);
          // echo $THEME->render_bs_container( 'closed' );
          break;
        }
      }
    }
