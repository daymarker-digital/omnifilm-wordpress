<?php

  /*
  *
  *	Template Name: Index
  *	Filename: index.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Mount Flexible Content
  get_template_part( 'snippets/flexible-content/main' );

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
