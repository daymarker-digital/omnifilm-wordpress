# Omnifilm WordPress

## Site URL
https://wp-omni/
https://www.omnifilm.com/
https://omnifilmdev.wpengine.com/

## Run Local Development
MAMP + local browser + Codekit

## Todo

### Misc
- [x] Update form validation JS module for ES6
- [x] Fix thank you page
- [x] Fix Watch Real in Header
- [x] Add icons to the lightgallery feature (close, download, etc)
- [x] Update mobile menu/drawer
- [ ] Add article content (From OMNI team)
- [x] Add video block to block system
- [ ] Fix submit button on form layout

### Templates

### Sections

### General

## Preflight
