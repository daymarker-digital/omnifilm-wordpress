<?php

/*
*
*	Filename: archive.php
*
*/

get_header();

$THEME = $THEME ?? new CustomTheme();

?>

<?php if ( have_posts() ) : ?>

  <?php
    $queried_object = get_queried_object();
    $queried_object_label = $queried_object->label ?? '';
  ?>

  <section class="archive__main">
    <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', 'container' ); ?>
      <?php if ( $queried_object_label ) : ?>
        <h1 class="archive__heading heading--primary heading--page-title"><?= $queried_object_label; ?></h1>
      <?php endif; ?>
      <div class="archive__listing grid grid--1 grid--sm-2 grid--lg-4">
        <?php while ( have_posts() ) : the_post(); ?>
          <?= $THEME->render_card_news_by_id(get_the_ID()); ?>
        <?php endwhile; ?>
      </div>
    <?= $THEME->render_bs_container( 'closed' ); ?>
  </section>

<?php endif; ?>

<?php get_footer(); ?>
