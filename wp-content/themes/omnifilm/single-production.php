<?php

  /*
  *
  *	Filename: single-production.php
  *
  */

  get_header();

  // ---------------------------------------- Vars
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  $block_name = "production";
  $block_sections = [
    [ "name" => "about", "enable" => get_field("about", $id) ],
    [ "name" => "photos", "enable" => have_rows("photos", $id) ],
    [ "name" => "cast", "enable" => have_rows("cast", $id) ],
    [ "name" => "awards", "enable" => have_rows("awards", $id) ],
    [ "name" => "news", "enable" => have_rows("news", $id) ],
  ];

  $title = get_the_title($id) ?: '';
  $featured_image = $THEME->get_featured_image_by_post_id($id);
  $featured_image_lazy = $THEME->render_nu_lazyload_image([ "image" => $featured_image ]);
  $svg_icon_plus = $THEME->render_svg_icon("plus");
  $tabbed_ids = [];
  $video_file = get_field("video_file", $id) ?: [];
  $video_placeholder_image = get_field("video_placeholder_image", $id) ?: $featured_image;
  $video_type = get_field("video_type", $id) ?: "";
  $video_vimeo_id = get_field("video_vimeo_id", $id);

  // ---------------------------------------- Conditionals
  $has_reel = ( "vimeo" == $video_type && $video_vimeo_id ) || ( "video-file" == $video_type && $video_file["url"] ?? "" ) ? true : false;

?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>

    <div class="<?= $block_name ?>">
      <?= $THEME->render_bs_container( "open", "col-12 col-xl-10 offset-xl-1" ); ?>

        <h1 class="<?= $block_name ?>__title heading--primary"><?= $title; ?></h1>

        <?php if ( $has_reel ) : ?>
          <div class="<?= $block_name ?>__reel">
            <?php
              get_template_part( "snippets/flexible-content/video", null, [
                "container" => "full-width",
                "enable" => true,
                "video_file" => $video_file,
                "video_placeholder_image" => $video_placeholder_image,
                "video_type" => get_field("video_type", $id),
                "video_vimeo_id" => $video_vimeo_id,
                "video_ratio_height" => get_field("video_ratio_height", $id),
                "video_ratio_width" => get_field("video_ratio_width", $id),
              ]);
            ?>
          </div>
        <?php else : ?>
          <div class="<?= $block_name ?>__feature-image"><?= $featured_image_lazy; ?></div>
        <?php endif; ?>

        <?php if ( !empty($block_sections) ) : ?>

          <div class="<?= $block_name ?>__main collapsible d-block d-lg-none">
            <?php foreach ( $block_sections as $index => $section ) : ?>

              <?php
                $section_enable = $section['enable'] ?: false;
                $section_name = $section['name'] ?: "";
                $collapse_id = $THEME->get_unique_id("{$section_name}-collapse-");
                $collapse_button_state = 0 === $index ? '' : ' collapsed';
                $collapse_content_state = 0 === $index ? ' show' : '';
              ?>

              <?php if ( $section_enable ) : ?>
                <div class="collapsible__item" data-index="<?= $index; ?>">
                  <button
                    class="collapsible__trigger button<?= $collapse_button_state; ?>"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#<?= $collapse_id; ?>"
                    aria-expanded="false"
                    aria-controls="<?= $collapse_id; ?>"
                  >
                    <span class="button__title"><?= ucfirst($section_name); ?></span>
                    <span class="button__icon"><?= $svg_icon_plus; ?></span>
                  </button>

                  <div class="collapsible__content collapse <?= $collapse_content_state; ?>" id="<?= $collapse_id; ?>">
                    <div class="collapsible__content-padding">
                      <?php
                        switch( $section_name ) {
                          case 'about': {
                            echo $THEME->render_production_about_by_id($id);
                            break;
                          }
                          case 'awards': {
                            echo $THEME->render_production_awards_by_id($id);
                            break;
                          }
                          case 'cast': {
                            echo $THEME->render_production_cast_by_id($id);
                            break;
                          }
                          case 'news': {
                            echo $THEME->render_production_news_by_id($id);
                            break;
                          }
                          case 'photos': {
                            echo $THEME->render_production_photos_by_id($id);
                            break;
                          }
                        }
                      ?>
                    </div>
                  </div>

                </div>
              <?php endif; ?>

            <?php endforeach; ?>
          </div>

          <div class="<?= $block_name ?>__main tabbed d-none d-lg-block">
            <div class="tabbed__nav nav nav-tabs" id="tabbed__nav" role="tablist">
              <?php foreach ( $block_sections as $index => $section ) : ?>

                <?php
                  $active_state = 0 === $index ? ' active' : '';
                  $section_enable = $section['enable'] ?: false;
                  $section_name = $section['name'] ?: "";
                  $tabbed_ids[$index] = $THEME->get_unique_id("{$section_name}--");
                ?>

                <?php if ( $section_enable ) : ?>
                  <button
                    class="tabbed__nav-button button button--tab nav-link<?= $active_state; ?>"
                    id="<?= $tabbed_ids[$index]; ?>-tab"
                    data-bs-toggle="tab"
                    data-bs-target="#<?= $tabbed_ids[$index]; ?>"
                    type="button"
                    role="tab"
                    aria-controls="<?= $tabbed_ids[$index]; ?>"
                    aria-selected="true"
                  ><?= ucfirst($section_name); ?></button>
                <?php endif; ?>

              <?php endforeach; ?>
            </div>
            <div class="tabbed__content tab-content" id="<?= $block_name ?>__tabbed-content">
              <?php foreach ( $block_sections as $index => $section ) : ?>

                <?php
                  $active_state = 0 === $index ? ' show active' : '';
                  $section_enable = $section['enable'] ?: false;
                  $section_name = $section['name'] ?: "";
                ?>

                <?php if ( $section_enable ) : ?>
                  <div
                    class="tabbed__content-pane tab-pane fade<?= $active_state; ?>"
                    id="<?= $tabbed_ids[$index]; ?>"
                    role="tabpanel"
                    aria-labelledby="<?= $tabbed_ids[$index]; ?>-tab"
                  >
                    <div class="tabbed__content-padding">
                      <?php
                        switch( $section_name ) {
                          case 'about': {
                            echo $THEME->render_production_about_by_id($id);
                            break;
                          }
                          case 'awards': {
                            echo $THEME->render_production_awards_by_id($id);
                            break;
                          }
                          case 'cast': {
                            echo $THEME->render_production_cast_by_id($id);
                            break;
                          }
                          case 'news': {
                            echo $THEME->render_production_news_by_id($id);
                            break;
                          }
                          case 'photos': {
                            echo $THEME->render_production_photos_by_id($id);
                            break;
                          }
                        }
                      ?>
                    </div>
                  </div>
                <?php endif; ?>

              <?php endforeach; ?>
            </div>
          </div>

        <?php endif; ?>

      <?= $THEME->render_bs_container( "closed" ); ?>
    </div>

    <?php get_template_part( "snippets/flexible-content/screener-request" ); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
