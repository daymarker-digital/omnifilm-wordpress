<?php

  /**
  *
  *   Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "text";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;
  $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]);

  // ---------------------------------------- Content (ACF)
  $cols = get_field('cols') ?: 'col-12 col-xl-10 offset-xl-1';
  $container = get_field('container') ?: 'container';
  $enable = get_field('enable') ?: false;
  $padding_bottom = get_field('padding_bottom') ?: 0;
  $padding_top = get_field('padding_top') ?: 0;
  $text_content = get_field('text_content') ?: '';

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => $padding_bottom,
        'padding_top' => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <?php if ( $text_content ) : ?>
          <div class="<?= $block_name; ?>__message body-copy--primary body-copy--md" <?= $aos_attrs; ?>><?= $text_content; ?></div>
        <?php endif; ?>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
