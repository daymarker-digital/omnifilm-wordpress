<?php
  $data = $block['data'] ?? [];
  $data["cta"] = get_field('cta') ?: [];
  $data["heading"] = get_field('heading') ?: [];
  $data["articles"] = get_field('articles') ?: [];
  get_template_part( "snippets/flexible-content/news-listing", null, $data );
?>
