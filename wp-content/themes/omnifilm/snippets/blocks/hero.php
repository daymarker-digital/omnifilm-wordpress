<?php

  /**
  *
  *   Hero
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "hero";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $cols = get_field('cols') ?: 'col-12 col-xl-10 offset-xl-1';
  $container = get_field('container') ?: 'container';
  $enable = get_field('enable') ?: false;
  $padding_bottom = get_field('padding_bottom') ?: 0;
  $padding_top = get_field('padding_top') ?: 0;

  // ---------------------------------------- Content (Post)
  $article_author = get_field("article_author", $id ) ?: "";
  $title = get_the_title( $id ) ?: '';
  $featured_image = $THEME->get_featured_image_by_post_id( $id );
  $featured_image_lazy = $THEME->render_nu_lazyload_image([ 'image' => $featured_image ]);
  $post_date = get_the_date( 'd M Y', $id ) ?: '';
  $post_date_time =  get_the_date( 'c', $id ) ?: '';

  // ---------------------------------------- Conditionals
  if ( !$article_author ) {
    $author_id = get_post_field('post_author', $id);
    $article_author = get_the_author_meta( 'display_name' , $author_id ) ?: get_bloginfo("name");
  }

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => $padding_bottom,
        'padding_top' => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__content body-copy--primary">

          <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay += $aos_increment; ?>
          <time class="<?= $block_name; ?>__publish-date body-copy--md" datetime="<?= $post_date_time; ?>" itemprop="datePublished" <?= $aos_attrs; ?>><?= $post_date; ?></time>

          <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); $aos_delay += $aos_increment; ?>
          <div class="<?= $block_name; ?>__featured-image" <?= $aos_attrs; ?>><?= $featured_image_lazy; ?></div>

          <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay += $aos_increment; ?>
          <h1 class="<?= $block_name; ?>__title heading--primary heading--page-title" <?= $aos_attrs; ?>><?= $title; ?></h1>

          <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay += $aos_increment; ?>
          <strong class="<?= $block_name; ?>__author font-weight--400" <?= $aos_attrs; ?>>Author: <?= $article_author; ?></strong>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
