<?php

  /**
  *
  *   Latest Articles
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "latest-articles";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $cols = get_field('cols') ?: 'col-12';
  $container = get_field('container') ?: 'container';
  $enable = get_field('enable') ?: false;
  $padding_bottom = get_field('padding_bottom') ?: 0;
  $padding_top = get_field('padding_top') ?: 0;

  // ---------------------------------------- Content (Post)
  $categoryies = get_the_terms($id, "category") ?: [];
  $cat_primary = $categoryies[0] ?? false;
  $cat_primary_id = $cat_primary->term_id ?? 0;
  $cat_primary_name = $cat_primary->name ?? '';
  $heading = "More" . ( $cat_primary_name ? " from {$cat_primary_name}" : "" );

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        'id' => $block_id,
        'padding_bottom' => $padding_bottom,
        'padding_top' => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__content">

          <h2 class="<?= $block_name; ?>__heading heading--primary heading--page-title"><?= $heading ; ?></h2>

          <?php
            $query_args = [
              "post_type" => "news",
              "post_status" => "publish",
              "posts_per_page" => 3,
              "orderby" => "date",
              "order" => "DESC",
              "post__not_in" => [ $id ],
            ];
            if ( $cat_primary_id ) {
              $query_args["category__in"] = [ $cat_primary_id ];
            }
            $query = new WP_Query($query_args);
            if ( $query->have_posts() ) {
              echo "<div class='{$block_name}__listing grid grid--1 grid--md-3'>";
                while ( $query->have_posts() ) {
                  $query->the_post();
                  echo $THEME->render_card_news_by_id(get_the_id());
                }
              echo "</div>";
            }
            wp_reset_postdata();
          ?>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
