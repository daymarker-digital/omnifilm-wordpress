<?php

  /**
  *
  *   Image
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "image";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $cols = get_field("cols") ?: "col-12 col-xl-10 offset-xl-1";
  $container = get_field("container") ?: "container";
  $enable = get_field("enable") ?: false;
  $image_caption = get_field("image_caption") ?: "";
  $image_content = get_field("image_content") ?: [];
  $image_content_count = count($image_content);
  $image_aspect_ratio = get_field("image_aspect_ratio") ?: "";
  $image_gutter = get_field("image_gutter") ?: 0;
  $image_layout = get_field("image_layout") ?: "stacked";
  $padding_bottom = get_field("padding_bottom") ?: 0;
  $padding_top = get_field("padding_top") ?: 0;

  // ---------------------------------------- Content (ACF)
  $image_content_clases = "grid {$image_layout}--{$image_content_count}";
  $image_content_clases .= $image_aspect_ratio ? " with-aspect-ratio" : "";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>

    <?php if ( $image_aspect_ratio ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__item {
        aspect-ratio: <?= $image_aspect_ratio; ?>;
      }
    <?php endif; ?>

    <?php if ( $image_gutter ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__content {
        gap: <?= $image_gutter; ?>px;
      }
    <?php endif; ?>

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
        <?php if ( !empty($image_content) ) : ?>
          <div class="<?= $block_name; ?>__content <?= $image_content_clases; ?>">
            <?php foreach ( $image_content as $i => $item ) : ?>

              <?php
                $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "transition" => "fade-up" ]);
                $aos_delay += $aos_increment;
                $image = $item["image"] ?? [];
                $image_html = $THEME->render_nu_lazyload_image(["image" => $image ]);
              ?>

              <?php if ( $image_html ) : ?>
                <div class="<?= $block_name; ?>__item" <?= $aos_attrs; ?>><?= $image_html; ?></div>
              <?php endif; ?>

            <?php endforeach; ?>
          </div>
          <?php if ( $image_caption ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "transition" => "fade-left" ]); ?>
            <div class="<?= $block_name; ?>__caption body-copy--primary body-copy--caption" <?= $aos_attrs; ?>><?= $image_caption; ?></div>
          <?php endif; ?>
        <?php endif; ?>
      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
