<?php

  /**
  *
  *   Productions Listing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "productions-listing";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $categories = $data["categories"] ?? [];
  $cols = $data["cols"] ?? "col-12";
  $container = $data["container"] ?? "container";
  $cta = $data["cta"] ?? [];
  $enable = $data["enable"] ?? false;
  $layout = $data["layout"] ?? "";
  $limit = $data["limit"] ?? 50;
  $order = $data["order"] ?? "DESC";
  $order_by = $data["order_by"] ?? "date";
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>

        <div class="<?= $block_name; ?>__layout" data-layout="<?= $layout; ?>">
          <?php
            $query = new WP_Query([
              "post_type" => "production",
              "post_status" => "publish",
              "posts_per_page" => $limit,
              "orderby" => $order_by,
              "order" => $order,
              'category__in' => $categories,
            ]);
            if ( $query->have_posts() ) {
              $query_index = 0;
              while ( $query->have_posts() ) {
                $query->the_post();
                $production_id = get_the_ID();
                echo "<div class='{$block_name}__item' data-index='{$query_index}' data-layout='{$layout}' data-post-id='{$production_id}'>";
                  echo $THEME->render_production_preview([ 'id' => $production_id, 'index' => $query_index, 'layout' => $layout ]);
                echo "</div>";
                $query_index++;
              }
            }
            wp_reset_postdata();
          ?>
        </div>

        <?php if ( !empty($cta) ) : ?>
          <div class="<?= $block_name; ?>__cta cta">
            <?= $THEME->render_cta($cta); ?>
          </div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( 'closed' ); ?>
    </div>
  </section>

<?php endif; ?>
