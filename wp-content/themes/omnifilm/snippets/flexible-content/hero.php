<?php

  /**
  *
  *   Hero
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "hero";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");
  $block_index = $data["index"] ?? 0;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 750;
  $aos_increment = 250;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $background = $data["hero"]["background"] ?? "";
  $cols = $data["cols"] ?? "col-12 col-lg-10 offset-lg-1";
  $container = $data["container"] ?? "container";
  $cta = $data["hero"]["cta"] ?? [];
  $enable = $data["enable"] ?? false;
  $heading = $data["hero"]["heading"] ?? [];
  $image = $data["hero"]["image"] ?? [];
  $image_lazy = $THEME->render_nu_lazyload_image([ "image" => $image ]);
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $video_file = $data["hero"]['video_file'] ?? [];
  $video_id = $data["hero"]['video_id'] ?? '';
  $vimeo_id = $data["hero"]['vimeo_id'] ?? '';
  $video_ratio_width = (float)$data["hero"]['video_ratio_width'] ?: 0;
  $video_ratio_height = (float)$data["hero"]['video_ratio_height'] ?: 0;
  $a = $video_ratio_height/$video_ratio_width * 100;
  $b = $video_ratio_width/$video_ratio_height * 100;

  // ---------------------------------------- Conditionals
  $block_classes .= " {$background}";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
    <?php
      if ( in_array( $background, [ "embedded-video", "vimeo" ] ) ) {
        echo "
          #{$block_id} iframe {
            height: {$a}vw;
            min-width: {$b}vh;
            min-height: 100vh;
            width: 100vw;
          }
        ";
      }
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

    <?php if ( $image_lazy ) : ?>
      <div class="<?= $block_name; ?>__image"><?= $image_lazy; ?></div>
    <?php endif; ?>

    <div class="<?= $block_name; ?>__background">
      <?php
        switch ( $background ) {
          case 'vimeo': {
            echo $THEME->render_lazyload_embed([
              "controls" => false,
               "id" => $vimeo_id,
               "source" => "vimeo"
            ]);
            break;
          }
          case 'video-file': {
            echo $THEME->render_lazyload_video([
              'autoplay' => true,
              'background' => true,
              'classes' => $block_name . '__video',
              'loop' => true,
              'muted' => true,
              'video' => $video_file,
            ]);
            break;
          }
        }
      ?>
    </div>

    <?php if ( $cta || $heading ) : ?>
      <?php
        $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]);
        $aos_delay += $aos_increment;
      ?>
      <div class="<?= $block_name; ?>__main">
        <div class="<?= $block_name; ?>__content" <?= $aos_attrs; ?>>
          <?= $THEME->render_bs_container( "open", $cols ); ?>
            <?php if ( $heading ) : ?>
              <h1 class="<?= $block_name; ?>__heading"><?= $heading; ?></h1>
            <?php endif; ?>
            <?php if ( !empty($cta) ) : ?>
              <div class="<?= $block_name; ?>__cta cta">
                <?= $THEME->render_cta($cta); ?>
              </div>
            <?php endif; ?>
          <?= $THEME->render_bs_container( "closed" ); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      if ( 0 === $block_index ) {
        $partners = get_field( 'partners', 'options' ) ?: [];
        $title = $partners['title'] ?? $title;
        $broadcasters = $partners['broadcasters'] ?? $broadcasters;
        echo $THEME->render_partners([
          'partners' => $broadcasters,
          'classes' => $block_name . '__broadcasters',
          'title' => $title
        ]);
      }
    ?>

  </section>

<?php endif; ?>

