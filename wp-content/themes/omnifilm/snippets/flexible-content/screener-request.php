<?php

  /**
  *
  *   Screener CTA
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $block_name = "screener-request";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $data = get_field( "screener_request", "options" ) ?: [];
  $heading = $data["heading"] ?: "";
  $banner = $data["banner"] ?: [];
  $banner_lazy = $THEME->render_nu_lazyload_image([ "image" => $banner ]);
  $contact_name = $data["contact_name"] ?: "";
  $contact_role = $data["contact_role"] ?: "";
  $contact_phone = $data["contact_phone"] ?: "";
  $contact_email = $data["contact_email"] ?: "";
  $production_title = "production" == get_post_type($id) ? get_the_title($id) : "";

  // ---------------------------------------- Conditionals
  $contact_email_subject = $production_title ? "Screener Request for – {$production_title}" : "General Screener Request";

?>

<div class="<?= $block_name; ?>">
  <?php if ( $banner_lazy ) : ?>
    <div class="<?= $block_name; ?>__banner"><?= $banner_lazy; ?></div>
  <?php endif; ?>
  <div class="<?= $block_name; ?>__main">
    <?= $THEME->render_bs_container( "open", "col-10 offset-1 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1", "container-fluid" ); ?>
      <div class="<?= $block_name; ?>__content">
        <?php if ( $heading ) : ?>
          <h3 class="<?= $block_name; ?>__heading"><?= $heading; ?></h3>
        <?php endif; ?>
        <?php if ( $contact_email || $contact_name || $contact_phone || $contact_role ) : ?>
          <div class="<?= $block_name; ?>__contact-info">

            <?php if ( $contact_name ) : ?>
              <span class="<?= $block_name; ?>__contact-info-item name"><?= $contact_name; ?></span>
            <?php endif; ?>

            <?php if ( $contact_role ) : ?>
              <span class="<?= $block_name; ?>__contact-info-item role"><?= $contact_role; ?></span>
            <?php endif; ?>

            <?php if ( $contact_phone ) : ?>
              <span class="<?= $block_name; ?>__contact-info-item phone">
                <a class="link" href="tel:<?= $contact_phone; ?>" target="_self" title="Call for Screener"><?= $contact_phone; ?></a>
              </span>
            <?php endif; ?>

            <?php if ( $contact_email ) : ?>
              <span class="<?= $block_name; ?>__contact-info-item email">
                <a class="link" href="mailto:<?= $contact_email; ?>?subject=<?= rawurlencode($contact_email_subject); ?>" target="_self" title="Email for Screener"><?= $contact_email; ?></a>
              </span>
            <?php endif; ?>

          </div>
        <?php endif; ?>
      </div>
    <?= $THEME->render_bs_container( "closed", "col-10 offset-1 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1", "container-fluid" ); ?>
  </div>
</div>
