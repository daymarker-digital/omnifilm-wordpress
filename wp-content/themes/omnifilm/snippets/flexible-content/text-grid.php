<?php

  /**
  *
  *   Text Grid
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "text-grid";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $content = $data["content"] ?? [];
  $enable = $data["enable"] ?? false;
  $heading = $data["heading"] ?? "";
  $layout = $data["layout"] ?? "two-column";
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;

  // ---------------------------------------- Conditionals
  $layout = ( "two-column" == $layout ) ? "grid--lg-2" : "grid--lg-3";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__content">

          <?php if ( $heading ) : ?>
            <h2 class="<?= $block_name; ?>__heading heading--primary heading--lg"><?= $heading; ?></h2>
          <?php endif; ?>

          <?php if ( !empty($content) ) : ?>
            <div class="<?= $block_name; ?>__listing grid grid--1 <?= $layout; ?>">
            <?php
              foreach ( $content as $i => $item ) {
                $heading = $item["heading"] ?? "";
                $content = $item["content"] ?? "";
                if ( $content || $heading ) {
                  echo "<div class='{$block_name}__item'>";
                    if ( $heading ) {
                      echo "<strong class='{$block_name}__item-subheading heading--secondary heading--sm'>{$heading}</strong>";
                    }
                    if ( $content ) {
                      echo "<div class='{$block_name}__item-content body-copy--primary body-copy--xs'>{$content}</div>";
                    }
                  echo "</div>";
                }
              }
            ?>
            </div>
          <?php endif; ?>

        </div>
      <?= $THEME->render_bs_container( 'closed' ); ?>
    </div>
  </section>

<?php endif; ?>
