 <?php

  /**
  *
  *   Flexible Content Main
  *
  */

  $id = get_queried_object_id() ?: 0;
  $flexible_content = get_field( 'flex_content', $id ) ?: [];

  if ( !empty( $flexible_content ) ) {
    foreach ( $flexible_content as $index => $section ) {
      $section_name = $section["acf_fc_layout"] ?? "";
      $section["index"] = $index;
      get_template_part( "snippets/flexible-content/{$section_name}", null, $section );
    }
  }

?>
