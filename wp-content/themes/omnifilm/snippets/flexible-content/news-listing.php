<?php

  /**
  *
  *   News Listing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "news-listing";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cta = $data["cta"] ?? [];
  $cols = $data["cols"] ?? "col-12";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $heading_content = $data["heading"]["content"] ?? "";
  $heading_tag = $data["heading"]["tag"] ?? "h2";
  $type = $data["type"] ?? "";
  $layout = $data["layout"] ?? "";
  $articles = $data["articles"] ?? [];
  $articles_limit = $data["limit"] ?? 30;
  $articles_order = $data["order"] ?? "DESC";
  $articles_orderby = $data["orderby"] ?? "date";
  $articles_sanitized = [];

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>

        <?php if ( $heading_content ) : ?>
          <<?= $heading_tag; ?> class="<?= $block_name; ?>__heading heading--primary heading--page-title"><?= $heading_content; ?></<?= $heading_tag; ?>>
        <?php endif; ?>

        <div class="<?= $block_name; ?>__listing grid grid--1 grid--sm-2 grid--lg-4">
          <?php
            switch ( $type ) {
              case "auto": {
                $news_query = new WP_Query([
                  "post_type" => "news",
                  "post_status" => "publish",
                  "posts_per_page" => $articles_limit,
                  "orderby" => $articles_orderby,
                  "order" => $articles_order,
                ]);
                if ( $news_query->have_posts() ) {
                  while ( $news_query->have_posts() ) {
                    $news_query->the_post();
                    $articles_sanitized[] = get_the_ID();
                  }
                }
                wp_reset_postdata();
                break;
              }
              case "manual": {
                $articles_sanitized = array_map(function($item) {
                  return $item["article"]->ID ?? 0;
                }, $articles);
                break;
              }
            }
            if ( !empty($articles_sanitized) ) {
              foreach ( $articles_sanitized as $id ) {
                echo $THEME->render_card_news_by_id($id);
              }
            }
          ?>
        </div>

        <?php if ( !empty($cta) ) : ?>
          <div class="<?= $block_name; ?>__cta cta">
            <?= $THEME->render_cta($cta); ?>
          </div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
