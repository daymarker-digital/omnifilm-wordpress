<?php

  /**
  *
  *   Team Members
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "team-members";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $heading = $data["heading"] ?? "";
  $team_members = $data["team_members"] ?? [];

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>

        <?php if ( $heading ) : ?>
          <h2 class="<?= $block_name; ?>__heading heading--primary heading--page-title"><?= $heading; ?></h2>
        <?php endif; ?>

        <?php if ( !empty($team_members) ) : ?>
          <div class="<?= $block_name; ?>__listing grid grid--2 grid--md-3 grid--lg-4 grid--xl-5">
            <?php
              foreach ( $team_members as $item ) {
                $team_member_id = $item['team_member']->ID ?? 0;
                echo $THEME->render_card_team_member_by_id($team_member_id);
              }
            ?>
          </div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( 'closed' ); ?>
    </div>
  </section>

<?php endif; ?>
