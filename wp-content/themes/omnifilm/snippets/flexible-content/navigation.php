<?php

  /**
  *
  *   Navigation
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "navigation";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $page_title = get_the_title($id) ?: '';
  $menu_name = $data["menu_name"] ?? "";
  $navigation = $data["navigation"] ?? "";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <h1 class="<?= $block_classes; ?>__page-title heading--primary heading--page-title d-lg-none"><?= $page_title; ?></h1>

  <nav class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <?php
      echo $THEME->render_wp_navigation_item([
        'current_id' => $id,
        'menu_name' => $menu_name
      ]);
    ?>
  </nav>

<?php endif; ?>
