<?php

  /**
  *
  *   Form
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "form";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;
  $form_type = $data["form_type"] ?? "";
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__content">
          <?php get_template_part( "snippets/forms/{$form_type}", null, $data ); ?>
        </div>
      <?= $THEME->render_bs_container( 'closed' ); ?>
    </div>
  </section>

<?php endif; ?>
