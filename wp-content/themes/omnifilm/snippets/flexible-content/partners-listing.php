<?php

  /**
  *
  *   Partners Listing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "partners-listing";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $heading = $data["heading"] ?? "";
  $enable = $data["enable"] ?? false;
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $type = $data["type"] ?? "auto";
  $partners = $data["partners"] ?: [];
  $partners_limit = $data["limit"] ?: 50;
  $partners_order = $data["order"] ?: "DESC";
  $partners_orderby = $data["orderby"] ?: "date";
  $partners_categories = $data["categories"] ?: [];
  $partners_category_search_type = $data["category_search_type"] ?: "category__in";
  $partners_sanitized = [];

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__content">

          <?php if ( $heading ) : ?>
            <h2 class="<?= $block_name; ?>__heading heading--secondary"><?= $heading; ?></h2>
          <?php endif; ?>

          <div class="<?= $block_name; ?>__listing">
             <?php
              switch ( $type ) {
                case 'auto': {
                  $query_args = [
                    "post_type" => "partner",
                    "post_status" => "publish",
                    "posts_per_page" => $partners_limit,
                    "orderby" => $partners_orderby,
                    "order" => $partners_order,
                  ];
                  if ( !empty($partners_categories) ) {
                    $query_args[$partners_category_search_type] = $partners_categories;
                  }
                  $partner_query = new WP_Query($query_args);
                  if ( $partner_query->have_posts() ) {
                    while ( $partner_query->have_posts() ) {
                      $partner_query->the_post();
                      $partners_sanitized[] = get_the_ID();
                    }
                  }
                  wp_reset_postdata();
                  break;
                }
                case 'manual': {
                  $partners_sanitized = array_map(function($item) {
                    return $item['partner']->ID ?? 0;
                  }, $partners);
                  break;
                }
              }
              if ( !empty($partners_sanitized) ) {
                foreach ( $partners_sanitized as $id ) {
                  echo $THEME->render_partner_by_id($id);
                }
              }
          ?>
          </div>

        </div>
      <?= $THEME->render_bs_container( 'closed' ); ?>
    </div>
  </section>

<?php endif; ?>
