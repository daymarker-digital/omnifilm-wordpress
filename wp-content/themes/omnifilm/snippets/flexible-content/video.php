<?php

  /**
  *
  *   Video
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "video";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $background_colour = $data["background_colour"] ?? "black";
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;

  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $video_file = $data["video_file"] ?? [];
  $video_placeholder_image = $data["video_placeholder_image"] ?? [];
  $video_placeholder_image_icon = $THEME->render_svg_icon("play");
  $video_placeholder_image_lazy = $THEME->render_nu_lazyload_image([ "image" => $video_placeholder_image ]);
  $video_type = $data["video_type"] ?? "vimeo";
  $video_vimeo_id = $data["video_vimeo_id"] ?: "";

  // ---------------------------------------- Conditionals
  $has_video_content = $video_vimeo_id || ($video_file["url"] ?? "") ? true : false;
  $video_ratio_height = ( "vimeo" == $video_type ) ? ($data["video_ratio_height"] ?? "" ) : ( $video_file["height"] ?? "");
  $video_ratio_width = ( "vimeo" == $video_type ) ? ( $data["video_ratio_width"] ?? "" ) : ( $video_file["width"] ?? "" );
  $block_classes .= $has_video_content ? " has-video-content" : " no-video-content";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "background_colour" => $background_colour,
        "id" => $block_id,
        "padding_bottom" => $padding_bottom,
        "padding_top" => $padding_top,
      ]);
    ?>

    <?php if ( $video_ratio_height && $video_ratio_width ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__content {
        aspect-ratio: <?= $video_ratio_width; ?>/<?= $video_ratio_height; ?>;
      }
    <?php endif; ?>

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>

        <div class="<?= $block_name; ?>__content <?= $video_type; ?> js--click-to-play-video" data-video-type="<?= $video_type; ?>">

          <?php if ( $video_placeholder_image_lazy ) : ?>
            <div class="<?= $block_name; ?>__placeholder">
              <?php if ( !empty($video_file) || $video_vimeo_id ) : ?>
                <div class="<?= $block_name; ?>__placeholder-icon">
                  <?= $video_placeholder_image_icon; ?>
                </div>
              <?php endif; ?>
              <div class="<?= $block_name; ?>__placeholder-image">
                <?= $video_placeholder_image_lazy; ?>
              </div>
            </div>
          <?php endif; ?>

          <?php
            switch ( $video_type ) {
              case "video-file": {
                echo $THEME->render_lazyload_video_file([
                  "controls" => true,
                  "video_file" => $video_file,
                ]);
                break;
              }
              case "vimeo": {
                echo $THEME->render_lazyload_embed([
                  "controls" => true,
                  "id" => $video_vimeo_id,
                  "source" => "vimeo"
                ]);
                break;
              }
            }
          ?>
        </div>

      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
