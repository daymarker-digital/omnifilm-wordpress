<?php

  /**
  *
  *   Mobile Menu
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'mobile-menu';
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content
  $menu_items = wp_get_nav_menu_items( "Mobile Menu" ) ?: [];

  foreach ( [] as $item ) {
    Tools::debug_this($item);
  }

?>

<div class="<?= $block_name ?>">
  <div class="<?= $block_name ?>__main">

    <?php if ( !empty($menu_items) ) : ?>
      <nav class="<?= $block_name ?>__navigation navigation">
        <?=
          $THEME->render_wp_navigation_items([
            "block_name" => $block_name,
            "current_id" => $id,
            "menu_items" => $menu_items
          ]);
        ?>
      </nav>
    <?php endif; ?>

  </div>
</div>
