public function render_intro( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'intro',
        'classes' => '',
        'html' => '',
        'id' => false,
      ],
      $params
    ));

    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $title = get_field( 'title', $id ) ?: get_the_title( $id );
    $hide_title = get_field( 'hide_title', $id ) ?: false;

    if ( !$hide_title && $title ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1' );
          $html .= '<h1 class="' . $block_name . '__title title title--page-title">' . $title . '</h1>';
        $html .= $this->render_bs_container( 'closed' );
      $html .= '</div>';
    }

    return $html;

  }
