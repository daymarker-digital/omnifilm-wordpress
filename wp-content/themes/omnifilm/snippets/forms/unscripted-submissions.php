<?php

  /**
  *
  *   Unscripted Submissions Form
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "unscripted-submissions";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $form_redirect_type = $data["form_redirect_type"] ?? "";
  $form_redirect_page = $data["form_redirect_page"] ?? "";
  $form_submission_button_title = $data["form_submission_button_title"] ?? "Submit";

?>

<form
  action="https://formspree.io/f/mgedknro"
  class="form js--validate-me"
  method="POST"
  enctype="multipart/form-data"
  data-redirect-url="<?= $form_redirect_page; ?>"
>

  <div class="form__main">

    <div class="form__row rude">
      <?= $THEME->render_form_field([ 'label' => 'Rude', 'name' => 'rude', 'type' => 'text' ]); ?>
    </div>

    <div class="form__row two-column">
      <?= $THEME->render_form_field([ 'label' => 'First Name', 'name' => 'first_name', 'required' => true, 'type' => 'text' ]); ?>
      <?= $THEME->render_form_field([ 'label' => 'Last Name', 'name' => 'last_name', 'required' => true, 'type' => 'text' ]); ?>
    </div>

    <div class="form__row two-column">
      <?= $THEME->render_form_field([ 'label' => 'Email', 'name' => '_replyto', 'required' => true, 'type' => 'email' ]); ?>
      <?= $THEME->render_form_field([ 'label' => 'Phone', 'name' => 'phone', 'required' => true, 'type' => 'tel' ]); ?>
    </div>

    <div class="form__row two-column">
      <?= $THEME->render_form_field([ 'label' => 'Street Address', 'name' => 'address', 'required' => true, 'type' => 'text' ]); ?>
      <?= $THEME->render_form_field([ 'label' => 'City', 'name' => 'city', 'required' => true, 'type' => 'text' ]); ?>
    </div>

    <div class="form__row two-column">
      <?= $THEME->render_form_field([ 'label' => 'Province/State/Region', 'name' => 'region', 'type' => 'text' ]); ?>
      <?= $THEME->render_form_field([ 'label' => 'Country', 'name' => 'country', 'required' => true, 'type' => 'text' ]); ?>
    </div>

    <div class="form__row two-column">
      <?= $THEME->render_form_field([ 'label' => 'Postal/Zip Code', 'name' => 'postal', 'required' => true, 'type' => 'text' ]); ?>
      <?= $THEME->render_form_field([ 'label' => 'Project Title', 'name' => 'project_title', 'required' => true, 'type' => 'text']); ?>
    </div>

    <div class="form__row">
      <?=
        $THEME->render_form_field([
          'accept' => 'application/pdf, .doc, .docx, image/png, image/jpeg',
          'label' => 'Upload File. The total size cannot exceed 25MB and the supported file types are .jpeg, .pngs, .doc, .docx or .pdf',
          'multiple' => false,
          'name' => 'files',
          'required' => false,
          'type' => 'file',
          'value' => '',
        ]);
      ?>
    </div>

    <div class="form__row">
      <?=
        $THEME->render_form_field([
          'label' => 'I have read, understood and agree to the foregoing.',
          'name' => 'agreement',
          'required' => true,
          'type' => 'checkbox',
          'value' => 'I have read, understood and agree to the foregoing.'
        ]);
      ?>
    </div>

    <div class="form__row submit">
      <button class="button button--primary outlined-arrow" type="submit">
        <span class="button__title"><?= $form_submission_button_title; ?></span>
        <span class="button__icon"><?= $THEME->render_svg_icon("arrow"); ?></span>
      </button>
    </div>

  </div>

  <div class="form__loading">
    <div class="form__spinner"></div>
  </div>

</form>

<!-- <script>
    var form = document.getElementById("my-form");

    async function handleSubmit(event) {
      event.preventDefault();
      var status = document.getElementById("my-form-status");
      var data = new FormData(event.target);
      fetch(event.target.action, {
        method: form.method,
        body: data,
        headers: {
            'Accept': 'application/json'
        }
      }).then(response => {
        if (response.ok) {
          status.innerHTML = "Thanks for your submission!";
          form.reset()
        } else {
          response.json().then(data => {
            if (Object.hasOwn(data, 'errors')) {
              status.innerHTML = data["errors"].map(error => error["message"]).join(", ")
            } else {
              status.innerHTML = "Oops! There was a problem submitting your form"
            }
          })
        }
      }).catch(error => {
        status.innerHTML = "Oops! There was a problem submitting your form"
      });
    }
    form.addEventListener("submit", handleSubmit)
</script> -->
