<?php

  /**
  *
  *   Latest News Articles
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "latest-news";
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $cols = $data["cols"] ?? "col-12 col-xl-10 offset-xl-1";
  $container = $data["container"] ?? "container";
  $enable = $data["enable"] ?? false;
  $padding_top = $data["padding_top"] ?? 0;
  $padding_bottom = $data["padding_bottom"] ?? 0;
  $heading = $data["heading"] ?? "";
  $team_members = $data["team_members"] ?? [];

  // ---------------------------------------- Content (Post)
  $category_list = get_the_terms($id, "category") ?: [];
  $category_list_primary = $category_list[0] ?? false;
  $category_list_primary_name = $category_list_primary->name ?? '';
  $title = "More News";

  // ---------------------------------------- Conditionals
  $title = $category_list_primary_name ? "{$title} from {$category_list_primary_name}" : $title;

?>

<section>
  <div class="<?= $block_name; ?>__main">
    <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
      <h2><?= $title; ?></h2>
    <?= $THEME->render_bs_container( 'closed' ); ?>
  </div>
</section>
