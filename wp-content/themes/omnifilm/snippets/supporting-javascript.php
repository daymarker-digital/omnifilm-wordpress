<script async src="https://cdn.jsdelivr.net/npm/lazysizes@5.3.2/lazysizes.min.js" integrity="sha256-PZEg+mIdptYTwWmLcBTsa99GIDZujyt7VHBZ9Lb2Jys=" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha256-m81NDyncZVbr7v9E6qCWXwx/cwjuWDlHCMzi9pjMobA=" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/axios@1.3.4/dist/axios.min.js" integrity="sha256-EIyuZ2Lbxr6vgKrEt8W2waS6D3ReLf9aeoYPZ/maJPI=" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/validator@13.9.0/validator.min.js" integrity="sha256-FwIvpbXCs5jmYYy4G0/pLEV4mkLI4Y++GVG1tSr6dTw=" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/combine/npm/lightgallery@2.7.1,npm/lightgallery@2.7.1/plugins/fullscreen/lg-fullscreen.min.js,npm/lightgallery@2.7.1/plugins/video/lg-video.min.js,npm/lightgallery@2.7.1/plugins/autoplay/lg-autoplay.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/animejs@3.2.1/lib/anime.min.js" integrity="sha256-XL2inqUJaslATFnHdJOi9GfQ60on8Wx1C2H8DYiN1xY=" crossorigin="anonymous"></script>

<script>

  document.documentElement.style.setProperty( "--theme-viewport-height--total", window.innerHeight + "px" );

  window.lazySizesConfig = window.lazySizesConfig || {};
  window.lazySizesConfig.lazyClass = 'lazyload';
  lazySizesConfig.loadMode = 1;

  document.addEventListener('lazybeforeunveil', function(e){
    let bg = e.target.getAttribute('data-bg');
    if ( bg ) e.target.style.backgroundImage = 'url(' + bg + ')';
  });

  document.addEventListener('lazyloaded', function(e){
    e.target.parentNode.classList.add('lazyloaded');
  });

  WebFontConfig = { typekit: { id: 'sjt4wgv' } };

  (function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://cdn.jsdelivr.net/npm/webfontloader@1.6.28/webfontloader.min.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
  })(document);

</script>
