<?php

  /**
  *
  *   Header
  *
  */


  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'header';
  $block_classes = $block_name;
  $block_id = $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 250;
  $aos_increment = 150;
  $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]);

  // ---------------------------------------- Content (ACF)
  $header = get_field("header", "options") ?: [];
  $header_menu = $header["menu"] ?? '';

  $header_reel_video_type = $header["video_type"] ?: "";
  $header_reel_video_file = $header["video_file"] ?: [];
  $header_reel_video_file_url = $header_reel_video_file["url"] ?? "";
  $header_reel_video_file_mime_type = $header_reel_video_file["mime_type"] ?? "";
  $header_reel_video_placeholder_image = $header["video_placeholder_image"] ?: [];
  $header_reel_video_ratio_height = $header["video_ratio_height"] ?: "";
  $header_reel_video_ratio_width = $header["video_ratio_width"] ?: "";
  $header_reel_video_vimeo_id = $header["video_vimeo_id"] ?: "";

  // ---------------------------------------- Conditionals
  $has_reel = ( "vimeo" == $header_reel_video_type && $header_reel_video_vimeo_id ) || ( "video-file" == $header_reel_video_type && $header_reel_video_file_url ) ? true : false;

?>

<header class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" <?= $aos_attrs; ?>>
  <?= $THEME->render_bs_container( 'open', 'col-12', 'container-fluid' ); ?>
    <div class="<?= $block_name; ?>__main">

      <div class="<?= $block_name; ?>__brand">
        <a class="<?= $block_name; ?>__brand-link link" href="<?= $THEME->get_theme_directory( 'home' ); ?>" target="_self" title="Home">
          <?= $THEME->render_svg([ 'type' => 'logo.wide' ]); ?>
        </a>
      </div>

      <nav class="<?= $block_name; ?>__navigation navigation">
        <?=
          $THEME->render_wp_navigation_item([
            'current_id' => $id,
            'menu_name' => $header_menu
          ]);
        ?>
      </nav>

      <button class="<?= $block_name; ?>__hamburger button button--hamburger hamburger hamburger--slider d-lg-none js--mobile-menu-trigger" type="button" aria-label="Menu" aria-controls="mobile-menu__navigation">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>

      <?php if ( $has_reel ) : ?>
        <button
          class='<?= $block_name; ?>__watch-reel button button--watch-reel d-none d-lg-inline-flex'
          id='watch-omni-reel'
          type='button'
          <?php if ( "video-file" == $header_reel_video_type ) : ?>
            data-video='{"source": [{"src":"<?= $header_reel_video_file_url; ?>", "type":"<?= $header_reel_video_file_mime_type; ?>"}], "attributes": {"preload": false, "playsinline": true, "controls": true}}'
          <?php endif; ?>
          <?php if ( "vimeo" == $header_reel_video_type ) : ?>
            data-src="https://vimeo.com/<?= $header_reel_video_vimeo_id; ?>?autoplay=1"
          <?php endif; ?>
        >
          <span class="button__title">Watch Our Reel</span>
          <span class="button__icon"><?= $THEME->render_svg_icon("play"); ?></span>
        </button>
      <?php endif; ?>

    </div>
  <?= $THEME->render_bs_container( 'closed' ); ?>
</header>

