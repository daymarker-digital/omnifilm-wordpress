<?php

  /**
  *
  *   Feature Grid Small
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "card-production";
  $block_style = "featured-grid-small";
  $block_classes = "{$block_name} {$block_style}";
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content
  $post_id = $data["id"] ?: 0;
  $about = get_field("about", $post_id) ?: "";
  $broadcasters = get_field("broadcasters", $post_id) ?: [];
  $collapse_id = $THEME->get_unique_id("{$block_name}__");
  $excerpt = get_the_excerpt($post_id);

  $feature_image = $THEME->get_featured_image_by_post_id($post_id);
  $feature_image_lazy = $THEME->render_nu_lazyload_image([ "image" => $feature_image ]);
  $feature_image_alt = get_field("alternate_featured", $post_id) ?: [];
  $feature_image_alt_src = $feature_image_alt["url"] ?? "";

  $permalink = get_the_permalink($post_id);

  $reel_video_type = get_field("video_type", $post_id) ?: "";
  $reel_video_file = get_field("video_file", $post_id) ?: [];
  $reel_video_placeholder_image = get_field("video_placeholder_image", $post_id) ?: $feature_image;
  $reel_video_ratio_height = get_field("video_ratio_height", $post_id) ?: "";
  $reel_video_ratio_width = get_field("video_ratio_width", $post_id) ?: "";
  $reel_video_vimeo_id = get_field("video_vimeo_id", $post_id) ?: "";

  $svg_icon_play = $THEME->render_svg_icon("play");
  $svg_icon_plus = $THEME->render_svg_icon("plus");
  $svg_icon_plus_circle = $THEME->render_svg_icon("plus-circle");

  $title = get_the_title($post_id) ?: "";
  $type = get_field("type", $post_id) ?: "";

  // ---------------------------------------- Conditionals
  $has_reel = ( "vimeo" == $reel_video_type && $reel_video_vimeo_id ) || ( "video-file" == $reel_video_type && $reel_video_file["url"] ?? "" ) ? true : false;

?>

<article class="<?= $block_classes; ?>" id="<?= $block_id; ?>" data-feature-image-alt-src="<?= $feature_image_alt_src; ?>">

  <div class="<?= $block_name; ?>__hero">
    <div class="<?= $block_name; ?>__hero-feature-image"><?= $feature_image_lazy; ?></div>
    <div class="<?= $block_name; ?>__hero-content">
      <?php if ( $type ) : ?>
        <span class="<?= $block_name; ?>__hero-type"><?= $type; ?></span>
      <?php endif; ?>
      <?php if ( $title ) : ?>
        <strong class="<?= $block_name; ?>__hero-title"><?= $title; ?></strong>
      <?php endif; ?>
      <?php if ( $has_reel ) : ?>
        <button class="<?= $block_name; ?>__hero-cta button button--tertiary js--toggle-card-production-reel" type="button">
          <span class="button__icon"><?= $svg_icon_play; ?></span>
          <span class="button__title">Watch Trailer</span>
        </button>
      <?php endif; ?>
    </div>
    <button class="<?= $block_name; ?>__hero-collapse-trigger button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#<?= $collapse_id; ?>" aria-expanded="false" aria-controls="<?= $collapse_id; ?>">
      <span class="button__icon">
        <?= $svg_icon_plus; ?>
      </span>
    </button>
  </div>

  <div class="<?= $block_name; ?>__body collapse" id="<?= $collapse_id; ?>">
    <div class="<?= $block_name; ?>__body-content body-copy--primary body-copy--md">

      <?php if ( $has_reel ) : ?>
        <div class="<?= $block_name; ?>__body-reel d-none">
          <?php
            get_template_part( "snippets/flexible-content/video", null, [
              "container" => "full-width",
              "enable" => true,
              "video_file" => $reel_video_file,
              "video_placeholder_image" => $reel_video_placeholder_image,
              "video_type" => $reel_video_type,
              "video_vimeo_id" => $reel_video_vimeo_id,
              "video_ratio_height" => $reel_video_ratio_height,
              "video_ratio_width" => $reel_video_ratio_width,
            ]);
          ?>
        </div>
      <?php endif; ?>

      <?php if ( $about ) : ?>
        <div class="<?= $block_name; ?>__body-about body-copy--primary">
          <?= $about; ?>
        </div>
      <?php endif; ?>

      <?php
        if ( !empty($broadcasters) ) {
          echo $THEME->render_production_partners([
            "partners" => $broadcasters,
            "title" => "Airing on"
          ]);
        }
      ?>

      <div class="<?= $block_name; ?>__body-cta">
        <?=
          $THEME->render_link([
            "link" => $permalink,
            "style" => "outlined-plus",
            "target" => "_self",
            "title" => "Read More",
          ]);
        ?>
      </div>

    </div>
  </div>

  <div class="<?= $block_name; ?>__hover-content">
    <?php if ( $title ) : ?>
      <h2 class="<?= $block_name; ?>__hover-content-title"><?= $title; ?></h2>
    <?php endif; ?>

    <?php if ( $excerpt ) : ?>
      <div class="<?= $block_name; ?>__hover-content-excerpt body-copy--primary body-copy--sm"><?= trim_string( $excerpt, 265, "..." ); ?></div>
    <?php endif; ?>

    <div class="<?= $block_name; ?>__hover-content-cta body-copy--primary body-copy--sm">
      <?=
        $THEME->render_link([
          "link" => $permalink,
          "style" => "outlined-plus",
          "target" => "_self",
          "title" => "Read More",
        ]);
      ?>
    </div>

    <?php
      if ( !empty($broadcasters) ) {
        echo $THEME->render_production_partners([
          "partners" => $broadcasters,
          "title" => ""
        ]);
      }
    ?>
  </div>

</article>
