<?php

  /**
  *
  *   Feature Grid Large
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "card-production";
  $block_style = "featured-grid-large";
  $block_classes = "{$block_name} {$block_style}";
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content
  $post_id = $data["id"] ?: 0;
  $feature_image = $THEME->get_featured_image_by_post_id($post_id);
  $feature_image_lazy = $THEME->render_nu_lazyload_image([ "image" => $feature_image ]);
  $feature_image_alt = get_field("alternate_featured", $post_id) ?: [];
  $feature_image_alt_src = $feature_image_alt["url"] ?? "";
  $permalink = get_the_permalink($post_id);
  $title = get_the_title($post_id) ?: "";
  $type = get_field("type", $post_id) ?: "";

  // ---------------------------------------- Conditionals
  $block_classes .= $feature_image_alt_src ? " has-feature-image-alt" : "";

?>

<article class="<?= $block_classes; ?>" id="<?= $block_id; ?>" data-feature-image-alt-src="<?= $feature_image_alt_src; ?>">
  <a class="<?= $block_name; ?>__link link" href="<?= $permalink; ?>" title="<?= $title; ?>" target="_self">
    <?php if ( $feature_image ) : ?>
      <div class="<?= $block_name; ?>__feature-image"><?= $feature_image_lazy; ?></div>
    <?php endif; ?>
    <div class="<?= $block_name; ?>__content">
      <?php if ( $type ) : ?>
        <span class="<?= $block_name; ?>__type"><?= $type; ?></span>
      <?php endif; ?>
      <?php if ( $title ) : ?>
        <strong class="<?= $block_name; ?>__title"><?= $title; ?></strong>
      <?php endif; ?>
    </div>
  </a>
</article>

