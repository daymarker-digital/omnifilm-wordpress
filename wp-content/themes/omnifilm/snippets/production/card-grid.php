<?php

  /**
  *
  *   Feature Grid Large
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Data
  $data = $args ?? [];
  $block_name = "card-production";
  $block_style = "grid";
  $block_classes = "{$block_name} {$block_style}";
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content
  $post_id = $data["id"] ?: 0;
  $feature_image = $THEME->get_featured_image_by_post_id($post_id);
  $feature_image_lazy = $THEME->render_nu_lazyload_image([ "image" => $feature_image ]);
  $permalink = get_the_permalink($post_id);
  $title = get_the_title($post_id) ?: "";

?>

<article class="<?= $block_classes; ?>" id="<?= $block_id; ?>">
  <a class="<?= $block_name; ?>__link link" href="<?= $permalink; ?>" title="<?= $title; ?>" target="_self">
    <?php if ( $feature_image ) : ?>
      <div class="<?= $block_name; ?>__feature-image"><?= $feature_image_lazy; ?></div>
    <?php endif; ?>
    <div class="<?= $block_name; ?>__content">
      <?php if ( $title ) : ?>
        <strong class="<?= $block_name; ?>__title"><?= $title; ?></strong>
      <?php endif; ?>
    </div>
    <div class="<?= $block_name; ?>__gradient"></div>
    <div class="<?= $block_name; ?>__nat-geo"></div>
  </a>
</article>
